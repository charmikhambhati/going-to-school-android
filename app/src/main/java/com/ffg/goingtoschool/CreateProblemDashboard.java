package com.ffg.goingtoschool;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.amazonaws.util.StringUtils;
import com.ffg.goingtoschool.S3.S3Uploader;
import com.ffg.goingtoschool.apiConsumer.APIClient;
import com.ffg.goingtoschool.apiConsumer.APIInterface;
import com.ffg.goingtoschool.apiConsumer.pojo.ProblemDashboardPojo;
import com.ffg.goingtoschool.apiConsumer.pojo.ResponseWrapper;
import com.ffg.goingtoschool.apiConsumer.pojo.School;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateProblemDashboard extends AppCompatActivity {

    Button popup_close;
    Button save_dashboard;
    TextView change_cover_photo;
    private Uri imageUri;
    private ImageView profilePictureView;
    boolean isImageFitToScreen;
    private APIInterface apiInterface;
    final MySharePreference sp = new MySharePreference();
    private S3Uploader s3uploaderObj;
    private TextInputLayout problemDashboardName;
    public LottieAnimationView animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_problem_dashboard);

        popup_close = (Button) findViewById(R.id.cancelDashBoardPopup);
        save_dashboard = (Button) findViewById(R.id.createProblemDashBtn);
        change_cover_photo = (TextView) findViewById(R.id.addDashboardCover);
        profilePictureView = (ImageView) findViewById(R.id.pdCoverPicture);
        popup_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();return;
            }
        });
        save_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDashboardDetails();
            }
        });
        change_cover_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        s3uploaderObj = new S3Uploader(getApplicationContext());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        problemDashboardName = findViewById(R.id.problemDashboardName);
        animation = (LottieAnimationView) findViewById(R.id.thumb_down);
        animation.setRepeatCount(LottieDrawable.INFINITE);

        profilePictureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width * .8), (int) (height * .8));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        params.dimAmount = 0.5f;
        getWindow().setAttributes(params);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "IMG_" + timeStamp, null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, Constants.MY_CAMERA_PERMISSION_CODE);
                        } else {
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Camera not supported", Toast.LENGTH_LONG).show();
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.CAMERA_REQUEST) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                Uri tempUri = getImageUri(getApplicationContext(), thumbnail);
                File finalFile = new File(getRealPathFromURI(tempUri));
                Log.d("ACTIVITY RESULT", "final url : " + finalFile);
                imageUri = Uri.fromFile(finalFile);
                Bitmap bMapScaled = Bitmap.createScaledBitmap(thumbnail, profilePictureView.getWidth(), profilePictureView.getHeight(), true);
                profilePictureView.setImageBitmap(bMapScaled);
            } else if (requestCode == 2) {
                imageUri = data.getData();
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(imageUri);
                } catch (FileNotFoundException e) {
                    Log.e("Exception:", e.getMessage());
                }
                if (imageStream != null) {
                    profilePictureView.setImageBitmap(BitmapFactory.decodeStream(imageStream));
                }
            }
        }
    }

    private void saveDashboardDetails() {
        if (StringUtils.isBlank(problemDashboardName.getEditText().getText().toString())) {
            Toast.makeText(CreateProblemDashboard.this, "Enter Dashboard Name", Toast.LENGTH_LONG).show();
        } else {
            uploadImageTos3(imageUri, false);
        }
    }

    private String getFilePathfromURI(Uri selectedImageUri) {
        if (selectedImageUri == null) {
            return "";
        }

        String filePath = "";
        if (selectedImageUri.toString().contains("file:")) {
            filePath = selectedImageUri.toString().substring(7, selectedImageUri.toString().length());
        } else if(selectedImageUri.toString().contains("com.google.android.apps.photos")){
            return CommonUtil.getPathFromInputStreamUri(getApplicationContext(),selectedImageUri);
        }else {
            Log.d("TAG", selectedImageUri.toString());
            String wholeID = DocumentsContract.getDocumentId(selectedImageUri);
            String id = wholeID.split(":")[1];  // Split at colon, use second item in the array
            String[] column = {MediaStore.Images.Media.DATA};
            String sel = MediaStore.Images.Media._ID + "=?"; // where id is equal to

            Cursor cursor = getApplicationContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath;
    }

    private void uploadImageTos3(Uri imageUri, final boolean isUpdate) {
        Log.d("TAG", "Entered Upload image to S3 bucket");
        showLoading();
        final String path = getFilePathfromURI(imageUri);
        Log.d("TEAM_CREATION", "Image path : " + path);
        if (!"".equalsIgnoreCase(path)) {
            s3uploaderObj.initUpload(path, true);
            s3uploaderObj.setOns3UploadDone(new S3Uploader.S3UploadInterface() {
                @Override
                public void onUploadSuccess(String response) {
                    if (response.equalsIgnoreCase("Success")) {
                        ProblemDashboardPojo problemDashboardPojo = new ProblemDashboardPojo();
                        problemDashboardPojo.setName(problemDashboardName.getEditText().getText().toString().replace(" ", ""));
                        problemDashboardPojo.setCover_picture((new File(path)).getName());

                        Gson gson = new Gson();
                        String school = sp.getStringFromMyPref(Constants.SCHOOL_SP, "");
                        String team = sp.getStringFromMyPref(Constants.TEAM_SP, "");


                        //Fetch School ID from SP
                        if (school != null && !"".equals(school)) {
                            problemDashboardPojo.setSchool((gson.fromJson(school, School.class)).getId());
                        } else {
                            // we should never get to this step..
                            Toast.makeText(CreateProblemDashboard.this, "No School Selected", Toast.LENGTH_LONG).show();
                        }
                        if (team != null && !"".equals(team)) {
                            problemDashboardPojo.setTeam((gson.fromJson(team, Team.class)).getId());
                        } else {
                            // we should never get to this step..
                            problemDashboardPojo.setTeam(0);
                            Toast.makeText(CreateProblemDashboard.this, "No Team Selected", Toast.LENGTH_LONG).show();
                        }

                        //Fetch WARD ID from SP
                        problemDashboardPojo.setWard(Integer.parseInt(sp.getStringFromMyPref(Constants.WARD_ID_SP, "")));


                        if (problemDashboardPojo.getTeam() == 0 || problemDashboardPojo.getSchool() == 0  || problemDashboardPojo.getWard() == 0
                                || problemDashboardPojo.getName().isEmpty()) {
                            hideLoading();
                            Toast.makeText(CreateProblemDashboard.this, "Some values needed to create dashbaord are not found", Toast.LENGTH_LONG).show();
                        } else {

                            Call<ProblemDashboardPojo> call = apiInterface.createProblemDashboard(sp.getStringFromMyPref(Constants.AWS_TOKEN, ""), problemDashboardPojo);
                            call.enqueue(new Callback<ProblemDashboardPojo>() {
                                @Override
                                public void onResponse(Call<ProblemDashboardPojo> call, Response<ProblemDashboardPojo> response) {

                                    AlertDialog alert;
                                    AlertDialog.Builder builder = new   AlertDialog.Builder(CreateProblemDashboard.this);
                                    builder.setMessage("Problem Dashboard Created Successfully")
                                            .setTitle("Alert")
                                            .setCancelable(false);

                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            hideLoading();
                                            gotoDashboardDetails(problemDashboardName.getEditText().getText().toString().replace(" ", ""));
//                                            Intent intent = new Intent(CreateProblemDashboard.this, ProblemDashboardDetails.class);
//                                            intent.putExtra("selected_problem_dashboard", problemDashboardName.getEditText().getText().toString().replace(" ", ""));
//                                            intent.putExtra("selected_problem_dashboard_id", 0);
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                            getApplicationContext().startActivity(intent);
                                        }
                                    });
                                    alert = builder.create();

                                    alert.show();

                                }

                                @Override
                                public void onFailure(Call<ProblemDashboardPojo> call, Throwable t) {
                                    AlertDialog alert;
                                    AlertDialog.Builder builder = new   AlertDialog.Builder(CreateProblemDashboard.this);
                                    builder.setMessage("Problem Dashboard")
                                            .setTitle("Alert")
                                            .setCancelable(true);

                                    builder.setPositiveButton("Something went wrong while creating Problem Dashboard",new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    alert = builder.create();
                                    alert.show();
                                    Toast.makeText(CreateProblemDashboard.this, "Problem Dashbaord creation failed", Toast.LENGTH_LONG);
                                    hideLoading();
                                }
                            });
                        }
                    }
                    hideLoading();
                }

                @Override
                public void onUploadError(String response) {
                    hideLoading();
                    Log.e("TAG", "Error Uploading Image : " + response);
                }
            });
        } else {
            ProblemDashboardPojo problemDashboardPojo = new ProblemDashboardPojo();
            problemDashboardPojo.setName(problemDashboardName.getEditText().getText().toString().replace(" ", ""));
            problemDashboardPojo.setCover_picture((new File(path)).getName());

            Gson gson = new Gson();
            String school = sp.getStringFromMyPref(Constants.SCHOOL_SP, "");
            String team = sp.getStringFromMyPref(Constants.TEAM_SP, "");


            //Fetch School ID from SP
            if (school != null && !"".equals(school)) {
                problemDashboardPojo.setSchool((gson.fromJson(school, School.class)).getId());
            } else {
                // we should never get to this step..
                Toast.makeText(CreateProblemDashboard.this, "No School Selected", Toast.LENGTH_LONG).show();
            }
            if (team != null && !"".equals(team)) {
                problemDashboardPojo.setTeam((gson.fromJson(team, Team.class)).getId());
            } else {
                // we should never get to this step..
                problemDashboardPojo.setTeam(0);
                Toast.makeText(CreateProblemDashboard.this, "No Team Selected", Toast.LENGTH_LONG).show();
            }

            //Fetch WARD ID from SP
            problemDashboardPojo.setWard(Integer.parseInt(sp.getStringFromMyPref(Constants.WARD_ID_SP, "")));


            if (problemDashboardPojo.getTeam() == 0 || problemDashboardPojo.getSchool() == 0 || problemDashboardPojo.getWard() == 0
                    || problemDashboardPojo.getName().isEmpty()) {
                hideLoading();
                Toast.makeText(CreateProblemDashboard.this, "Some values needed to create dashbaord are not found", Toast.LENGTH_LONG).show();
            } else {

                Call<ProblemDashboardPojo> call = apiInterface.createProblemDashboard(sp.getStringFromMyPref(Constants.AWS_TOKEN, ""), problemDashboardPojo);
                call.enqueue(new Callback<ProblemDashboardPojo>() {
                    @Override
                    public void onResponse(Call<ProblemDashboardPojo> call, Response<ProblemDashboardPojo> response) {

                        AlertDialog alert;
                        AlertDialog.Builder builder = new AlertDialog.Builder(CreateProblemDashboard.this);
                        builder.setMessage("Problem Dashboard Created Successfully")
                                .setTitle("Alert")
                                .setCancelable(false);

                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                gotoDashboardDetails(problemDashboardName.getEditText().getText().toString().replace(" ", ""));
//                                hideLoading();
//                                Intent intent = new Intent(CreateProblemDashboard.this, MainActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                getApplicationContext().startActivity(intent);
                            }
                        });
                        alert = builder.create();

                        alert.show();
                    }

                    @Override
                    public void onFailure(Call<ProblemDashboardPojo> call, Throwable t) {
                        AlertDialog alert;
                        AlertDialog.Builder builder = new AlertDialog.Builder(CreateProblemDashboard.this);
                        builder.setMessage("Problem Dashboard")
                                .setTitle("Alert")
                                .setCancelable(true);

                        builder.setPositiveButton("Something went wrong while creating Problem Dashboard", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert = builder.create();
                        alert.show();
                        Toast.makeText(CreateProblemDashboard.this, "Problem Dashbaord creation failed", Toast.LENGTH_LONG);
                        hideLoading();
                    }
                });
            }
        }
    }

    private void showLoading() {
        animation.setVisibility(View.VISIBLE);
        animation = (LottieAnimationView) findViewById(R.id.thumb_down);
        animation.setRepeatCount(LottieDrawable.INFINITE);
        animation.playAnimation();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void hideLoading() {
        animation.pauseAnimation();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        animation.setVisibility(View.INVISIBLE);
    }

    private void gotoDashboardDetails(String dashbaordName){
        showLoading();
        ProblemDashboardPojo pojo = new ProblemDashboardPojo();
        pojo.setName(dashbaordName);

        Call<ResponseWrapper<ProblemDashboardPojo>> call =
                apiInterface.fetchProblemDashboard(sp.getStringFromMyPref(Constants.AWS_TOKEN, ""), pojo);
        call.enqueue(new Callback<ResponseWrapper<ProblemDashboardPojo>>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseWrapper<ProblemDashboardPojo>> call, Response<ResponseWrapper<ProblemDashboardPojo>> responseWrapper) {
                Log.d("TAG", responseWrapper.code() + "");
                if (responseWrapper.isSuccessful()) {
                    ResponseWrapper<ProblemDashboardPojo> res = responseWrapper.body();
                    if (res != null && res.getStatus() != null && res.getStatus().equalsIgnoreCase("success")) {
                        List<ProblemDashboardPojo> problems = res.getData();
                        Log.d("TAG", "ProblemDashboardModel -> " + problems.size());

                        if (problems.size() != 0) {
                            hideLoading();
                            Intent intent = new Intent(CreateProblemDashboard.this, ProblemDashboardDetails.class);
                            intent.putExtra("selected_problem_dashboard", problems.get(0).getName());
                            intent.putExtra("selected_problem_dashboard_id", problems.get(0).getId());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getApplicationContext().startActivity(intent);
                        }else{
                            Toast.makeText(getApplicationContext(), "Dashboard creation failed", Toast.LENGTH_SHORT).show();
                            Log.e("TAG", responseWrapper.message());
                            hideLoading();
                        }

                    }
                    hideLoading();
                } else {
                    Toast.makeText(getApplicationContext(), "Error while fetching dashboard", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", responseWrapper.message());
                    hideLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ProblemDashboardPojo>> call, Throwable t) {
                call.cancel();
                Toast.makeText(getApplicationContext(), "Error while fetching dashboards", Toast.LENGTH_SHORT).show();
                hideLoading();
            }
        });
    }

}