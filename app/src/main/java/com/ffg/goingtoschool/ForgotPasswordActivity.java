package com.ffg.goingtoschool;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.chaos.view.PinView;
import com.google.android.material.textfield.TextInputLayout;

public class ForgotPasswordActivity extends AppCompatActivity {

    private TextInputLayout m_username;
    private PinView m_resetCode;
    private TextInputLayout m_newPassword;
    private TextInputLayout m_newPasswordRepeat;

    private Button sendCode;
    private Button confirmReset;
    public static LinearLayout first, second;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        m_username = (TextInputLayout) findViewById(R.id.etForgotpwdUsername);
        sendCode = (Button) findViewById(R.id.btnSendResetCode);
        sendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cognito auth = new Cognito(getApplicationContext(), ForgotPasswordActivity.this);
                auth.forgetPassword(m_username.getEditText().getText().toString().replace(" ", ""));
            }
        });

        first = findViewById(R.id.first_step);
        second = findViewById(R.id.secondStep);
        m_resetCode = (PinView) findViewById(R.id.resetCode);
        m_newPassword = (TextInputLayout) findViewById(R.id.newPassword);
        m_newPasswordRepeat = (TextInputLayout) findViewById(R.id.newPasswordRepeat);


        confirmReset = findViewById(R.id.resetPwdVerify);


        confirmReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (m_newPassword.getEditText().getText().toString().equals(m_newPasswordRepeat.getEditText().getText().toString())) {
                    Cognito auth = new Cognito(getApplicationContext(), ForgotPasswordActivity.this);
                    auth.resetPassword(m_resetCode.getText().toString().replace(" ", ""), m_newPassword.getEditText().getText().toString());
                } else {
                    Toast.makeText(getApplicationContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
