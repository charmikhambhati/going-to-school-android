package com.ffg.goingtoschool.S3;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.ffg.goingtoschool.Constants;
import com.ffg.goingtoschool.MySharePreference;
import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.ffg.goingtoschool.database.MediaUploadUtil;
import com.google.gson.Gson;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class S3Uploader {

    private static final String TAG = "S3Uploader";

    Context context;

    private TransferUtility transferUtility;
    public S3UploadInterface s3UploadInterface;
    final MySharePreference sp = new MySharePreference();

    public S3Uploader(Context context) {
        this.context = context;
        transferUtility = AmazonUtil.getTransferUtility(context);
    }

    public void initUpload(String filePath, boolean isTeamOrCoverPic) {
        File file = new File(filePath);
        ObjectMetadata myObjectMetadata = new ObjectMetadata();
        myObjectMetadata.setContentType("image/png");

        Gson gson = new Gson();
        String team = sp.getStringFromMyPref(Constants.TEAM_SP, "");
        int teamId = (gson.fromJson(team, Team.class)).getId();
        String teamName = (gson.fromJson(team, Team.class)).getName();

        MediaUploadUtil mediaUtil = new MediaUploadUtil(context);
        LinkedHashSet<MediaUpload> uploadedMediaList = mediaUtil.readMediaUpload(filePath);
        Iterator<MediaUpload> itr = uploadedMediaList.iterator();

        while (itr.hasNext()) {
            MediaUpload mediaUpload;
            mediaUpload = itr.next();
            teamId = mediaUpload.getTeam_Id();
            teamName = mediaUpload.getTeam_Name();
        }

        String mediaUrl;
        if (isTeamOrCoverPic || "".equalsIgnoreCase(team)) {
            myObjectMetadata.addUserMetadata("team", "" + 0);
             mediaUrl = file.getName();

        } else {
            myObjectMetadata.addUserMetadata("team", "" + teamName);
            mediaUrl = teamId + "_" + teamName + "/" + file.getName();

        }

        Log.d(TAG, "initUpload: " + mediaUrl);
        TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, mediaUrl, file, myObjectMetadata);

        observer.setTransferListener(new UploadListener());
    }
    public void initUpload(String filePath,  String subDirName) {
        File file = new File(filePath);
        ObjectMetadata myObjectMetadata = new ObjectMetadata();
        myObjectMetadata.setContentType("image/png");

        String mediaUrl = subDirName + "/" + file.getName();
        TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, mediaUrl, file, myObjectMetadata);

        observer.setTransferListener(new UploadListener());
    }

    public void initDownload(){

    }

    private class UploadListener implements TransferListener {
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "Error during upload: " + id, e);
            s3UploadInterface.onUploadError(e.toString());
            s3UploadInterface.onUploadError("Error");
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            if (newState == TransferState.COMPLETED) {
                s3UploadInterface.onUploadSuccess("Success");
            }
        }
    }

    public void setOns3UploadDone(S3UploadInterface s3UploadInterface) {
        this.s3UploadInterface = s3UploadInterface;
    }

    public interface S3UploadInterface {
        void onUploadSuccess(String response);
        void onUploadError(String response);
    }
}
