package com.ffg.goingtoschool.S3;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.ffg.goingtoschool.Constants;
import com.ffg.goingtoschool.MySharePreference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class AmazonUtil {

    // We only need one instance of the clients and credentials provider
    private static AmazonS3Client sS3Client;
    private static CognitoCachingCredentialsProvider sCredProvider;
    private static TransferUtility sTransferUtility;
    private static MySharePreference sp = new MySharePreference();

    /**
     * Gets an instance of CognitoCachingCredentialsProvider which is
     * constructed using the given Context.
     *
     * @return A default credential provider.
     */
    private AmazonUtil() {

    }
    protected static CognitoCachingCredentialsProvider getCredProvider(Context context) {
        if (sCredProvider == null) {
            // Initialize the Amazon Cognito credentials provider
            sCredProvider = new CognitoCachingCredentialsProvider(
                    context,
                    Constants.IDENTITY_POOL_ID, // Identity Pool ID
                    Regions.AP_SOUTH_1 // Region
            );
            Map<String, String> logins = new HashMap<>();
            logins.put("cognito-idp.ap-south-1.amazonaws.com/"+Constants.POOL_ID, sp.getStringFromMyPref(Constants.AWS_TOKEN,""));
            sCredProvider.setLogins(logins);
        }

        return sCredProvider;
    }

    /**
     * Gets an instance of a S3 client which is constructed using the given
     * Context.
     *
     * @param context An Context instance.
     * @return A default S3 client.
     */
    public static AmazonS3Client getS3Client(Context context) {
        if (sS3Client == null) {
            sS3Client = new AmazonS3Client(getCredProvider(context));
            sS3Client.setRegion(Region.getRegion(Regions.AP_SOUTH_1));
        }
        return sS3Client;
    }

    /**
     * Gets an instance of the TransferUtility which is constructed using the
     * given Context
     *
     * @param context
     * @return a TransferUtility instance
     */
    public static TransferUtility getTransferUtility(Context context) {
        if (sTransferUtility == null) {
            sTransferUtility = new TransferUtility(getS3Client(context.getApplicationContext()),
                    context.getApplicationContext());
        }

        return sTransferUtility;
    }

    /**
     * Converts number of bytes into proper scale.
     *
     * @param bytes number of bytes to be converted.
     * @return A string that represents the bytes in a proper scale.
     */
    public static String getBytesString(long bytes) {
        String[] quantifiers = new String[]{
                "KB", "MB", "GB", "TB"
        };
        double speedNum = bytes;
        for (int i = 0; ; i++) {
            if (i >= quantifiers.length) {
                return "";
            }
            speedNum /= 1024;
            if (speedNum < 512) {
                return String.format("%.2f", speedNum) + " " + quantifiers[i];
            }
        }
    }

    /**
     * Copies the data from the passed in Uri, to a new file for use with the
     * Transfer Service
     *
     * @param context
     * @param uri
     * @return
     * @throws IOException
     */
    public static File copyContentUriToFile(Context context, Uri uri) throws IOException {
        try (InputStream is = context.getContentResolver().openInputStream(uri)) {
            File copiedData = new File(context.getDir("SampleImagesDir", Context.MODE_PRIVATE), UUID
                    .randomUUID().toString());
            copiedData.createNewFile();

            try (FileOutputStream fos = new FileOutputStream(copiedData)) {
                byte[] buf = new byte[2046];
                int read = -1;
                while ((read = is.read(buf)) != -1) {
                    fos.write(buf, 0, read);
                }

                fos.flush();
            }
            return copiedData;
        }
    }

    public static boolean deleteObject(String bucketName, String imageName) {
        try {
            sS3Client.deleteObject(bucketName, imageName);
            return true;
        } catch (Exception e) {
            Log.d("Exception:", e.getMessage());
            return false;
        }
    }

    /**
     * @desc This method is used to return list of files name from S3 Bucket
     * @param bucket
     * @param subDir
     * @return object with list of files
     */
    public static List<String> getObjectNamesForBucket(String bucket, String subDir) {
        ListObjectsRequest listObjectsRequest =
                new ListObjectsRequest()
                        .withBucketName(bucket)
                        .withPrefix(subDir);
        ObjectListing objects = sS3Client.listObjects(listObjectsRequest);
        List<String> objectNames=new ArrayList<String>(objects.getObjectSummaries().size());
        Iterator<S3ObjectSummary> iterator=objects.getObjectSummaries().iterator();
        while (iterator.hasNext()) {
            objectNames.add(iterator.next().getKey());
        }
        while (objects.isTruncated()) {
            objects=sS3Client.listNextBatchOfObjects(objects);
            iterator=objects.getObjectSummaries().iterator();
            while (iterator.hasNext()) {
                objectNames.add(iterator.next().getKey());
            }
        }
        return objectNames;
    }

    public static PutObjectResult putImages(String filePath,  String subDirName){
        File file = new File(filePath);
        ObjectMetadata myObjectMetadata = new ObjectMetadata();
        myObjectMetadata.setContentType("image/png");

        String mediaUrl = subDirName + "/" + file.getName();

        return sS3Client.putObject(Constants.BUCKET_NAME,mediaUrl,file);
    }
}
