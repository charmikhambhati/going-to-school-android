package com.ffg.goingtoschool.S3;

import android.content.Context;
import android.util.Log;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.ffg.goingtoschool.Constants;

import java.io.File;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class S3Utils {

    public static final ExecutorService executor = Executors.newSingleThreadExecutor();

    /**
     * Method to generate a presignedurl for the image
     * @param applicationContext context
     * @param path image path
     * @return presignedurl
     */
    public static String generates3ShareUrl(Context applicationContext, String path) {

        File f = new File(path);
        AmazonS3 s3client = AmazonUtil.getS3Client(applicationContext);

        Date expiration = new Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += 1000 * 60 * 60;
        expiration.setTime(expTimeMillis);

        ResponseHeaderOverrides overrideHeader = new ResponseHeaderOverrides();
        overrideHeader.setContentType("image/jpeg");
        String mediaUrl = f.getName();
        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(Constants.BUCKET_NAME, "CapturedMedia/"+mediaUrl);
        generatePresignedUrlRequest.setMethod(HttpMethod.GET); // Default.
        generatePresignedUrlRequest.setExpiration(expiration);
        generatePresignedUrlRequest.setResponseHeaders(overrideHeader);

        URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);
        return url.toString();
    }

    public static String generates3ShareUrlFromFileName(Context applicationContext, String fileName) {

        AmazonS3 s3client = AmazonUtil.getS3Client(applicationContext);
        Date expiration = new Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += 1000 * 60 * 60;
        expiration.setTime(expTimeMillis);

        ResponseHeaderOverrides overrideHeader = new ResponseHeaderOverrides();
        overrideHeader.setContentType("image/jpeg");
        String mediaUrl = fileName;
        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(Constants.BUCKET_NAME, mediaUrl);
        generatePresignedUrlRequest.setMethod(HttpMethod.GET); // Default.
        generatePresignedUrlRequest.setExpiration(expiration);
        generatePresignedUrlRequest.setResponseHeaders(overrideHeader);

        URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);
        return url.toString();
    }

    public static boolean checkObjectExistInS3(final Context applicationContext, final String object){
        final AmazonS3 s3client = AmazonUtil.getS3Client(applicationContext);
        try {
            Future<Boolean> objectCheck = executor.submit(new Callable<Boolean>() {
                public Boolean call() throws Exception {
                    return s3client.doesObjectExist(Constants.BUCKET_NAME,object);
                }
            });

            Boolean res =  objectCheck.get();
            return res;
        }catch (Exception e) {
            Log.e("Exception", e.getMessage());
            return false;
        }
    }


    public static String getS3Url(final Context applicationContext, final String object){
        try {
        Future<String> result = executor.submit(new Callable<String>() {
            public String call() throws Exception {
                return S3Utils.generates3ShareUrlFromFileName(applicationContext, object);
            }
        });
        String res = result.get();
        return res;
        } catch (Exception e) {
            Log.d("Exception:", e.getMessage());
            return "";
        }
    }
}
