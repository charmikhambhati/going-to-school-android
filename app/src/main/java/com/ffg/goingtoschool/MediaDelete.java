package com.ffg.goingtoschool;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ffg.goingtoschool.S3.AmazonUtil;
import com.ffg.goingtoschool.S3.S3Utils;
import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.ffg.goingtoschool.database.MediaUploadUtil;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class MediaDelete extends Fragment {

    protected Switch autoSync;
    protected SharedPreferences mPrefs;
    protected LinkedHashSet<String> contents = new LinkedHashSet();
    protected List<String> checkedMediaList = new ArrayList();
    protected LinkedHashSet<MediaUpload> pendingDeleteMediaList = new LinkedHashSet();
    protected LinkedHashSet<String> pendingDeleteMediaListContents = new LinkedHashSet();

    private View v;
    private Context context;
    protected GalleryAdapter mAdapter;
    private RecyclerView recyclerView;
    private MediaUploadUtil mediaUploadUtil;
    private short wifiStatus = -1;

    public MediaDelete() {
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        mediaUploadUtil = new MediaUploadUtil(getActivity());
        autoSync = (Switch) getView().findViewById(R.id.autoSync);

        if (contents != null && !contents.isEmpty()) {
            contents.clear();
        }
        if (contents != null) {
            contents.addAll(FetchFileNames());
        }

        if (pendingDeleteMediaList != null && !pendingDeleteMediaList.isEmpty()) {
            pendingDeleteMediaList.clear();
        }

        if (pendingDeleteMediaList != null) {
            pendingDeleteMediaList.addAll(mediaUploadUtil.readPendingDeleteMedia());
        }

        if (pendingDeleteMediaListContents != null && !pendingDeleteMediaListContents.isEmpty()) {
            pendingDeleteMediaListContents.clear();
        }

        if (pendingDeleteMediaList != null) {
            Iterator<MediaUpload> itr = pendingDeleteMediaList.iterator();
            while (itr.hasNext()) {
                if (pendingDeleteMediaListContents != null) {
                    pendingDeleteMediaListContents.add(itr.next().getMedia_File_Name());
                }
            }
        }

        mPrefs = getActivity().getSharedPreferences("autoSync_Status", Context.MODE_PRIVATE);

        recyclerView = (RecyclerView) getView().findViewById(R.id.recycler_view);

        mAdapter = new GalleryAdapter(getActivity(), new ArrayList<String>(pendingDeleteMediaListContents));
        setHasOptionsMenu(true);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getActivity(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i;

                ArrayList<String> _pendingDeleteMediaListContents = new ArrayList(pendingDeleteMediaListContents);

                try {
                    if (_pendingDeleteMediaListContents.get(position).contains(Constants.IMAGE_EXTENSION)) {
                        i = new Intent(getActivity(), ImageViewerActivity.class);
                        i.putExtra("filepath", _pendingDeleteMediaListContents.get(position));
                        startActivity(i);
                    } else if (_pendingDeleteMediaListContents.get(position).contains(Constants.VIDEO_EXTENSION)) {
                        i = new Intent(getActivity(), ExoPlayerActivity.class);
                        i.putExtra("filepath", _pendingDeleteMediaListContents.get(position));
                        startActivity(i);
                    }
                } finally {
                    if (_pendingDeleteMediaListContents != null && !_pendingDeleteMediaListContents.isEmpty()) {
                        _pendingDeleteMediaListContents.clear();
                    }
                    if (_pendingDeleteMediaListContents != null && !_pendingDeleteMediaListContents.isEmpty()) {
                        _pendingDeleteMediaListContents.clear();
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {

                ArrayList<MediaUpload> _pendingDeleteMediaList = new ArrayList(pendingDeleteMediaList);
                ArrayList<String> _pendingDeleteMediaListContents = new ArrayList(pendingDeleteMediaListContents);
                try {
                    for (int i = 0; i < _pendingDeleteMediaList.size(); i++) {
                        if (_pendingDeleteMediaList.get(i).getMedia_File_Name().contains(_pendingDeleteMediaListContents.get(position))
                                && !(checkedMediaList.contains(_pendingDeleteMediaListContents.get(position)))) {

                            ((CheckBox) (view.findViewById(R.id.chkBox))).setChecked(true);
                            view.findViewById(R.id.chkBox).setVisibility(View.VISIBLE);
                            checkedMediaList.add(_pendingDeleteMediaListContents.get(position));
                            break;
                        } else if (checkedMediaList.contains(_pendingDeleteMediaListContents.get(position))) {
                            ((CheckBox) (view.findViewById(R.id.chkBox))).setChecked(false);
                            view.findViewById(R.id.chkBox).setVisibility(View.GONE);
                            checkedMediaList.remove(_pendingDeleteMediaListContents.get(position));
                            break;
                        }
                    }
                } finally {
                    if (_pendingDeleteMediaListContents != null && !_pendingDeleteMediaListContents.isEmpty()) {
                        _pendingDeleteMediaListContents.clear();
                    }
                    if (_pendingDeleteMediaList != null && !_pendingDeleteMediaList.isEmpty()) {
                        _pendingDeleteMediaList.clear();
                    }
                }

            }
        }));

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater layout, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = layout.inflate(R.layout.activity_video_gallery_delete, container, false);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private LinkedHashSet<String> FetchFileNames() {

        LinkedHashSet<String> filenames = new LinkedHashSet<String>();
        String path = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + Constants.GALLERY_DIRECTORY_NAME;
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        if (null != files) {
            Log.d("Files", "Size: " + files.length);
            for (File f : files) {
                Log.d("Files", "FileName:" + f.getName());
                String file_name = f.getName();
                // you can store name to arraylist and use it later
                filenames.add(path + "/" + file_name);
            }
        }
        return filenames;
    }

    protected void doMark(boolean bypassMsg) {
        if (pendingDeleteMediaListContents != null && !pendingDeleteMediaListContents.isEmpty()) {
            checkedMediaList.addAll(pendingDeleteMediaListContents);
            mAdapter.selectAll();
        } else if (pendingDeleteMediaListContents != null && pendingDeleteMediaListContents.isEmpty()) {
            Toast.makeText(getActivity(), "No Media found, please take a image/video from capture option", Toast.LENGTH_SHORT).show();
        } else if (bypassMsg) {
            alertDialog(Constants.WARNING_MSG, false);
        }
    }

    public void doUnmark(boolean bypassMsg) {
        if (checkedMediaList != null && !checkedMediaList.isEmpty()) {
            mAdapter.deselectAll();
        } else if (pendingDeleteMediaListContents != null && pendingDeleteMediaListContents.isEmpty() && checkedMediaList != null && checkedMediaList.isEmpty()) {
            Toast.makeText(getActivity(), "No Media found, please take an image/video from Capture option", Toast.LENGTH_SHORT).show();
        } else if (bypassMsg && pendingDeleteMediaListContents != null && !pendingDeleteMediaListContents.isEmpty() && checkedMediaList != null && checkedMediaList.isEmpty()) {
            Toast.makeText(getActivity(), "Nothing to Unmark", Toast.LENGTH_SHORT).show();
        } else if (!bypassMsg) {
            alertDialog(Constants.WARNING_MSG, false);
        }

        if (checkedMediaList != null) {
            checkedMediaList.clear();
        }
    }

    private void alertDialog(String message, boolean btnFlag) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(message);
        dialog.setTitle("Alert!!!");
        dialog.setIcon(R.drawable.ic_error_outline_black_24dp);

        if (btnFlag) {
            dialog.setCancelable(false);

            dialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    LinkedHashSet<MediaUpload> deleteMediaList = new LinkedHashSet();

                                    try {
                                        for (int i = 0; i < checkedMediaList.size(); i++) {
                                            File file = new File(checkedMediaList.get(i));
                                            Uri fileUri = CameraUtils.getOutputMediaFileUri(getActivity(), file);

                                            deleteMediaList = mediaUploadUtil.readMediaUpload(checkedMediaList.get(i));
                                            if (deleteMediaList != null && !deleteMediaList.isEmpty()) {
                                                Iterator<MediaUpload> itr = deleteMediaList.iterator();
                                                while (itr.hasNext()) {
                                                    MediaUpload mediaUpload = new MediaUpload();
                                                    mediaUpload = itr.next();

                                                    if (mediaUpload.getIs_Media_Uploaded() != null && mediaUpload.getIs_Media_Uploaded().equals(Constants.PENDING_DELETE_S3)) {
                                                        try {
                                                            AmazonUtil.deleteObject(Constants.BUCKET_NAME, mediaUpload.getTeam_Id() + "_" + mediaUpload.getTeam_Name() + "/" + file.getName());
                                                        } catch (Exception e) {
                                                            Log.d("Exception catch:", e.getMessage());
                                                        }
                                                        deleteImage(fileUri, file.getAbsolutePath());
                                                        updateUploadedMedia(mediaUpload, Constants.DELETE_S3);
                                                    }
                                                }
                                            }
                                        }
                                    } finally {
                                        if (deleteMediaList != null && !deleteMediaList.isEmpty()) {
                                            deleteMediaList.clear();
                                        }
                                    }
                                }
                            }).start();
                        }
                    });

            // Setting Negative "NO" Btn
            dialog.setNegativeButton("NO",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
        }
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.MarkAll:
                doMark(true);
                return true;
            case R.id.UnmarkAll:
                doUnmark(false);
                return true;
            case R.id.Upload:
                alertDialog(Constants.DISABLE_OPTION_MSG, false);
                //disable this menu
                return true;

            case R.id.Delete:
                // Setting Icon to Dialog

                wifiStatus = NetworkUtil.getConnectivityStatusString(context);
                if (checkedMediaList != null && !checkedMediaList.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    alertDialog(Constants.DELETE_PERM_MSG, true);
                } else if (pendingDeleteMediaListContents != null && pendingDeleteMediaListContents.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    Toast.makeText(getActivity(), "Trash is empty.", Toast.LENGTH_LONG).show();
                } else if (checkedMediaList != null && checkedMediaList.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    Toast.makeText(getActivity(), Constants.SELECT_ATLEAST_MEDIA, Toast.LENGTH_SHORT).show();
                } else if (!mPrefs.getBoolean("value", true) && checkedMediaList != null && !checkedMediaList.isEmpty() && (wifiStatus != 0 || wifiStatus != 1)) {
                    Toast.makeText(getActivity(), Constants.NO_NETWORK, Toast.LENGTH_LONG).show();
                } else {
                    alertDialog(Constants.WARNING_MSG, false);
                }
                mAdapter.deselectAll();
                return true;

            case R.id.Recover:
                if (checkedMediaList != null && !checkedMediaList.isEmpty()) {
                    LinkedHashSet<MediaUpload> recoverMediaList = new LinkedHashSet();

                    try {
                        for (int i = 0; i < checkedMediaList.size(); i++) {

                            recoverMediaList = mediaUploadUtil.readMediaUpload(checkedMediaList.get(i));

                            if (recoverMediaList != null && !recoverMediaList.isEmpty()) {
                                Iterator<MediaUpload> itr = recoverMediaList.iterator();
                                while (itr.hasNext()) {
                                    MediaUpload mediaUpload = new MediaUpload();
                                    mediaUpload = itr.next();

                                    if (mediaUpload.getIs_Media_Uploaded() != null && mediaUpload.getIs_Media_Uploaded().equals(Constants.PENDING_DELETE_S3)) {
                                        final MySharePreference sp = new MySharePreference();
                                        Gson gson = new Gson();
                                        int teamId = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getId();
                                        String teamName = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getName();


                                        if (S3Utils.checkObjectExistInS3(context, teamId + "_" + teamName + "/" + mediaUpload.getMedia_File_Name().substring(mediaUpload.getMedia_File_Name().lastIndexOf('/') + 1))) {
                                            updateUploadedMedia(mediaUpload, Constants.UPLOADED_S3);
                                        } else {
                                            updateUploadedMedia(mediaUpload, Constants.NOT_UPLOADED_S3);
                                        }
                                    }
                                }
                            }
                        }
                    } finally {
                        if (recoverMediaList != null && !recoverMediaList.isEmpty()) {
                            recoverMediaList.clear();
                        }
                    }
                    doUnmark(false);
                } else if (pendingDeleteMediaListContents != null && pendingDeleteMediaListContents.isEmpty()) {
                    Toast.makeText(getActivity(), "Trash is empty.", Toast.LENGTH_LONG).show();
                } else if (checkedMediaList != null && checkedMediaList.isEmpty()) {
                    Toast.makeText(getActivity(), "Please select at least one Media", Toast.LENGTH_SHORT).show();
                } else if (!mPrefs.getBoolean("value", true) && checkedMediaList != null && !checkedMediaList.isEmpty() && (wifiStatus != 0 || wifiStatus != 1)) {
                    Toast.makeText(getActivity(), "Please enable, either wifi or mobile data.", Toast.LENGTH_LONG).show();
                } else {
                    alertDialog(Constants.WARNING_MSG, false);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUploadedMedia(MediaUpload mediaUpload, String status) {
        MediaUpload updateMediaUpload = new MediaUpload();
        updateMediaUpload.setIs_Media_Uploaded(status);
        updateMediaUpload.setMedia_File_Name(mediaUpload.getMedia_File_Name());
        updateMediaUpload.setTeam_Name(mediaUpload.getTeam_Name());
        updateMediaUpload.setTeam_Id(mediaUpload.getTeam_Id());

        mediaUploadUtil.updateMediaUpload(updateMediaUpload);
    }

    private boolean deleteImage(Uri fileUri, String filePath) {
        File fdelete = new File(filePath);
        if (fdelete.exists() && fdelete.delete()) {
            callBroadCast(fileUri, filePath);
            return true;
        }
        return false;
    }

    public void callBroadCast(Uri fileUri, String path) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, fileUri
            ));
        } else {
            String[] filePath = {path};

            MediaScannerConnection.scanFile(context, filePath, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                }
            });

        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
