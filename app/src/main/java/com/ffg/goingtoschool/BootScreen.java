package com.ffg.goingtoschool;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

public class BootScreen extends AppCompatActivity {

    final MySharePreference sp = new MySharePreference();
    Boolean firstTime;

    public static Context contextOfApplication;

    private static  int SCREEN_TIMEOUT = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boot_screen);
        contextOfApplication = getApplicationContext();
        firstTime = sp.getBooleanFromMyPref(Constants.FIRST_TIME, true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (firstTime) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sp.addBooleanToMyPref(Constants.FIRST_TIME, false);
                            Intent i = new Intent(getApplicationContext(), SplashActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }, 0);
                }
                else{
                    startActivity(getStartScreenIntent());
                }
                finish();
            }
        },SCREEN_TIMEOUT);
    }

    public static Context getContextOfApplication(){
        return contextOfApplication;
    }

    private Intent getStartScreenIntent(){
        final Cognito auth = new Cognito(getApplicationContext(), this);

        if(Constants.EMPTY_STRING.equalsIgnoreCase(sp.getStringFromMyPref(Constants.AWS_TOKEN,"")) ||
                auth.isAWSTokenExpired()){
            return new Intent(BootScreen.this, LoginActivity.class);
        }else{
            if(Constants.EMPTY_STRING.equalsIgnoreCase(sp.getStringFromMyPref(Constants.WARD_SP,"")) ||
                    Constants.EMPTY_STRING.equalsIgnoreCase(sp.getStringFromMyPref(Constants.SCHOOL_SP,"")) ||
                    (Constants.STUDENT.equalsIgnoreCase(sp.getStringFromMyPref(Constants.LOGGED_IN_USER_ROLE,"")) &&
                            Constants.EMPTY_STRING.equalsIgnoreCase(sp.getStringFromMyPref(Constants.TEAM_SP,"")))){
                return new Intent(BootScreen.this, LoginActivity.class);
            }else{
                return new Intent(BootScreen.this, MainActivity.class);
            }
        }


    }
}
