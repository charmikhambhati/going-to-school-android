package com.ffg.goingtoschool;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class ImageViewerActivity extends AppCompatActivity {

    private String filepath;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);
        Intent i = getIntent();
        filepath = i.getExtras().getString("filepath");

        imageView = findViewById(R.id.image_view);
        imageView.setImageURI(Uri.parse(filepath));
    }
}
