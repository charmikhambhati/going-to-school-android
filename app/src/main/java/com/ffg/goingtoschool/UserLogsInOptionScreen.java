package com.ffg.goingtoschool;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.amazonaws.util.StringUtils;
import com.ffg.goingtoschool.apiConsumer.APIClient;
import com.ffg.goingtoschool.apiConsumer.APIInterface;
import com.ffg.goingtoschool.apiConsumer.pojo.ResponseWrapper;
import com.ffg.goingtoschool.apiConsumer.pojo.School;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.ffg.goingtoschool.apiConsumer.pojo.Ward;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserLogsInOptionScreen extends AppCompatActivity implements View.OnClickListener{

    private Button newTeamProceed;

    Dialog messageDialog;
    private Button enterTeamDashboard;
    private MaterialBetterSpinner wardSpinner, schoolSpinner ,teamSpinner;
    private String selectedWard, selectedSchool, selectedTeam;
    APIInterface apiInterface;
    final MySharePreference sp = new MySharePreference();
    public LottieAnimationView animation;
    private Map<String, School> schoolMap = null;
    private Map<String, Team> teamMap = null;
    private List<School> schoolList = new ArrayList<>();
    private int selectedSchoolIndex;
    private List<Ward> wards = new ArrayList<>();
    private String wardId;

    private ImageView logoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_logs_in);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        init();

        logoutBtn = (ImageView) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void init(){
        wardSpinner = findViewById(R.id.teamWardSpinner);
        schoolSpinner = findViewById(R.id.teamSchoolSpinner);
        teamSpinner = findViewById(R.id.teamTeamSpinner);

        schoolSpinner.setVisibility(View.INVISIBLE);
        teamSpinner.setVisibility(View.INVISIBLE);

        enterTeamDashboard = findViewById(R.id.enterTeamDash);
        enterTeamDashboard.setOnClickListener(this);
        animation = (LottieAnimationView) findViewById(R.id.thumb_down);
        animation.setRepeatCount(LottieDrawable.INFINITE);
        animation.setVisibility(View.INVISIBLE);
        newTeamProceed = findViewById(R.id.newTeamProceed);
        newTeamProceed.setOnClickListener(this);
        if(!Constants.STUDENT.equalsIgnoreCase(sp.getStringFromMyPref(Constants.LOGGED_IN_USER_ROLE,""))){
            teamSpinner.setVisibility(View.INVISIBLE);
        }
        populateWard();
        wardSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String spinner_value= adapterView.getItemAtPosition(position).toString();
                wardId = String.valueOf(wards.get(position).getId());
                Log.d("the ward id was", wardId);
                if(!StringUtils.isBlank(spinner_value)){
                    selectedWard = spinner_value;
                    populateSchool(spinner_value);
                    newTeamProceed.setVisibility(View.GONE);
                }
            }
        });

        schoolSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String spinner_value= adapterView.getItemAtPosition(position).toString();
                if(!StringUtils.isBlank(spinner_value)){
                    selectedSchool = spinner_value;
                    selectedSchoolIndex = position;
                    if(Constants.STUDENT.equalsIgnoreCase(sp.getStringFromMyPref(Constants.LOGGED_IN_USER_ROLE,""))){
                        teamSpinner.setVisibility(View.VISIBLE);
                        newTeamProceed.setVisibility(View.VISIBLE);
                        populateTeam(spinner_value);
                    }
                }
            }
        });

        teamSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String spinner_value= adapterView.getItemAtPosition(position).toString();
                if(!StringUtils.isBlank(spinner_value)){
                    selectedTeam = spinner_value;
                }
            }
        });

        messageDialog = new Dialog(this);


        logoutBtn = (ImageView) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void populateSchool(String ward) {
        selectedSchool = null;
        if(StringUtils.isBlank(ward)){
            Toast.makeText(this.getBaseContext(), "Please select a ward", Toast.LENGTH_LONG).show();
            return;
        }
        showLoading();
        Call<ResponseWrapper<School>> call = apiInterface.getAllSchoolsInWard(sp.getStringFromMyPref(Constants.AWS_TOKEN,""),ward);
        call.enqueue(new Callback<ResponseWrapper<School>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<School>> call, Response<ResponseWrapper<School>> responseWrapper) {
                Log.d("TAG", responseWrapper.code() + "");
                if (responseWrapper.isSuccessful()) {
                    ResponseWrapper<School> res = responseWrapper.body();
                    if (res.getStatus().equalsIgnoreCase("success")) {
                        List<School> schools = res.getData();
                        Log.d("TAG", "Schools -> " + schools.size());

                        if(schools.size() == 0){
                            showPopup("No schools associated with selected ward", false);

                            schoolSpinner.setVisibility(View.INVISIBLE);
                            teamSpinner.setVisibility(View.INVISIBLE);
                            ArrayAdapter<String> schoolAdapter = new ArrayAdapter<String>(UserLogsInOptionScreen.this, android.R.layout.simple_spinner_dropdown_item, new ArrayList<String>());
                            schoolSpinner.setAdapter(schoolAdapter);
                            hideLoading();
                            return;
                        }
                        schoolSpinner.setVisibility(View.VISIBLE);
                        schoolMap = new HashMap<String, School>();
                        schoolList.clear();
                        List<String> schoolStringList = new ArrayList<>();
                        for (School school : schools) {
                            schoolStringList.add(school.getName());
                            schoolMap.put(school.getName(),school);
                            schoolList.add(school);
                        }
                        ArrayAdapter<String> schoolAdapter = new ArrayAdapter<String>(UserLogsInOptionScreen.this, android.R.layout.simple_spinner_dropdown_item, schoolStringList);
                        schoolSpinner.setAdapter(schoolAdapter);
                        hideLoading();
                    }
                } else {
                    hideLoading();
                    showPopup("Error while fetching schools", false);
                    Log.e("TAG", responseWrapper.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<School>> call, Throwable t) {
                call.cancel();
                hideLoading();
                showPopup("Error while fetching schools", false);
            }
        });
    }

    private void populateTeam(String school) {

        selectedTeam = null;
        showLoading();
        Team team = new Team();
        team.setSchool_id(((School) schoolMap.get(school)).getId());

        Call<ResponseWrapper<Team>> call = apiInterface.getTeamByTeamName(sp.getStringFromMyPref(Constants.AWS_TOKEN,""),team);
        call.enqueue(new Callback<ResponseWrapper<Team>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Team>> call, Response<ResponseWrapper<Team>> responseWrapper) {
                Log.d("TAG", responseWrapper.code()+"");
                if(responseWrapper.isSuccessful()){
                    ResponseWrapper<Team> res = responseWrapper.body();
                    if(res.getStatus().equalsIgnoreCase("success")){
                        List<Team> teams = res.getData();
                        Log.d("TAG","Teams -> " + teams.size());

                        if(teams.size() == 0){
                            Toast.makeText(getApplicationContext(), "No Teams associated with selected school", Toast.LENGTH_SHORT).show();
                            teamSpinner.setVisibility(View.INVISIBLE);
                            ArrayAdapter<String> teamAdapter = new ArrayAdapter<String>(UserLogsInOptionScreen.this, android.R.layout.simple_spinner_dropdown_item, new ArrayList<String>());
                            teamSpinner.setAdapter(teamAdapter);
                            hideLoading();
                            return;
                        }

                        teamMap = new HashMap<String, Team>();
                        List<String> teamStringList = new ArrayList<>();
                        for (Team team :teams) {
                            teamStringList.add(team.getName());
                            teamMap.put(team.getName(),team);
                        }
                        ArrayAdapter<String> teamAdapter = new ArrayAdapter<String>(UserLogsInOptionScreen.this, android.R.layout.simple_spinner_dropdown_item, teamStringList);
                        teamSpinner.setAdapter(teamAdapter);
                        hideLoading();
                    }
                }else{
                    hideLoading();
                    showPopup("Error while fetching wards", false);
                    Log.e("TAG", responseWrapper.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Team>> call, Throwable t) {
                call.cancel();
                hideLoading();
                showPopup("Error while fetching teams", false);
            }
        });
    }

    private void populateWard() {
        showLoading();
        Call<ResponseWrapper<Ward>> call = apiInterface.getAllWards(sp.getStringFromMyPref(Constants.AWS_TOKEN,""));
        call.enqueue(new Callback<ResponseWrapper<Ward>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Ward>> call, Response<ResponseWrapper<Ward>> responseWrapper) {
                Log.d("TAG", responseWrapper.code()+"");
                if(responseWrapper.isSuccessful()){
                    ResponseWrapper<Ward> res = responseWrapper.body();
                    if(res.getStatus().equalsIgnoreCase("success")){
                        wards = res.getData();
                        Log.d("TAG","Wards -> " + wards.size());

                        List<String> wardStringList = new ArrayList<>();
                        for (Ward ward :wards) {
                            wardStringList.add(ward.getName());
                        }
                        ArrayAdapter<String> wardAdapter = new ArrayAdapter<String>(UserLogsInOptionScreen.this, android.R.layout.simple_spinner_dropdown_item, wardStringList);
                        wardSpinner.setAdapter(wardAdapter);
                        hideLoading();
                    }
                }else{
                    hideLoading();
                    Toast.makeText(getApplicationContext(), "Error while fetching wards", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", responseWrapper.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Ward>> call, Throwable t) {
                call.cancel();
                hideLoading();
                Toast.makeText(getApplicationContext(), "Error while fetching wards", Toast.LENGTH_SHORT).show();
                showPopup("Error while fetching wards", false);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.enterTeamDash:
                moveToTeamDashboard();
                break;
           case R.id.newTeamProceed:
                newTeamCreate();
                break;
        }
    }

    private void newTeamCreate() {
        if(!StringUtils.isBlank(selectedSchool)){
            Intent teamCreationActivity = new Intent(this, BasicTeamCreation.class);
            teamCreationActivity.putExtra("selected_school", ((School) schoolMap.get(selectedSchool)).getId());
            startActivity(teamCreationActivity);
        }else{
            Toast.makeText(UserLogsInOptionScreen.this, "Please select a School", Toast.LENGTH_SHORT).show();
        }
    }

    public void showLoading() {
        animation.setVisibility(View.VISIBLE);
        animation = (LottieAnimationView) findViewById(R.id.thumb_down);
        animation.setRepeatCount(LottieDrawable.INFINITE);
        animation.playAnimation();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        animation.pauseAnimation();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        animation.setVisibility(View.INVISIBLE);
    }

    private void moveToTeamDashboard() {
        if(StringUtils.isBlank(selectedWard)
                || StringUtils.isBlank(selectedSchool)
                || (Constants.STUDENT.equalsIgnoreCase(sp.getStringFromMyPref(Constants.LOGGED_IN_USER_ROLE,"")) &&
                StringUtils.isBlank(selectedTeam))){
            AlertDialog alert;
            AlertDialog.Builder builder = new   AlertDialog.Builder(UserLogsInOptionScreen.this);
            builder.setMessage("Please select all fields to proceed")
                    .setTitle("Alert")
                    .setCancelable(true);

            builder.setPositiveButton("ok",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   dialog.cancel();
                }
            });
            alert = builder.create();
            alert.show();
        }else{
            sp.addStringToMyPref(Constants.WARD_SP, String.valueOf(selectedWard));
            sp.addStringToMyPref(Constants.WARD_ID_SP, wardId);
            sp.addSchoolToMyPref(Constants.SCHOOL_SP, schoolList.get(selectedSchoolIndex));



            if(!Constants.STUDENT.equalsIgnoreCase(sp.getStringFromMyPref(Constants.LOGGED_IN_USER_ROLE,""))){
               //fetch school default team ID
                Team team = new Team();
                team.setSchool_id(schoolList.get(selectedSchoolIndex).getId());
                Call<ResponseWrapper<Team>> call = apiInterface.getTeamByTeamName(sp.getStringFromMyPref(Constants.AWS_TOKEN,"") , team);
                call.enqueue(new Callback<ResponseWrapper<Team>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<Team>> call, Response<ResponseWrapper<Team>> responseWrapper) {
                        if(responseWrapper.isSuccessful()){
                            ResponseWrapper<Team> res = responseWrapper.body();
                            if(res.getStatus().equalsIgnoreCase("success")){
                                List<Team> team = res.getData();
                                Log.d("TAG","Team -> " + team.size());

                                if (team.isEmpty()) {
                                    Toast.makeText(getApplicationContext(), "No Default team exists for school :: "+schoolList.get(selectedSchoolIndex).getId(), Toast.LENGTH_SHORT).show();
                                    return;
                                }else{
                                    sp.addTeamToMyPref(Constants.TEAM_SP, team.get(0));
                                    Intent teamDashActivity = new Intent(UserLogsInOptionScreen.this, MainActivity.class);
                                    teamDashActivity.putExtra("selected_team", String.valueOf(selectedTeam));
                                    startActivity(teamDashActivity);
                                }
                            }
                        }else{
                            showPopup("Error while fetching teams", false);
                            Log.e("TAG", responseWrapper.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<Team>> call, Throwable t) {
                        call.cancel();
                        Toast.makeText(getApplicationContext(), "Error while fetching team", Toast.LENGTH_SHORT).show();
                    }
                });
            }else{
                sp.addTeamToMyPref(Constants.TEAM_SP, teamMap.get(String.valueOf(selectedTeam)));
                Intent teamDashActivity = new Intent(UserLogsInOptionScreen.this, MainActivity.class);
                teamDashActivity.putExtra("selected_team", String.valueOf(selectedTeam));
                startActivity(teamDashActivity);
            }

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserLogsInOptionScreen.this, LoginActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void showPopup(String msg, Boolean isSuccess) {
        if(isSuccess){
            messageDialog.setContentView(R.layout.popup_message_positive);
        }
        else{
            messageDialog.setContentView(R.layout.popup_message_negative);
        }
        ImageView closeImg = (ImageView)messageDialog.findViewById(R.id.closePopupImg);
        Button closeBtn = messageDialog.findViewById(R.id.closePopupButton);
        TextView alertMsg = messageDialog.findViewById(R.id.message);
        alertMsg.setText(msg);

        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        messageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        messageDialog.show();
    }

    private void logout() {
        android.app.AlertDialog alert;
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(UserLogsInOptionScreen.this);
        builder.setMessage("Sign out ?")
                .setTitle("Alert")
                .setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Cognito cognito = new Cognito(UserLogsInOptionScreen.this, UserLogsInOptionScreen.this);
                if (cognito.signOut()) {
                    Log.d(this.getClass().getSimpleName(), "Redirecting to Login Page");
                    Intent loginActivity = new Intent(UserLogsInOptionScreen.this, LoginActivity.class);
                    startActivity(loginActivity);
                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(UserLogsInOptionScreen.this, "Sign out Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
        alert = builder.create();

        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Context context = UserLogsInOptionScreen.this;

                Button negButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                negButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                negButton.setTextColor(context.getResources().getColor(R.color.themeRed));

                Button posButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                posButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                posButton.setTextColor(context.getResources().getColor(R.color.themeYellow));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(20, 0, 0, 0);
                negButton.setLayoutParams(params);
            }
        });

        alert.show();
    }

}
