package com.ffg.goingtoschool;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ffg.goingtoschool.S3.S3Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ImageViewHolder> {
    private ArrayList<String> images;
    private String imageBaseDir;
    private Context m_context;
    public RecyclerAdapter(ArrayList<String> images, Context m_context){
        this.images = images;
        this.m_context = m_context;
        imageBaseDir = Environment.getExternalStorageDirectory().toString() + "/Pictures/" + Constants.GALLERY_DIRECTORY_NAME;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_image_layout, parent, false);
        ImageViewHolder imageViewHolder = new ImageViewHolder(view);
        return imageViewHolder;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
       String image_path = imageBaseDir + "/" + images.get(position);
        File image = new File(image_path);
        Uri image_uri;
        if(!image.exists()){
            image_path = getSignedUrlsForImages(images.get(position));
        }
        Glide.with(m_context)
                .asBitmap()
                .load(Uri.fromFile(new File(image_path)))
                .placeholder(R.drawable.ic_error_outline_black_24dp)
                .thumbnail(0.2f)
                .into(holder.thumbnail);
    }
    @Override
    public int getItemCount() {
        return images.size();
    }

    private String getSignedUrlsForImages(final String fileName){
        String returnValue = null;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> result = executor.submit(new Callable<String>() {
            public String call() throws Exception {
                return S3Utils.generates3ShareUrlFromFileName(m_context, fileName);
            }
        });
        try {
            returnValue = result.get();
        } catch (Exception exception) {
            //handle exception
        }
        return returnValue;
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder{
        ImageView thumbnail;
        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            thumbnail = itemView.findViewById(R.id.thumbnail);
        }
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RecyclerAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final RecyclerAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildLayoutPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildLayoutPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
