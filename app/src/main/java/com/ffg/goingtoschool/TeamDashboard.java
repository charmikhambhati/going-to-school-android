package com.ffg.goingtoschool;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

public class TeamDashboard extends Fragment implements  View.OnClickListener{

    private RelativeLayout teamCapture,teamUpload;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_team_dashboard, null);
        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        teamCapture = (RelativeLayout) view.findViewById(R.id.teamCapture);
        teamUpload = (RelativeLayout) view.findViewById(R.id.teamUpload);
        teamCapture.setOnClickListener(this);
        teamUpload.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.teamCapture:
                gotoCaptureContent();
                break;
            case R.id.teamUpload:
                gotoUploadFiles();
                break;
            default:
                break;
        }
    }

    private void gotoCaptureContent() {
        Intent captureActivity = new Intent(getActivity().getApplicationContext(), CaptureContentActivity.class);
        startActivity(captureActivity);
        return;
    }

    private void gotoUploadFiles() {
        Intent intent = new Intent(getActivity().getApplicationContext(), VideoGalleryActivity.class);
        startActivity(intent);
    }

}
