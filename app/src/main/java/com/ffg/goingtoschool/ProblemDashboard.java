package com.ffg.goingtoschool;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.ffg.goingtoschool.Animations.AlphaPageTransformer;
import com.ffg.goingtoschool.ProblemDashBoardArtifacts.ProblemDashboardAdapter;
import com.ffg.goingtoschool.apiConsumer.APIClient;
import com.ffg.goingtoschool.apiConsumer.APIInterface;
import com.ffg.goingtoschool.apiConsumer.pojo.ProblemDashboardPojo;
import com.ffg.goingtoschool.apiConsumer.pojo.ResponseWrapper;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProblemDashboard extends Fragment {

    ViewPager viewPager;
    ProblemDashboardAdapter adapter;
    List<com.ffg.goingtoschool.ProblemDashBoardArtifacts.ProblemDashboard> problemDashboards;
    APIInterface apiInterface;
    final MySharePreference sp = new MySharePreference();
    private MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_problem_dashboard, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        problemDashboards = new ArrayList<>();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mainActivity = (MainActivity) getActivity();
        viewPager = view.findViewById(R.id.viewPager);
        fetchAllProblems();
    }

    @Override
     public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void fetchAllProblems() {
        final List<com.ffg.goingtoschool.ProblemDashBoardArtifacts.ProblemDashboard> problemDashboards = new ArrayList<>();
        com.ffg.goingtoschool.ProblemDashBoardArtifacts.ProblemDashboard createDashboard = new com.ffg.goingtoschool.ProblemDashBoardArtifacts.ProblemDashboard(0,"download.png","Create New Dashboard","");
        problemDashboards.add(createDashboard);

        mainActivity.showLoading();

        Gson gson = new Gson();
        ProblemDashboardPojo pojo = new ProblemDashboardPojo();
        String team = sp.getStringFromMyPref(Constants.TEAM_SP, "");
        if (team != null && !"".equals(team)) {
            pojo.setTeam((gson.fromJson(team, Team.class)).getId());
        } else {
            Toast.makeText(getActivity(), Constants.DASHBOARD_FETCH_ERROR, Toast.LENGTH_SHORT).show();
            mainActivity.hideLoading();
            return;
        }

        Call<ResponseWrapper<ProblemDashboardPojo>> call = apiInterface.fetchProblemDashboard(sp.getStringFromMyPref(Constants.AWS_TOKEN,""),pojo);
        call.enqueue(new Callback<ResponseWrapper<ProblemDashboardPojo>>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseWrapper<ProblemDashboardPojo>> call, Response<ResponseWrapper<ProblemDashboardPojo>> responseWrapper) {
                Log.d("TAG", responseWrapper.code() + "");
                if (responseWrapper.isSuccessful()) {
                    ResponseWrapper<ProblemDashboardPojo> res = responseWrapper.body();
                    if (res != null && res.getStatus() != null && res.getStatus().equalsIgnoreCase("success")) {
                        List<ProblemDashboardPojo> problems = res.getData();

                        if(problems.isEmpty()){
                            mainActivity.hideLoading();
                        }
                        for (ProblemDashboardPojo problem : problems) {
                            com.ffg.goingtoschool.ProblemDashBoardArtifacts.ProblemDashboard problemDashboard = new com.ffg.goingtoschool.ProblemDashBoardArtifacts.ProblemDashboard(problem.getId(),problem.getCover_picture(),problem.getName(),problem.getSchool()+" "+problem.getWard());
                            problemDashboards.add(problemDashboard);
                        }

                        adapter = new ProblemDashboardAdapter(problemDashboards, getActivity());
                        viewPager.setAdapter(adapter);
                        viewPager.setPageTransformer(true, new AlphaPageTransformer());
                        viewPager.setPadding(130, 0,130,0);
                        mainActivity.hideLoading();
                    }
                    mainActivity.hideLoading();
                } else {
                    Toast.makeText(getActivity(), Constants.DASHBOARD_FETCH_ERROR, Toast.LENGTH_SHORT).show();
                    Log.e("TAG", responseWrapper.message());
                    mainActivity.hideLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ProblemDashboardPojo>> call, Throwable t) {
                call.cancel();
                Toast.makeText(getActivity(), Constants.DASHBOARD_FETCH_ERROR, Toast.LENGTH_SHORT).show();
                mainActivity.hideLoading();
            }
        });
    }

}
