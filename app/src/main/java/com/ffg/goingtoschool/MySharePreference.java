package com.ffg.goingtoschool;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.ffg.goingtoschool.apiConsumer.pojo.School;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.google.gson.Gson;

public class MySharePreference {

    private static SharedPreferences mySharedPref;


    private SharedPreferences getMySharePreference(){
        if(mySharedPref == null){
            Log.d(this.getClass().getSimpleName(),"Creating my shared preference for first time");
            mySharedPref =  BootScreen.getContextOfApplication().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        }
        Log.d(this.getClass().getSimpleName(),"SP exists so returning instance");
        return mySharedPref;
    }

    public void addStringToMyPref(String key, String value){
        SharedPreferences.Editor editor = getMySharePreference().edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
        Log.d(this.getClass().getSimpleName(),"Adding "+key+"/"+value+" pair to SP");
    }

    public String getStringFromMyPref(String key,String defaulValue){
        Log.d(this.getClass().getSimpleName(),"returning ## "+getMySharePreference().getString(key,defaulValue == null ? "" : defaulValue));
        return getMySharePreference().getString(key,defaulValue);
    }

    public void addBooleanToMyPref(String key, boolean value){
        SharedPreferences.Editor editor = getMySharePreference().edit();
        editor.putBoolean(key, value);
        editor.apply();
        editor.commit();
        Log.d(this.getClass().getSimpleName(),"Adding "+key+"/"+value+" pair to SP");
    }

    public boolean getBooleanFromMyPref(String key, boolean defaulValue){
        Log.d(this.getClass().getSimpleName(),"returning ## "+getMySharePreference().getBoolean(key,defaulValue ));
        return getMySharePreference().getBoolean(key,defaulValue);
    }

    public void addLongToMyPref(String key, Long value){
        SharedPreferences.Editor editor = getMySharePreference().edit();
        editor.putLong(key, value);
        editor.apply();
        editor.commit();
        Log.d(this.getClass().getSimpleName(),"Adding "+key+"/"+value+" pair to SP");
    }

    public Long getLongFromMyPref(String key,Long defaulValue){
        Log.d(this.getClass().getSimpleName(),"returning ## "+getMySharePreference().getLong(key,defaulValue));
        return getMySharePreference().getLong(key,defaulValue);
    }

    public void addSchoolToMyPref(String key, School school) {
        SharedPreferences.Editor editor = getMySharePreference().edit();
        Gson gson = new Gson();
        String schoolValue = gson.toJson(school);

        editor.putString(key, schoolValue);
        editor.apply();
        editor.commit();
        Log.d(this.getClass().getSimpleName(),"Adding "+key+"/"+schoolValue+" pair to SP");
    }

    public void addTeamToMyPref(String key, Team team) {
        SharedPreferences.Editor editor = getMySharePreference().edit();
        Gson gson = new Gson();
        String teamValue = gson.toJson(team);

        editor.putString(key, teamValue);
        editor.apply();
        editor.commit();
        Log.d(this.getClass().getSimpleName(),"Adding "+key+"/"+teamValue+" pair to SP");
    }

    public void addWardDetailsToMyPref(String key, String value) {
        SharedPreferences.Editor editor = getMySharePreference().edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
        Log.d(this.getClass().getSimpleName(),"Adding "+key+"/"+value+" pair to SP");
    }

    public boolean destroySharedPref(){
        try{
            SharedPreferences.Editor editor = getMySharePreference().edit();
            editor.remove(Constants.AWS_TOKEN);
            editor.remove(Constants.LOGGED_IN_USER);
            editor.remove(Constants.TOKEN_EXPIRY_TIME);
            editor.remove(Constants.SCHOOL_SP);
            editor.remove(Constants.WARD_SP);
            editor.remove(Constants.TEAM_SP);
            editor.remove(Constants.WARD_ID_SP);
            editor.commit();
            return true;
        }catch(Exception e){
            Log.e(this.getClass().getSimpleName(),"Something went wrong while clearing SP");
            return false;
        }
    }


}
