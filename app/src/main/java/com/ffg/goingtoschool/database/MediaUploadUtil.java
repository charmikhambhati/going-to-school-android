package com.ffg.goingtoschool.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ffg.goingtoschool.Constants;
import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class MediaUploadUtil extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "goingToSchoolDB.db";
    public static final int version = 1;

    public MediaUploadUtil(Context context) {
        super(context, DATABASE_NAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String dbQuery = "CREATE TABLE IF NOT EXISTS MediaUpload (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Media_File_Name TEXT UNIQUE, Is_Media_Uploaded TEXT, Team_Id INTEGER, Team_Name TEXT )";
        sqLiteDatabase.execSQL(dbQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS MediaUpload");
        onCreate(sqLiteDatabase);
    }

    public void insertMediaUpload(MediaUpload MediaUpload) {

        SQLiteDatabase db = getWritableDatabase();
        try {
            String query = "INSERT INTO MediaUpload (Media_File_Name, Is_Media_Uploaded, Team_name, Team_Id) VALUES (?,?,?,?)";
            db.execSQL(query, new String[]{MediaUpload.getMedia_File_Name(), MediaUpload.getIs_Media_Uploaded(), MediaUpload.getTeam_Name(), MediaUpload.getTeam_Id() + ""});
        } catch (Exception e) {
            Log.d("Exception:", e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public void updateMediaUpload(MediaUpload MediaUpload) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("Is_Media_Uploaded", MediaUpload.getIs_Media_Uploaded());
            String whereClause = "Media_File_Name=?";
            String whereArgs[] = {MediaUpload.getMedia_File_Name()};
            db.update("MediaUpload", contentValues, whereClause, whereArgs);
        } catch (Exception e) {
            Log.d("Exception:", e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public void deleteMediaUpload(MediaUpload MediaUpload) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            String whereClause = "Media_File_Name=?";
            String whereArgs[] = {MediaUpload.getMedia_File_Name()};
            db.delete("MediaUpload", whereClause, whereArgs);
        } catch (Exception e) {
            Log.d("Exception:", e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    //Query to read particular file
    public LinkedHashSet<MediaUpload> readMediaUpload(String mediaFileName) {
        LinkedHashSet<MediaUpload> mediaList = new LinkedHashSet<>();
        SQLiteDatabase db = getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from MediaUpload WHERE Media_File_Name = '" + mediaFileName + "'", null);
            cursor.moveToFirst();

            if (cursor != null && cursor.getCount() > 0) {
                do {
                    MediaUpload MediaUpload = readMedia(cursor);
                    mediaList.add(MediaUpload);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("Exception", e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mediaList;
    }

    private MediaUpload readMedia(Cursor cursor) {
        MediaUpload mediaUpload = new MediaUpload();

        mediaUpload.setId(cursor.getInt(cursor.getColumnIndex("Id")));
        mediaUpload.setMedia_File_Name(cursor.getString(cursor.getColumnIndex("Media_File_Name")));
        mediaUpload.setIs_Media_Uploaded(cursor.getString(cursor.getColumnIndex("Is_Media_Uploaded")));
        mediaUpload.setTeam_Id(cursor.getInt(cursor.getColumnIndex("Team_Id")));
        mediaUpload.setTeam_Name(cursor.getString(cursor.getColumnIndex("Team_Name")));

        return mediaUpload;
    }

    //Query to read all media irrespective of status
    public ArrayList<MediaUpload> readAllMediaUpload() {
        ArrayList<MediaUpload> mediaList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery("select * from MediaUpload ", null);
            cursor.moveToFirst();

            if (cursor != null && cursor.getCount() > 0) {
                do {
                    MediaUpload MediaUpload = readMedia(cursor);
                    mediaList.add(MediaUpload);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("Exception", e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mediaList;
    }

    //Query to real media which are pending for upload
    public ArrayList<MediaUpload> readPendingUploadMedia() {
        ArrayList<MediaUpload> mediaList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        try {
            Cursor cursor = db.query("MediaUpload", null, "Is_Media_Uploaded" + "=?", new String[]{Constants.NOT_UPLOADED_S3}, null, null, null);
            cursor.moveToFirst();

            if (cursor != null && cursor.getCount() > 0) {
                do {
                    MediaUpload MediaUpload = readMedia(cursor);
                    mediaList.add(MediaUpload);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("Exception", e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mediaList;
    }

    //Query to real media which are pending for upload
    public ArrayList<MediaUpload> readUploadedMedia() {
        ArrayList<MediaUpload> mediaList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        try {
            Cursor cursor = db.query("MediaUpload", null, "Is_Media_Uploaded" + "=?", new String[]{Constants.UPLOADED_S3}, null, null, null);
            cursor.moveToFirst();

            if (cursor != null && cursor.getCount() > 0) {
                do {
                    MediaUpload MediaUpload = readMedia(cursor);
                    mediaList.add(MediaUpload);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("Exception", e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mediaList;
    }

    //Query to real media which are pending for delete, i.e. available in Recycle bin
    public LinkedHashSet<MediaUpload> readPendingDeleteMedia() {
        LinkedHashSet<MediaUpload> mediaList = new LinkedHashSet<>();
        SQLiteDatabase db = getReadableDatabase();

        try {
            Cursor cursor = db.query("MediaUpload", null, "Is_Media_Uploaded" + "=?", new String[]{Constants.PENDING_DELETE_S3}, null, null, null);
            cursor.moveToFirst();

            if (cursor != null && cursor.getCount() > 0) {
                do {
                    MediaUpload MediaUpload = readMedia(cursor);
                    mediaList.add(MediaUpload);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("Exception", e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mediaList;
    }
}
