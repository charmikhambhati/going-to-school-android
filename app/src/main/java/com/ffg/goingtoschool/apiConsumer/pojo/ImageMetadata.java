package com.ffg.goingtoschool.apiConsumer.pojo;

import java.util.ArrayList;

public class ImageMetadata {
    private int image_id;
    private ArrayList<String> image_names;
    private String image_name;
    private int problem_dashboard_id;
    private int team_id;

    public ImageMetadata(){
    }
    public ImageMetadata(int image_id, ArrayList<String> image_names, int problem_dashboard_id, int team_id){
        this.image_id = image_id;
        this.image_names = image_names;
        this.problem_dashboard_id = problem_dashboard_id;
        this.team_id = team_id;
    }
    public ImageMetadata(int image_id, String image_name, int problem_dashboard_id, int team_id){
        this.image_id = image_id;
        this.image_names = new ArrayList<>();
        image_names.add(image_name);
        this.image_name = image_name;
        this.problem_dashboard_id = problem_dashboard_id;
        this.team_id = team_id;
    }
    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public String getImage_name() {
        return image_name;
    }
    public void setImage_name(String image_name) {
        this.image_name = image_name;
        this.image_names.add(0, image_name);
    }

    public int getProblem_dashboard_id() {
        return problem_dashboard_id;
    }

    public void setProblem_dashboard_id(int problem_dashboard_id) {
        this.problem_dashboard_id = problem_dashboard_id;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public ArrayList<String> getImage_names(){
        return this.image_names;
    }

    public void setImage_names(ArrayList<String> image_names) {
        this.image_names = image_names;
    }
}
