package com.ffg.goingtoschool.apiConsumer.pojo;

import java.util.Objects;

public class MediaUpload {

    private int Id;
    private String Media_File_Name;
    private String Is_Media_Uploaded;
    private int Team_Id;
    private String Team_Name;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getMedia_File_Name() {
        return Media_File_Name;
    }

    public void setMedia_File_Name(String media_File_Name) {
        Media_File_Name = media_File_Name;
    }

    public String getIs_Media_Uploaded() {
        return Is_Media_Uploaded;
    }

    public void setIs_Media_Uploaded(String is_Media_Uploaded) {
        Is_Media_Uploaded = is_Media_Uploaded;
    }

    public int getTeam_Id() {
        return Team_Id;
    }

    public void setTeam_Id(int team_Id) {
        Team_Id = team_Id;
    }

    public String getTeam_Name() {
        return Team_Name;
    }

    public void setTeam_Name(String team_Name) {
        Team_Name = team_Name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MediaUpload that = (MediaUpload) o;
        return Team_Id == that.Team_Id &&
                Media_File_Name.equals(that.Media_File_Name) &&
                Is_Media_Uploaded.equals(that.Is_Media_Uploaded) &&
                Team_Name.equals(that.Team_Name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Media_File_Name, Is_Media_Uploaded, Team_Id, Team_Name);
    }
}
