package com.ffg.goingtoschool.apiConsumer.pojo;

public class Ward {
    private int id;
    private String name;
    private Number contact;
    private String city;
    private String state;

    public Ward(int id, String name, Number contact, String city, String state) {
        this.id = id;
        this.name = name;
        this.contact = contact;
        this.city = city;
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Number getContact() {
        return contact;
    }

    public void setContact(Number contact) {
        this.contact = contact;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
