package com.ffg.goingtoschool.apiConsumer;

import com.ffg.goingtoschool.Constants;
import com.ffg.goingtoschool.apiConsumer.pojo.DashboardUpdatePojo;
import com.ffg.goingtoschool.apiConsumer.pojo.ImageMetadata;
import com.ffg.goingtoschool.apiConsumer.pojo.ProblemDashboardPojo;
import com.ffg.goingtoschool.apiConsumer.pojo.ResponseWrapper;
import com.ffg.goingtoschool.apiConsumer.pojo.School;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.ffg.goingtoschool.apiConsumer.pojo.User;
import com.ffg.goingtoschool.apiConsumer.pojo.Ward;
import com.ffg.goingtoschool.apiConsumer.pojo.Workshop;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Interface to call AWS service, get response
 * and the map to the assigned pojo class
 */
public interface APIInterface {

    /***
     * @param username
     * @return User data returned from AWS
     */
    @GET(Constants.GET_USER_DATA_BY_USERNAME)
    Call<ResponseWrapper<User>> getUserDataByUserName(@Header("Authorization") String token, @Query("username") String username);

    /***
     * @return Returns all wards available
     */
    @GET(Constants.GET_ALL_WARDS)
    Call<ResponseWrapper<Ward>> getAllWards(@Header("Authorization") String token);

    /***
     * @return Returns all schools available
     */
    @GET(Constants.GET_ALL_SCHOOLS)
    Call<ResponseWrapper<School>> getAllSchools(@Header("Authorization") String token);

    /***
     * @return Returns all schools in a ward
     */
    @GET(Constants.GET_ALL_SCHOOLS)
    Call<ResponseWrapper<School>> getAllSchoolsInWard(@Header("Authorization") String token, @Query("ward") String ward);

    /***
     * @return Create team
     */
    @POST(Constants.CREATE_TEAM)
    Call<Team> createTeam(@Header("Authorization") String token, @Body Team team);

    /***
     * @return Update team
     */
    @POST(Constants.UPDATE_TEAM)
    Call<Team> updateTeam(@Header("Authorization") String token, @Body Team team);

    /***
     * @return Returns all teams available
     */
    @GET(Constants.GET_TEAMS)
    Call<ResponseWrapper<Team>> getAllTeams(@Header("Authorization") String token);

    /***
     * @return Returns all teams by team name
     */
    @POST(Constants.GET_TEAMS)
    Call<ResponseWrapper<Team>> getTeamByTeamName(@Header("Authorization") String token,@Body Team team);

    /***
     * @return Returns all problems available
     */
    @GET(Constants.GET_ALL_PROBLEMS)
    Call<ResponseWrapper<ProblemDashboardPojo>> getAllProblems(@Header("Authorization") String token);
   /* *
     *
     * @return Return all workshops available
     */
    @GET(Constants.GET_ALL_WORKSHOPS)
    Call<ResponseWrapper<Workshop>> getWorkshops(@Header("Authorization") String token, @Query("school") String school, @Query("ward") String ward);
    /**
     *
     * @return Return updated workshop
     */
    @POST(Constants.UPDATE_WORKSHOP)
    Call<Workshop> updateWorkshop(@Header("Authorization") String token, @Body Workshop workshop);

    /***
     * @param user
     * @return Register user
     */
    @POST(Constants.REGISTER_USER)
    Call<ResponseWrapper<User>> registerUser(@Body User user);

    /***
     * @return Create problem dashboard
     */
    @POST(Constants.CREATE_DASHBOARD)
    Call<ProblemDashboardPojo> createProblemDashboard(@Header("Authorization") String token, @Body ProblemDashboardPojo ProblemDashboardPojo);

    /***
     * @return Fetch problem dashboard
     */
    @POST(Constants.FETCH_DASHBOARD)
    Call<ResponseWrapper<ProblemDashboardPojo>> fetchProblemDashboard(@Header("Authorization") String token, @Body ProblemDashboardPojo ProblemDashboardPojo);

    /***
     * @return update problem dashboard
     */
    @POST(Constants.UPDATE_DASHBOARD)
    Call<DashboardUpdatePojo> updateProblemDashboard(@Header("Authorization") String token, @Body DashboardUpdatePojo problemDashboardPojo);

    /***
     * @return images associated with team
     */
    @GET(Constants.FETCH_IMAGES_WITH_TEAM)
    Call<ResponseWrapper<ImageMetadata>> fetchImages(@Header("Authorization") String token, @Query("dashboardId") int dashboardId, @Query("team_id") int team_id);

    /***
     * @return create image metadata
     */
    @POST(Constants.CREATE_IMAGE_METADATA)
    Call<ResponseWrapper<ImageMetadata>> createImageMetadata(@Header("Authorization") String token, @Body ImageMetadata imageMetadata);

    /***
     * @return removed image from dashboard
     */
    @POST(Constants.REMOVE_IMAGE_FROM_DASHBOARD)
    Call<ResponseWrapper<ImageMetadata>> removeImageFromDashboard(@Header("Authorization") String token, @Body ImageMetadata imageMetadata);
}
