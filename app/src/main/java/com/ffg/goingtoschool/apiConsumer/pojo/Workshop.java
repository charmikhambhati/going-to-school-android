package com.ffg.goingtoschool.apiConsumer.pojo;

public class Workshop {

    private String name;
    private String status;
    private Integer school;
    private Integer ward;
    private Integer id;

    public Workshop(Integer schoolId, Integer wardId, Integer workshopId, String status) {
        this.status = status;
        this.school = schoolId;
        this.ward = wardId;
        this.id = workshopId;
    }

    public Integer getSchool() {
        return school;
    }

    public void setSchool(Integer school) {
        this.school = school;
    }

    public Integer getWard() {
        return ward;
    }

    public void setWard(Integer ward) {
        this.ward = ward;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Workshop(String name, String status, Integer workshopId) {
        this.name = name;
        this.status = status;
        this.id = workshopId;
    }

    public Workshop(String name, String status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
