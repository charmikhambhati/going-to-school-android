package com.ffg.goingtoschool.apiConsumer.pojo;

public class DashboardUpdatePojo {
    Condition condition;
    Update update;

    public DashboardUpdatePojo(Condition condition, Update update) {
        this.condition = condition;
        this.update = update;
    }
}
