package com.ffg.goingtoschool.apiConsumer.pojo;

public class ProblemDashboardPojo {
    private int id;
    private String name;
    private int school;
    private int ward;

    private String cover_picture;
    private String description;
    private String action;
    private int team;

    public ProblemDashboardPojo() {
    }

    public ProblemDashboardPojo(String name, int school, int ward, String description) {
        this.name = name;
        this.school = school;
        this.ward = ward;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSchool() {
        return school;
    }

    public void setSchool(int school) {
        this.school = school;
    }

    public int getWard() {
        return ward;
    }

    public void setWard(int ward) {
        this.ward = ward;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCover_picture() {
        return cover_picture;
    }

    public void setCover_picture(String cover_picture) {
        this.cover_picture = cover_picture;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }
}
