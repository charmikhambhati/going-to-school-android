package com.ffg.goingtoschool.apiConsumer.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseWrapper<T> {
    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private List<T> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
