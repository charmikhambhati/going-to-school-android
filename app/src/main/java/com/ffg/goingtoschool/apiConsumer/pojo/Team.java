package com.ffg.goingtoschool.apiConsumer.pojo;

import java.util.Date;

public class Team {
    private int id;
    private String name;
    private String members;
    private String profilepicture;
    private String created_by;
    private Date created_date;
    private int school_id;

    public Team(String name, String members,String profilepicture ,String created_by,int school_id) {
        this.name = name;
        this.members = members;
        this.profilepicture = profilepicture;
        this.created_by = created_by;
        this.school_id = school_id;
    }

    public Team() {
    }

    public Team(String name) {
        this.name = name;
    }

    public Team(int school_id) {
        this.school_id = school_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public String getProfilepicture() {
        return profilepicture;
    }

    public void setProfilePicture(String profilepicture) {
        this.profilepicture = profilepicture;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public int getSchool_id() {
        return school_id;
    }

    public void setSchool_id(int school_id) {
        this.school_id = school_id;
    }
}
