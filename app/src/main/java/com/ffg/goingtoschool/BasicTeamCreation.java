package com.ffg.goingtoschool;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ffg.goingtoschool.S3.S3Uploader;
import com.ffg.goingtoschool.apiConsumer.APIClient;
import com.ffg.goingtoschool.apiConsumer.APIInterface;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.google.android.material.textfield.TextInputLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BasicTeamCreation extends AppCompatActivity implements View.OnClickListener {

    private ImageView profilePictureView;
    private Uri imageUri;
    private S3Uploader s3uploaderObj;
    private APIInterface apiInterface;
    final MySharePreference sp = new MySharePreference();
    private TextView changePhoto;
    private TextInputLayout teamName;
    private TextInputLayout teamMembers;
    private Button saveTeamBtn;
    private int school_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_team_creation);

        s3uploaderObj = new S3Uploader(getApplicationContext());
        apiInterface = APIClient.getClient().create(APIInterface.class);

        changePhoto = findViewById(R.id.changePhotoId);
        profilePictureView = findViewById(R.id.profilePicture);
        changePhoto.setOnClickListener(this);
        profilePictureView.setOnClickListener(this);

        teamName = findViewById(R.id.teamNameId);
        teamMembers = findViewById(R.id.teamMemberList);
        saveTeamBtn = findViewById(R.id.enterTeamDash);
        saveTeamBtn.setOnClickListener(this);
        school_id = getIntent().getIntExtra("selected_school",0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changePhotoId:
            case R.id.profilePicture:
                selectImage();
                break;
            case R.id.enterTeamDash:
                createTeam();
                break;
            case R.id.logoutBtn:
                logout();
                break;
            default:
                break;
        }
    }

    private void selectImage() {
        if(CameraUtils.checkPermissions(getApplicationContext())){
            final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA}, Constants.MY_CAMERA_PERMISSION_CODE);
                            } else {
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Camera not supported", Toast.LENGTH_LONG).show();
                        }
                    } else if (options[item].equals("Choose from Gallery")) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }else{
            requestCameraPermission(Constants.MEDIA_TYPE_IMAGE);
        }


    }

    private void showPermissionsAlert() {
        android.app.AlertDialog alert;
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(BasicTeamCreation.this);
        builder.setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setTitle("Permissions required!")
                .setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CameraUtils.openSettings(getApplicationContext());
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(BasicTeamCreation.this, "Sign out Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
        alert = builder.create();

        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Context context = BasicTeamCreation.this;

                Button negButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                negButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                negButton.setTextColor(context.getResources().getColor(R.color.themeRed));

                Button posButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                posButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                posButton.setTextColor(context.getResources().getColor(R.color.themeYellow));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(20, 0, 0, 0);
                negButton.setLayoutParams(params);
            }
        });

        alert.show();

    }

    private void requestCameraPermission(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            selectImage();

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        } else {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void createTeam() {
        Log.d("TEAM_CREATION","Image URI :: "+imageUri);

        if("".equalsIgnoreCase(teamName.getEditText().getText().toString().trim())){
            Toast.makeText(getApplicationContext(), "Please enter team name", Toast.LENGTH_LONG).show();
        }else{
            uploadImageTos3(imageUri,false);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "IMG_"+timeStamp, null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.CAMERA_REQUEST ) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                Uri tempUri = getImageUri(getApplicationContext(), thumbnail);
                File finalFile = new File(getRealPathFromURI(tempUri));
                Log.d("ACTIVITY RESULT","final url : "+finalFile);
                imageUri = Uri.fromFile(finalFile);
                Bitmap bMapScaled = Bitmap.createScaledBitmap(thumbnail, profilePictureView.getWidth(), profilePictureView.getHeight(), true);
                profilePictureView.setImageBitmap(bMapScaled);
            } else if (requestCode == 2) {
                imageUri = data.getData();

                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(imageUri);
                } catch (FileNotFoundException e) {
                    Log.e("Exception:", e.getMessage());
                }
                if (imageStream != null) {
                    profilePictureView.setImageBitmap(BitmapFactory.decodeStream(imageStream));
                }
            }
        }
    }

    private void uploadImageTos3(Uri imageUri, final boolean isUpdate) {
        Log.d("TEAM_CREATION","Entered Upload image to S3 bucket");

        final String path = getFilePathfromURI(imageUri);
        Log.d("TEAM_CREATION","Image path : "+path);

        if(!"".equalsIgnoreCase(path)){
            s3uploaderObj.initUpload(path, true);
            s3uploaderObj.setOns3UploadDone(new S3Uploader.S3UploadInterface() {
                @Override
                public void onUploadSuccess(String response) {
                    if (response.equalsIgnoreCase("Success")) {
                        Team team = new Team(teamName.getEditText().getText().toString().trim(),teamMembers.getEditText().getText().toString().trim(),(new File(path)).getName(),sp.getStringFromMyPref(Constants.LOGGED_IN_USER,""),school_id);
                        saveTeamData(team);
                    }
                }

                @Override
                public void onUploadError(String response) {
                    Log.e("TAG", "Error Uploading Image : "+response);
                }
            });
        }else{
            Team team = new Team(teamName.getEditText().getText().toString().trim(),teamMembers.getEditText().getText().toString().trim(),"",sp.getStringFromMyPref(Constants.LOGGED_IN_USER,""),school_id);
            saveTeamData(team);
        }
    }

    private String getFilePathfromURI(Uri selectedImageUri) {
        if(selectedImageUri == null){
            return "";
        }

        String filePath = "";
        if(selectedImageUri.toString().contains("file:")){
            filePath = selectedImageUri.toString().substring(7,selectedImageUri.toString().length());
        }else{
            Log.d("TAG",selectedImageUri.toString());
            String wholeID = DocumentsContract.getDocumentId(selectedImageUri);
            String id = wholeID.split(":")[1];  // Split at colon, use second item in the array
            String[] column = {MediaStore.Images.Media.DATA};
            String sel = MediaStore.Images.Media._ID + "=?"; // where id is equal to

            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath;
    }

    private boolean saveTeamData(Team team) {
        Call<Team> call = null;
        String successStatus = "";
        call = apiInterface.createTeam(sp.getStringFromMyPref(Constants.AWS_TOKEN, ""), team);
        successStatus = "Team Created ";
        final String finalSuccessStatus = successStatus;
        call.enqueue(new Callback<Team>() {
            @Override
            public void onResponse(Call<Team> call, Response<Team> response) {

                AlertDialog alert;
                AlertDialog.Builder builder = new AlertDialog.Builder(BasicTeamCreation.this);
                builder.setMessage(finalSuccessStatus)
                        .setTitle("Alert")
                        .setCancelable(false);

                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent logedinScreen = new Intent(getApplicationContext(), UserLogsInOptionScreen.class);
                        startActivity(logedinScreen);
                    }
                });
                alert = builder.create();
                alert.show();
            }

            @Override
            public void onFailure(Call<Team> call, Throwable t) {
                AlertDialog alert;
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setMessage("Team Creation Failed.")
                        .setTitle("Alert")
                        .setCancelable(true);

                builder.setPositiveButton("Something went wrong while creating team", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert = builder.create();
                alert.show();
            }
        });
        return true;
    }

    private void logout() {
        android.app.AlertDialog alert;
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(BasicTeamCreation.this);
        builder.setMessage("Sign out ?")
                .setTitle("Alert")
                .setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Cognito cognito = new Cognito(BasicTeamCreation.this, BasicTeamCreation.this);
                if (cognito.signOut()) {
                    Log.d(this.getClass().getSimpleName(), "Redirecting to Login Page");
                    Intent loginActivity = new Intent(BasicTeamCreation.this, LoginActivity.class);
                    startActivity(loginActivity);
                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(BasicTeamCreation.this, "Sign out Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
        alert = builder.create();

        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Context context = BasicTeamCreation.this;

                Button negButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                negButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                negButton.setTextColor(context.getResources().getColor(R.color.themeRed));

                Button posButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                posButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                posButton.setTextColor(context.getResources().getColor(R.color.themeYellow));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(20, 0, 0, 0);
                negButton.setLayoutParams(params);
            }
        });

        alert.show();
    }
}