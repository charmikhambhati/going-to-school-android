package com.ffg.goingtoschool;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class SplashActivity extends Activity {
    private boolean ispaused = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        VideoView vView = (VideoView) findViewById(R.id.videoView);
        Uri video = Uri.parse("android.resource://" + getPackageName() + "/"
                + R.raw.gts);

        MediaController mediaController = new MediaController(this);


        if (vView != null) {
            mediaController.setAnchorView(vView);
            vView.setMediaController(mediaController);
            vView.setVideoURI(video);
            vView.setZOrderOnTop(true);
            vView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    jump();
                }
            });


            vView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    jump();
                    return false;
                }
            });

            vView.start();

        } else {

            jump();
        }
    }

    private void jump() {

        // Jump to your Next Activity or MainActivity
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);

        SplashActivity.this.finish();

    }


    @Override
    protected void onPause() {
        super.onPause();
        ispaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ispaused) {
            jump();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
