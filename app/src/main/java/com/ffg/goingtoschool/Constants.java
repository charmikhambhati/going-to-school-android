package com.ffg.goingtoschool;

public interface Constants {
    String POOL_ID = "ap-south-1_EmGTl5jHj";
    String CLIENT_ID = "1g0t61vnvdil3jftvsap1ebg7i";
    String IDENTITY_POOL_ID = "ap-south-1:1836cd06-5b1c-4bc8-911f-7f7d897aed8d";

    String FIRST_TIME = "firstTime";
    String AWS_TOKEN = "jwtToken";
    String BUCKET_NAME = "going-to-school-captured-content";
    String THUMBNAIL_BUCKET_NAME = "going-to-school-thumbnails";
    String LOGGED_IN_USER = "loggedInUser";
    int MY_CAMERA_PERMISSION_CODE = 100;
    int CAMERA_REQUEST = 1888;
    String EMPTY_STRING = "";
    String TOKEN_EXPIRY_TIME = "tokenExpiryTime";
    String LOGGED_IN_USER_ROLE = "loggenInUserRole";
    String SCHOOL_SP = "School";
    String WARD_SP = "Ward";
    String TEAM_SP = "Team";

    /**
     * Roles
     */
    String FACILITATOR = "facilitator";
    String STUDENT = "student";

    /**
     * Service Endpoints
     */
    String GET_USER_DATA_BY_USERNAME = "/dev/user/login?";
    String GET_ALL_WARDS = "/dev/ward";
    String CREATE_TEAM = "/dev/team/create";
    String UPDATE_TEAM = "/dev/team/update";
    String GET_TEAMS = "/dev/team";
    String GET_TEAM_BY_TEAM_NAME = "/dev/team";
    String GET_ALL_SCHOOLS = "/dev/school";
    String GET_ALL_PROBLEMS = "/dev/problem";
    String UPDATE_WORKSHOP = "/dev/school/workshop/update";
    String GET_ALL_WORKSHOPS = "/dev/school/workshop";
    String REGISTER_USER = "/dev/user/createuser";
    String CREATE_DASHBOARD = "/dev/problem/create";
    String FETCH_DASHBOARD = "/dev/problem";
    String UPDATE_DASHBOARD = "/dev/problem/update";
    String FETCH_IMAGES_WITH_TEAM = "/dev/image";
    String CREATE_IMAGE_METADATA = "/dev/image/create";
    String REMOVE_IMAGE_FROM_DASHBOARD = "/dev/image/removeImage";
    String WARD_ID_SP = "WARD_ID";


    /**
     * Gallery Directory name to save videos
     */
    String GALLERY_DIRECTORY_NAME = "gts_captures";

    // Activity request codes
    int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    // key to store image path in savedInstance state
    String KEY_IMAGE_STORAGE_PATH = "image_path";

    int MEDIA_TYPE_VIDEO = 2;
    int MEDIA_TYPE_IMAGE = 1;

    // Bitmap sampling size
    int BITMAP_SAMPLE_SIZE = 8;


    // Image and Video file extensions
    String VIDEO_EXTENSION = "mp4";
    String IMAGE_EXTENSION = "jpg";

    // Progress Strings
    String IN_PROGRESS = "In Progress";
    String NOT_STARTED = "Not Started";
    String COMPLETED = "Completed";
    String ON_HOLD = "On Hold";

    String PWD_LENGTH_6_ERROR = "Member must have length greater than or equal to 6";
    String PWD_LENGTH_ERROR = "Password not long enough";
    String PWD_SYMBOL_ERROR = "Password must have symbol characters";
    String USER_EXISTS_ERROR = "User already exists";
    String INVALID_CODE = "Invalid verification code provided";
    String EMAIL_ALREADY_EXISTS = "An account with the email already exists";
    String EMAIL_ADDRESS_DUBLICATE = "Duplicate entry";

    String NOT_UPLOADED_S3 = "N";
    String UPLOADED_S3 = "Y";
    String PENDING_DELETE_S3 = "P";
    String DELETE_S3 = "D";

    String CONFIRMATION_MSG = "If Auto Sync is enabled then media will get uploaded automatically " +
            "as soon as your device gets connected to wifi, In that case Manual upload will not work, " +
            "Select Ok to proceed, else click Cancel";
    String WARNING_MSG = "This functionality is not available, Since Auto Sync is enabled, In order to use this functionality, kindly disable Auto Sync.";
    String DISABLE_OPTION_MSG = "This option is not available in current tab.";
    String DELETE_PERM_MSG = "This will delete the media permanently from device and cloud storage, are you sure want to proceed?";
    String SELECT_ATLEAST_MEDIA = "Please select at least one Media";
    String NO_NETWORK = "Please enable, either wifi or mobile data.";
    String NO_MEDIA_FOUND = "No Media found, please take a image/video from capture option";
    String DASHBOARD_FETCH_ERROR = "Error while fetching dashboards";
}
