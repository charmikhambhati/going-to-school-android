package com.ffg.goingtoschool;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.ffg.goingtoschool.S3.AmazonUtil;
import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.ffg.goingtoschool.database.MediaUploadUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class AttachImageActivity extends AppCompatActivity implements View.OnClickListener{

    final MySharePreference sp = new MySharePreference();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> images = new ArrayList<String>();

    private ArrayList<String> checkedImages = new ArrayList();
    private ArrayList<String> allTeamImageNames = new ArrayList();
    private ArrayList<String> attachedImagesNames = new ArrayList();
    private RecyclerAdapter adapter;
    public LottieAnimationView animation;
    private String problemDashName;
    private int problemDashboardId;
    private int selectedImagesCount;
    private int teamID;
    private Dialog messageDialog;

    private MediaUploadUtil mediaUploadUtil;
    private List<String> listing;

    String imageBaseDir;
    Button backButton, attachButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_attach_image);

        messageDialog = new Dialog(this);

        mediaUploadUtil = new MediaUploadUtil(this);
        animation = (LottieAnimationView) findViewById(R.id.loadingAnim);
        animation.setRepeatCount(LottieDrawable.INFINITE);

        imageBaseDir = Environment.getExternalStorageDirectory().toString() + "/Pictures/" + Constants.GALLERY_DIRECTORY_NAME;

        selectedImagesCount = 0;
        problemDashName = getIntent().getStringExtra("selected_problem_dashboard");
        problemDashboardId = getIntent().getIntExtra("selected_problem_dashboard_id",0);
        Gson gson = new Gson();

        String attachedImages = getIntent().getStringExtra("attachedImagesNames");
        if (attachedImages != null) {
            attachedImagesNames = gson.fromJson(attachedImages, new TypeToken<ArrayList<String>>() {
            }.getType());
        }

        teamID = 0;
        String team = sp.getStringFromMyPref(Constants.TEAM_SP, "");
        if (team != null && !"".equals(team)) {
            teamID = (gson.fromJson(team, Team.class)).getId();
        }

        backButton = findViewById(R.id.backButton);
        attachButton = findViewById(R.id.attachButton);

        backButton.setOnClickListener(this);
        attachButton.setOnClickListener(this);

        images = getLocalImagesNames();
        animation = (LottieAnimationView) findViewById(R.id.loadingAnim);
        getAllTeamImageNames();
    }

    private String getStringResourceByName(String key) {
        String packageName = getPackageName();
        int resId = getResources().getIdentifier(key, "string", packageName);
        return getString(resId);
    }

    private ArrayList<String> getLocalImagesNames() {

        ArrayList<String> _images = new ArrayList<>();

        // get images names from local db
        ArrayList<MediaUpload> media = mediaUploadUtil.readAllMediaUpload();
        for (int i = 0; i < media.size(); ++i) {
            if (media.get(i).getTeam_Id() == teamID) {
                String fullFileName = media.get(i).getMedia_File_Name();
                String fileName = fullFileName.substring(fullFileName.lastIndexOf('/') + 1);
                _images.add(fileName);
            }
        }
        return _images;
    }

    private void getAllTeamImageNames() {

        Gson gson = new Gson();
        final Integer team = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getId();

        // Get List of files from S3 Bucket
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {

                try {
                    Looper.prepare();
                    listing = AmazonUtil.getObjectNamesForBucket(Constants.BUCKET_NAME, team.toString() );

                    for (int i=0; i< listing.size(); i++){
                        allTeamImageNames.add(listing.get(i));
                    }

                    Looper.loop();
                }
                catch (Exception e) {
                    Log.e("tag", "Exception found while listing "+ e);
                }
            }
        });
        thread.start();
        initRecyclerView();
    }

    private void initRecyclerView() {
        // combining both lists
        showLoading();
        ArrayList<String> allTeamImagesCopy = new ArrayList<>(allTeamImageNames);
        allTeamImagesCopy.removeAll(images);
        images.addAll(allTeamImagesCopy);
        images.removeAll(attachedImagesNames);
        if(images.size() == 0){
            showPopup("No Images found.", false);
        }
        recyclerView = findViewById(R.id.recycler_view);
        layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter(images, this);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerAdapter.RecyclerTouchListener(this, recyclerView, new RecyclerAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if(selectedImagesCount != 0){
                    selectImage(view, position);
                }
                else{
                    Intent i;
                    try {
                        // show full size image
                        if (images.get(position).contains(Constants.IMAGE_EXTENSION)) {
                            i = new Intent(AttachImageActivity.this, ImageViewerActivity.class);
                            i.putExtra("filepath", imageBaseDir + "/" + images.get(position));
                            startActivity(i);
                        } else if (images.get(position).contains(Constants.VIDEO_EXTENSION)) {
                            i = new Intent(AttachImageActivity.this, ExoPlayerActivity.class);
                            i.putExtra("filepath", imageBaseDir + "/" + images.get(position));
                            startActivity(i);
                        }
                    } catch (Exception e) {
                        Log.d("Exception:", e.getMessage());
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                selectImage(view, position);
            }

            private void selectImage(View view, int position) {
                if(!checkedImages.contains(images.get(position))){
                    ((CheckBox) (view.findViewById(R.id.chkBox))).setChecked(true);
                    view.findViewById(R.id.chkBox).setVisibility(View.VISIBLE);
                    checkedImages.add(images.get(position));
                    selectedImagesCount++;
                } else{
                    ((CheckBox) (view.findViewById(R.id.chkBox))).setChecked(false);
                    view.findViewById(R.id.chkBox).setVisibility(View.GONE);
                    checkedImages.remove(images.get(position));
                    selectedImagesCount--;
                }

                // set the count on button
                String label = getStringResourceByName("attach");
                if(selectedImagesCount != 0){
                    label = label + " (" + selectedImagesCount + ")";
                }
                attachButton.setText(label);
            }

        }));

        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //At this point the layout is complete
                //Remove listener after changed RecyclerView's height to prevent infinite loop
                hideLoading();
                recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        hideLoading();
    }


    private void showLoading() {
        animation.setVisibility(View.VISIBLE);
        animation.setRepeatCount(LottieDrawable.INFINITE);
        animation.playAnimation();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void hideLoading() {
        animation.pauseAnimation();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        animation.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                // transition to dashboard details page
                gotoProblemDashboardDetails();
                break;

            case R.id.attachButton:
                // attach images to dashboard and transition to dashboard details page
                try {
                    replicateToSubDir(checkedImages, problemDashboardId);
                } catch (InterruptedException e) {
                    Log.d("Exception:", e.getMessage());
                }
                break;
            default:
                break;

        }
    }


    private void replicateToSubDir(ArrayList<String> checkedImages, int problemDashboardId) throws InterruptedException {
        showLoading();

        ExecutorService threadpool  = Executors.newFixedThreadPool(3);
        List<Future<PutObjectResult>> futures = new ArrayList<>();
        for (int i = 0; i < checkedImages.size(); i++) {
            String subDirName = problemDashboardId + "_" + problemDashName;
            String image = checkedImages.get(i);
            AmazonPutCallable callable = new AmazonPutCallable(image,subDirName);
            futures.add(threadpool.submit(callable));
        }

        for (Future<PutObjectResult> obj: futures) {
            while (!obj.isDone()) {
                try {
                    Thread.sleep(1); //sleep for 1 millisecond before checking again
                } catch (InterruptedException e) {
                    Log.d("Exception:", e.getMessage());
                }

            }
        }
        gotoProblemDashboardDetails();
    }

    class AmazonPutCallable implements Callable<PutObjectResult>{

        private String path;
        private String subDirName;

        AmazonPutCallable(String image, String subDirName){
            this.path = imageBaseDir + "/" + image;
            this.subDirName = subDirName;
        }

        @Override
        public PutObjectResult call() throws Exception {
            return AmazonUtil.putImages(path, subDirName);
        }
    }

    private void gotoProblemDashboardDetails() {
        Intent intent = new Intent(this, ProblemDashboardDetails.class);
        intent.putExtra("selected_problem_dashboard", problemDashName);
        intent.putExtra("selected_problem_dashboard_id", problemDashboardId);
        this.startActivity(intent);
    }

    private void showPopup(String msg, Boolean isSuccess) {
        if(isSuccess){
            messageDialog.setContentView(R.layout.popup_message_positive);
        }
        else{
            messageDialog.setContentView(R.layout.popup_message_negative);
        }
        ImageView _closeImg = (ImageView) messageDialog.findViewById(R.id.closePopupImg);
        Button closeBtn = messageDialog.findViewById(R.id.closePopupButton);
        TextView alertMsg = messageDialog.findViewById(R.id.message);
        alertMsg.setText(msg);

        _closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
                gotoProblemDashboardDetails();
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
                gotoProblemDashboardDetails();
            }
        });
        messageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        messageDialog.show();
    }
}
