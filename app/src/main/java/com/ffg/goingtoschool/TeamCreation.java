package com.ffg.goingtoschool;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.amazonaws.util.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.ffg.goingtoschool.S3.S3Uploader;
import com.ffg.goingtoschool.S3.S3Utils;
import com.ffg.goingtoschool.apiConsumer.APIClient;
import com.ffg.goingtoschool.apiConsumer.APIInterface;
import com.ffg.goingtoschool.apiConsumer.pojo.ResponseWrapper;
import com.ffg.goingtoschool.apiConsumer.pojo.School;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamCreation extends Fragment implements  View.OnClickListener{
        private ImageView profilePictureView;
        private Uri imageUri;
        private S3Uploader s3uploaderObj;
        private APIInterface apiInterface;
        final MySharePreference sp = new MySharePreference();
    private TextInputLayout teamName;
        private TextInputLayout teamMembers;
        private MainActivity mainActivity;
        private Button updateTeamBtn;
        private String fetchedImageName;
        School school;

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_team_creation, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        s3uploaderObj = new S3Uploader(getActivity().getApplicationContext());
        apiInterface = APIClient.getClient().create(APIInterface.class);

        TextView changePhoto = view.findViewById(R.id.changePhotoId);
        profilePictureView = view.findViewById(R.id.profilePicture);
        changePhoto.setOnClickListener(this);
        profilePictureView.setOnClickListener(this);

        teamName = view.findViewById(R.id.teamNameId);
        teamMembers = view.findViewById(R.id.teamMemberList);
        updateTeamBtn = view.findViewById(R.id.updateTeamDash);
        updateTeamBtn.setOnClickListener(this);
        mainActivity = (MainActivity) getActivity();

        fetchTeam();
        }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changePhotoId:
                selectImage();
                break;
            case R.id.profilePicture:
                if(imageUri == null){
                    selectImage();
                }
                break;
            case R.id.enterTeamDash:
                createTeam();
                break;
            case R.id.updateTeamDash:
                updateTeam();
                break;
            default:
                break;
        }
    }

    private void updateTeam() {
        Log.d("TEAM_UPDATION","Image URI :: "+imageUri);
        uploadImageTos3(imageUri,true);
    }

    private void createTeam() {
        Log.d("TEAM_CREATION","Image URI :: "+imageUri);
        if("".equalsIgnoreCase(teamName.getEditText().getText().toString().trim())){
            Toast.makeText(getActivity(), "Please enter team name", Toast.LENGTH_LONG).show();
        }else{
            uploadImageTos3(imageUri,false);
        }

    }

    private void selectImage() {
        if(CameraUtils.checkPermissions(getActivity().getApplicationContext())){
            final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")){
                        if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                                requestPermissions(new String[]{Manifest.permission.CAMERA}, Constants.MY_CAMERA_PERMISSION_CODE);
                            }else{
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
                            }
                        } else {
                            Toast.makeText(getActivity(), "Camera not supported", Toast.LENGTH_LONG).show();
                        }
                    }
                    else if (options[item].equals("Choose from Gallery")){
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
                    }else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }else{
            requestCameraPermission();
        }
    }

    private void requestCameraPermission() {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            selectImage();

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showPermissionsAlert() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(getActivity().getApplicationContext());
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
                return true;
            } else {

                Log.v("TAG","Permission is revoked");
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG","Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_CAMERA_PERMISSION_CODE){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                if(isStoragePermissionGranted()){
                    Toast.makeText(getActivity().getApplicationContext(), "camera permission granted", Toast.LENGTH_LONG).show();
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
                }

            }
            else{
                Toast.makeText(getActivity().getApplicationContext(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == 1){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getActivity().getApplicationContext(), "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
            }else{
                Toast.makeText(getActivity().getApplicationContext(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "IMG_"+timeStamp, null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == Constants.CAMERA_REQUEST ) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                Uri tempUri = getImageUri(getActivity().getApplicationContext(), thumbnail);
                File finalFile = new File(getRealPathFromURI(tempUri));
                Log.d("ACTIVITY RESULT","final url : "+finalFile);
                imageUri = Uri.fromFile(finalFile);
                Bitmap bMapScaled = Bitmap.createScaledBitmap(thumbnail, profilePictureView.getWidth(), profilePictureView.getHeight(), true);
                profilePictureView.setImageBitmap(bMapScaled);
            } else if (requestCode == 2) {
                imageUri = data.getData();

                InputStream imageStream = null;
                try {
                    imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                } catch (FileNotFoundException e) {
                    Log.e("Exception:", e.getMessage());
                }
                if (imageStream != null) {
                    profilePictureView.setImageBitmap(BitmapFactory.decodeStream(imageStream));
                }
            }
        }
    }

    private void uploadImageTos3(Uri imageUri, final boolean isUpdate) {
        Log.d("TEAM_CREATION","Entered Upload image to S3 bucket");

        final String path = getFilePathfromURI(imageUri);
        Log.d("TEAM_CREATION","Image path : "+path);
        mainActivity.showLoading();
        if(!"".equalsIgnoreCase(path)){
            s3uploaderObj.initUpload(path, true);
            s3uploaderObj.setOns3UploadDone(new S3Uploader.S3UploadInterface() {
                @Override
                public void onUploadSuccess(String response) {
                    if (response.equalsIgnoreCase("Success")) {
                        Team team = new Team(teamName.getEditText().getText().toString().trim(), teamMembers.getEditText().getText().toString().trim(), (new File(path)).getName(), sp.getStringFromMyPref(Constants.LOGGED_IN_USER, ""), 0);
                        saveTeamData(team,isUpdate);
                        mainActivity.hideLoading();
                    }
                }

                @Override
                public void onUploadError(String response) {
                    mainActivity.hideLoading();
                    Log.e("TAG", "Error Uploading Image : "+response);
                }
            });
        }else{
            Team team = new Team(teamName.getEditText().getText().toString().trim(), teamMembers.getEditText().getText().toString().trim(), "", sp.getStringFromMyPref(Constants.LOGGED_IN_USER, ""), 0);
            saveTeamData(team,isUpdate);
            mainActivity.hideLoading();
        }
    }

    private String getFilePathfromURI(Uri selectedImageUri) {
        if(selectedImageUri == null){
            return "";
        }

        String filePath = "";
        if(selectedImageUri.toString().contains("file:")){
            filePath = selectedImageUri.toString().substring(7);
        }else if(selectedImageUri.toString().contains("com.google.android.apps.photos")){
            return CommonUtil.getPathFromInputStreamUri(getActivity().getApplicationContext(),selectedImageUri);
        }else{
            Log.d("TAG",selectedImageUri.toString());
            String wholeID = DocumentsContract.getDocumentId(selectedImageUri);
            String id = wholeID.split(":")[1];  // Split at colon, use second item in the array
            String[] column = {MediaStore.Images.Media.DATA};
            String sel = MediaStore.Images.Media._ID + "=?"; // where id is equal to

            Cursor cursor = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();


           }
        return filePath;



    }

    private void fetchTeam(){
        Gson gson = new Gson();
        Team val = new Team();
        String team = sp.getStringFromMyPref(Constants.TEAM_SP, "");
        if (team != null && !"".equals(team)) {
            val.setName((gson.fromJson(team, Team.class)).getName());
        } else {
            // we should never get to this step..
            Toast.makeText(getActivity().getApplicationContext(), "Issue while fetching current team", Toast.LENGTH_LONG).show();
            return;
        }

        mainActivity.showLoading();
        Call<ResponseWrapper<Team>> call = apiInterface.getTeamByTeamName(sp.getStringFromMyPref(Constants.AWS_TOKEN,"") , val);
        call.enqueue(new Callback<ResponseWrapper<Team>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Team>> call, Response<ResponseWrapper<Team>> responseWrapper) {
                if(responseWrapper.isSuccessful()){
                    ResponseWrapper<Team> res = responseWrapper.body();
                    if(res.getStatus().equalsIgnoreCase("success")){
                        List<Team> team = res.getData();
                        Log.d("TAG","Team -> " + team.size());

                        if(team.isEmpty()){
                            mainActivity.hideLoading();
                            //TO-DO: Raise an alert for not finding a team
                        }
                        Team team1 = new Team(team.get(0).getName(),team.get(0).getMembers(),team.get(0).getProfilepicture(),team.get(0).getCreated_by(),team.get(0).getSchool_id());

                        teamName.getEditText().setText(team.get(0).getName());
                        teamMembers.getEditText().setText(team.get(0).getMembers());

                        setProfilePicture(team1.getProfilepicture());

                        updateTeamBtn.setVisibility(View.VISIBLE);
                        teamName.setEnabled(false);

                        mainActivity.hideLoading();
                    }
                }else{
                    mainActivity.hideLoading();
                    Toast.makeText(getActivity(), "Error while fetching team", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", responseWrapper.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Team>> call, Throwable t) {
                call.cancel();
                mainActivity.hideLoading();
                Toast.makeText(getActivity(), "Error while fetching team", Toast.LENGTH_SHORT).show();
            }
        });

        mainActivity.hideLoading();
    }

    private void setProfilePicture(final String fileName) {
        if (fileName == null || "".equalsIgnoreCase(fileName)) {
            Toast.makeText(getActivity().getApplicationContext(), "No Team Photo uploaded yet. Why dont you set one now?", Toast.LENGTH_LONG).show();

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.progress_animation).centerCrop();
            requestOptions.error(R.drawable.ic_error_outline_black_24dp);
            requestOptions.skipMemoryCache(true);
            requestOptions.fitCenter();
            requestOptions.priority(Priority.HIGH);
            requestOptions.dontAnimate();
            requestOptions.dontTransform();
            Glide.with(profilePictureView.getContext()).asBitmap().load(R.mipmap.capture_foreground)
                    .apply(requestOptions).into(profilePictureView);
        } else {
            fetchedImageName = fileName;

            String signedUrl = S3Utils.getS3Url(getActivity().getApplicationContext(), fileName);
            if(!"".equalsIgnoreCase(signedUrl)){
                if (S3Utils.checkObjectExistInS3(getActivity().getApplicationContext(), fileName)) {
                    Glide.with(profilePictureView.getContext()).asBitmap().load(signedUrl).into(profilePictureView);
                }else{
                    Toast.makeText(getActivity().getApplicationContext(), "No Team Photo uploaded yet. Why dont you set one now?", Toast.LENGTH_LONG).show();
                    Glide.with(profilePictureView.getContext()).asBitmap().load(R.mipmap.capture_foreground).into(profilePictureView);
                }
            }else{
                Toast.makeText(getActivity().getApplicationContext(), "No Team Photo uploaded yet. Why dont you set one now?", Toast.LENGTH_LONG).show();
                Glide.with(profilePictureView.getContext()).asBitmap().load(R.mipmap.capture_foreground).into(profilePictureView);
            }
        }
    }

    private boolean saveTeamData(Team team, boolean isUpdate){
        Gson gson = new Gson();
        String schoolValue = sp.getStringFromMyPref(Constants.SCHOOL_SP, "");
        if (schoolValue != null && !"".equals(schoolValue)) {
            School s = gson.fromJson(schoolValue, School.class);
            team.setSchool_id(s.getId());
        } else {
            // we should never get to this step..
            Toast.makeText(getActivity().getApplicationContext(), "Issue while fetching school to create team.", Toast.LENGTH_LONG).show();
            return false;
        }

        Call<Team> call = null;
        String successStatus = "";
        if(isUpdate){
            call = apiInterface.updateTeam(sp.getStringFromMyPref(Constants.AWS_TOKEN,"") , team);
            successStatus = "Team Updated ";
            if(StringUtils.isBlank(team.getProfilepicture())){
                team.setProfilePicture(fetchedImageName);
            }

        }else{
            call = apiInterface.createTeam(sp.getStringFromMyPref(Constants.AWS_TOKEN,"") , team);
            successStatus = "Team Created ";
        }
        final String finalSuccessStatus = successStatus;
        call.enqueue(new Callback<Team>() {
            @Override
            public void onResponse(Call<Team> call, Response<Team> response) {

                AlertDialog alert;
                AlertDialog.Builder builder = new   AlertDialog.Builder(getActivity());
                builder.setMessage(finalSuccessStatus)
                        .setTitle("Alert")
                        .setCancelable(false);

                builder.setPositiveButton("ok",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert = builder.create();

                teamName.setEnabled(false);
                updateTeamBtn.setVisibility(View.VISIBLE);

                alert.show();
            }

            @Override
            public void onFailure(Call<Team> call, Throwable t) {
                AlertDialog alert;
                AlertDialog.Builder builder = new   AlertDialog.Builder(getActivity());
                builder.setMessage("Team Creation Failed.")
                        .setTitle("Alert")
                        .setCancelable(true);

                builder.setPositiveButton("Something went wrong while creating team",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("Message:", "No Action to be performed");
                    }
                });
                alert = builder.create();
                alert.show();
            }
        });

        return true;

    }
}
