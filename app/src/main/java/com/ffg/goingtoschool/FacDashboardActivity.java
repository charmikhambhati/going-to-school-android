package com.ffg.goingtoschool;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.ffg.goingtoschool.apiConsumer.pojo.School;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

public class FacDashboardActivity extends Fragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback  {
    final MySharePreference sp = new MySharePreference();
    protected GoogleApiClient mGoogleApiClient;
    FusedLocationProviderClient mFusedLocationClient;
    int REQUEST_CHECK_SETTINGS = 100;
    int PERMISSION_ID = 44; // helps us to identify user's action with which permission request
    double permissibleDistance = 500;
    School school;
    LinearLayout workshopAct;

    Dialog messageDialog;

    private RelativeLayout m_trackProgress, m_captureContent,  m_upload;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setAnimation();
        return inflater.inflate(R.layout.activity_facilitator_dashboard, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        m_trackProgress = (RelativeLayout) view.findViewById(R.id.trackProgressSubmit);
        m_captureContent = (RelativeLayout) view.findViewById(R.id.captureContentSubmit);
        m_upload = (RelativeLayout) view.findViewById(R.id.uploadFiles);
        m_trackProgress.setOnClickListener(this);
        m_captureContent.setOnClickListener(this);
        m_upload.setOnClickListener(this);


        messageDialog = new Dialog(this.getContext());

        // set school
        Gson gson = new Gson();
        String schoolValue = sp.getStringFromMyPref(Constants.SCHOOL_SP, "");
        if (schoolValue != null && !"".equals(schoolValue)) {
            school = gson.fromJson(schoolValue, School.class);
        }
        else{
            // we should never get to this step..
            Toast.makeText(getActivity().getApplicationContext(), "No School Selected", Toast.LENGTH_LONG).show();
        }

        workshopAct = (LinearLayout) view.findViewById(R.id.workshopFunctionality);
        if(Constants.STUDENT.equalsIgnoreCase(sp.getStringFromMyPref(Constants.LOGGED_IN_USER_ROLE, ""))){
            workshopAct.setVisibility(View.GONE);
        }else{
            workshopAct.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.trackProgressSubmit:
                createLocationRequest();
                break;
            case R.id.captureContentSubmit:
                gotoCaptureContent();
                break;
            case R.id.uploadFiles:
                gotoUploadFiles();
                break;
            default:
                break;
        }
    }

    private void gotoUploadFiles() {
        Intent intent = new Intent(getActivity().getApplicationContext(), VideoGalleryActivity.class);
        startActivity(intent);
    }

    private void gotoCaptureContent() {
        Intent captureActivity = new Intent(getActivity().getApplicationContext(), CaptureContentActivity.class);
        startActivity(captureActivity);
    }


    private void gotoWorkshopActivity() {
        Intent workshopActivity = new Intent(getActivity().getApplicationContext(), WorkshopActionActivity.class);
        startActivity(workshopActivity);
    }

    @SuppressLint("MissingPermission")
    private void checkUserLocation() {
        if(checkLocationPermissions()) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());
            mFusedLocationClient.getLastLocation().addOnCompleteListener(
                    new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            Location location = task.getResult();
                            if (location == null) {
                                createLocationRequest();
                            } else {
                                double userLat = location.getLatitude();
                                double userLon = location.getLongitude();
                                if (!isValidLocation(school.getLatitude(), school.getLongitude())) {
                                    // show popup
                                    showPopup("School Location is not updated. Please contact the administrator", false);
                                } else if (distance(userLat, school.getLatitude(), userLon, school.getLongitude()) <= permissibleDistance) {
                                    gotoWorkshopActivity();
                                } else{
                                    Toast.makeText(getActivity().getApplicationContext(), "You need to be in the vicinity of the school to be able to mark progress", Toast.LENGTH_LONG).show();
                                    showPopup("You need to be in the vicinity of the school to be able to mark progress", false);

                                }
                            }
                        }
                    }
            );
        } else{
            requestLocationPermissions();
        }
    }

    private boolean isValidLocation(Double latitude, Double longitude) {
        return !(latitude == 0 && longitude == 0);
    }

    protected void createLocationRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        SettingsClient client = LocationServices.getSettingsClient(getActivity().getApplicationContext());
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());


        task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here
                checkUserLocation();
            }
        });

        task.addOnFailureListener(getActivity(), new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(getActivity(),
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(getActivity().getApplicationContext(), "GPS enabled", Toast.LENGTH_LONG).show();
                checkUserLocation();

            } else {
                Toast.makeText(getActivity().getApplicationContext(), "GPS is not enabled", Toast.LENGTH_LONG).show();
            }

        }
    }

    private boolean checkLocationPermissions() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestLocationPermissions() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    // calculate distance between two locations
    private double distance(double lat1, double lat2, double lon1, double lon2) {
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        // Haversine formula
        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2),2);

        double c = 2 * Math.asin(Math.sqrt(a));
        // Radius of earth in meters
        double r = 6371000;
        return(c * r);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Result result) {

    }

    public void setAnimation(){
        if(Build.VERSION.SDK_INT>20) {
            Slide slide = new Slide();
            slide.setSlideEdge(Gravity.LEFT);
            slide.setDuration(400);
            slide.setInterpolator(new DecelerateInterpolator());
            getActivity().getWindow().setExitTransition(slide);
            getActivity().getWindow().setEnterTransition(slide);
        }
    }


    private void showPopup(String msg, Boolean isSuccess) {
        if (isSuccess) {
            messageDialog.setContentView(R.layout.popup_message_positive);
        } else {
            messageDialog.setContentView(R.layout.popup_message_negative);
        }
        ImageView closeImg = (ImageView) messageDialog.findViewById(R.id.closePopupImg);
        Button closeBtn = messageDialog.findViewById(R.id.closePopupButton);
        TextView alertMsg = messageDialog.findViewById(R.id.message);
        alertMsg.setText(msg);

        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        messageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        messageDialog.show();
    }
}
