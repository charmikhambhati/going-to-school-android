package com.ffg.goingtoschool;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ForgotPasswordContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.ForgotPasswordHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler;
import com.amazonaws.regions.Regions;
import com.ffg.goingtoschool.apiConsumer.APIClient;
import com.ffg.goingtoschool.apiConsumer.APIInterface;
import com.ffg.goingtoschool.apiConsumer.pojo.ResponseWrapper;
import com.ffg.goingtoschool.apiConsumer.pojo.User;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class Cognito {
    Dialog messageDialog;
    Activity m_activity;
    private CognitoUserPool userPool;
    private CognitoUserAttributes userAttributes;       // Used for adding attributes to the user
    private Context appContext;

    private User currentUserForReg;

    private String userPassword;

    static ForgotPasswordContinuation resultContinuation;
    static MySharePreference sp = new MySharePreference();

    APIInterface apiInterface;

    public Cognito(Context context, Activity m_activity) {
        apiInterface = APIClient.getClient().create(APIInterface.class);
        appContext = context;
        this.m_activity = m_activity;
        userPool = new CognitoUserPool(context, Constants.POOL_ID, Constants.CLIENT_ID,"", Regions.AP_SOUTH_1);
        userAttributes = new CognitoUserAttributes();
        messageDialog = new Dialog(m_activity);

    }

    public void signUpInBackground(String userId, String password) {
        currentUserForReg = new User();

        userPool.signUpInBackground(userId, password, this.userAttributes, null, signUpCallback);

        currentUserForReg.setUsername(userId);
        currentUserForReg.setName(this.userAttributes.getAttributes().get("name"));
        currentUserForReg.setEmail( this.userAttributes.getAttributes().get("email"));
        currentUserForReg.setPhone(this.userAttributes.getAttributes().get("phone_number"));
    }

    SignUpHandler signUpCallback = new SignUpHandler() {
        @Override
        public void onSuccess(CognitoUser cognitoUser, boolean userConfirmed, CognitoUserCodeDeliveryDetails cognitoUserCodeDeliveryDetails) {
            // Sign-up was successful
            Log.d(TAG, "Sign-up success");
            Toast.makeText(appContext, "Sign-up success", Toast.LENGTH_LONG).show();
            SignUp.first.setVisibility(View.GONE);
            SignUp.second.setVisibility(View.VISIBLE);
            showPopup("Sign-up successful", true);
            // Check if this user (cognitoUser) needs to be confirmed
            if (!userConfirmed) {
                // This user must be confirmed and a confirmation code was sent to the user
                // cognitoUserCodeDeliveryDetails will indicate where the confirmation code was sent
                // Get the confirmation code from user
            } else {
                showPopup("User Confirmed before", false);
            }
        }

        @Override
        public void onFailure(Exception exception) {
            showPopup("Sign-up failed", false);
            if (exception.getMessage().contains(Constants.PWD_LENGTH_ERROR) ||
                    exception.getMessage().contains(Constants.PWD_LENGTH_6_ERROR) ||
                    exception.getMessage().contains(Constants.PWD_SYMBOL_ERROR)) {
                showPopup("Password length should be more than 6 \n Password must have uppercase characters \n Password must have symbol characters", false);
            } else if (exception.getMessage().contains(Constants.USER_EXISTS_ERROR)) {
                showPopup("User Name already exists", false);
            }
            Log.d(TAG, "Sign-up failed: " + exception.getMessage());
        }
    };

    public void confirmUser(String userId, String code) {
        CognitoUser cognitoUser = userPool.getUser(userId);
        cognitoUser.confirmSignUpInBackground(code, false, confirmationCallback);
    }

    // Callback handler for confirmSignUp API
    GenericHandler confirmationCallback = new GenericHandler() {

        @Override
        public void onSuccess() {
            createUser(currentUserForReg);
        }

        @Override
        public void onFailure(Exception exception) {
            if (exception.getMessage().contains(Constants.INVALID_CODE)) {
                showPopup(Constants.INVALID_CODE, false);
            } else if (exception.getMessage().contains(Constants.EMAIL_ALREADY_EXISTS)) {
                showPopup(Constants.EMAIL_ALREADY_EXISTS, false);
            }
        }
    };

    public void addAttribute(String key, String value) {
        userAttributes.addAttribute(key, value);
    }

    public void userLogin(String userId, String password) {
        CognitoUser cognitoUser = userPool.getUser(userId);
        this.userPassword = password;
        cognitoUser.getSessionInBackground(authenticationHandler);

    }

    public void forgetPassword(String userId) {
        CognitoUser cognitoUser = userPool.getUser(userId);
        cognitoUser.forgotPasswordInBackground(forgetPasswordCallback);
        ForgotPasswordActivity.first.setVisibility(View.GONE);
        ForgotPasswordActivity.second.setVisibility(View.VISIBLE);
    }

    // Callback handler for the sign-in process
    AuthenticationHandler authenticationHandler = new AuthenticationHandler() {
        @Override
        public void authenticationChallenge(ChallengeContinuation continuation) {}

        @Override
        public void onSuccess(final CognitoUserSession userSession, CognitoDevice newDevice) {
            String userName = null;
            try {
                userName = userSession.getAccessToken().getUsername();
            } catch (Exception e) {
                Log.d("Exception:", e.getMessage());
            }
            sp.addStringToMyPref(Constants.AWS_TOKEN, userSession.getIdToken().getJWTToken());
            sp.addStringToMyPref(Constants.LOGGED_IN_USER, userName);
            sp.addLongToMyPref(Constants.TOKEN_EXPIRY_TIME,userSession.getIdToken().getExpiration().getTime());


            getUserDetails(userSession.getIdToken().getJWTToken(), userName);


        }

        @Override
        public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String userId) {
            // The API needs user sign-in credentials to continue
            AuthenticationDetails authenticationDetails = new AuthenticationDetails(userId, userPassword, null);
            // Pass the user sign-in credentials to the continuation
            authenticationContinuation.setAuthenticationDetails(authenticationDetails);
            // Allow the sign-in to continue
            authenticationContinuation.continueTask();
        }

        @Override
        public void getMFACode(MultiFactorAuthenticationContinuation multiFactorAuthenticationContinuation) {
            // Multi-factor authentication is required; get the verification code from user
            // Allow the sign-in process to continue
        }

        @Override
        public void onFailure(Exception exception) {
            // Sign-in failed, check exception for the cause
            showPopup("Sign in Failure. Please Check ID or Password", false);
        }
    };

    public CognitoUserPool getUserPool() {
        return new CognitoUserPool(appContext, Constants.POOL_ID, Constants.CLIENT_ID, "", Regions.AP_SOUTH_1);
    }

    ForgotPasswordHandler forgetPasswordCallback = new ForgotPasswordHandler() {
        @Override
        public void onSuccess() {
            Toast.makeText(appContext, "Password changed successfully", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(appContext, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            appContext.startActivity(intent);
        }

        @Override
        public void getResetCode(ForgotPasswordContinuation continuation) {

            Log.d("in cognito", "Resetting...");
            CognitoUserCodeDeliveryDetails code = continuation.getParameters();
            Log.i("code sent here: ", code.getDestination());
            resultContinuation = continuation;

            Log.d(TAG, "resultContinuation: " + resultContinuation.getParameters().getDestination());

        }

        @Override
        public void onFailure(Exception exception) {
            Log.d("in cognito", "change fail...");
        }
    };

    public void resetPassword(String code, String newPassword) {
        Log.d(TAG, "resetPassword: " + code + newPassword);
        Log.d(TAG, "resultContinuation: " + resultContinuation.getParameters().getDestination());

        resultContinuation.setVerificationCode(code);
        resultContinuation.setPassword(newPassword);

        Log.d(TAG, "resetPassword: got code and pwd");

        resultContinuation.continueTask();
    }

    public boolean signOut(){
       try{
           Cognito auth = new Cognito(appContext, m_activity);
           auth.getUserPool().getCurrentUser().signOut();
           sp.destroySharedPref();
           return true;
       } catch (Exception e){
            return false;
       }
    }

    public boolean isAWSTokenExpired(){
        Date currTime = new Date();
        Date expTime = new Date(sp.getLongFromMyPref(Constants.TOKEN_EXPIRY_TIME,0L));
        return !expTime.after(currTime);
    }

    private void getUserDetails(String authToken, String userName){
        /**
         GET logged in user details
         **/
        Call<ResponseWrapper<User>> call = apiInterface.getUserDataByUserName(authToken, userName);
        call.enqueue(new Callback<ResponseWrapper<User>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<User>> call, Response<ResponseWrapper<User>> responseWrapper) {
                Log.d("TAG", responseWrapper.code() + "");
                if (responseWrapper.isSuccessful()) {
                    ResponseWrapper<User> res = responseWrapper.body();
                    if (res !=null && res.getStatus().equalsIgnoreCase("success")) {
                        List<User> users = res.getData();
                        sp.addStringToMyPref(Constants.LOGGED_IN_USER_ROLE,"");
                        Intent intent = null;
                        if(users.size() == 0){
                            //User has not been created
                            showPopup("User is not authorized. Be patient", false);
                        }else{
                            if(users.get(0).getApprove() != null && !users.get(0).getApprove()){
                                //User is not approved yet show and error
                                showPopup("User is not authorized. Be patient", false);
                            }else{
                                intent = new Intent(appContext, UserLogsInOptionScreen.class);
                                if (Constants.STUDENT.equalsIgnoreCase(users.get(0).getRole())){
                                    sp.addStringToMyPref(Constants.LOGGED_IN_USER_ROLE,Constants.STUDENT);
                                }else{
                                    sp.addStringToMyPref(Constants.LOGGED_IN_USER_ROLE,users.get(0).getRole());
                                }
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//because we use Application context
                                appContext.startActivity(intent);
                            }
                        }

                    }
                } else {
                    Log.e("TAG", responseWrapper.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<User>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void createUser(User currentUserForReg){
        Call<ResponseWrapper<User>> call = apiInterface.registerUser(currentUserForReg);
        call.enqueue(new Callback<ResponseWrapper<User>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<User>> call, Response<ResponseWrapper<User>> responseWrapper) {
                Log.d("TAG", responseWrapper.code() + "");
                if (responseWrapper.isSuccessful()) {
                    ResponseWrapper<User> res = responseWrapper.body();
                    if (res.getStatus().equalsIgnoreCase("success")) {
                        Intent intent = new Intent(appContext, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//because we use Application context
                        appContext.startActivity(intent);
                                       }
                } else {
                    Log.e("TAG", responseWrapper.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<User>> call, Throwable t) {
                call.cancel();
                AlertDialog alert;
                AlertDialog.Builder builder = new   AlertDialog.Builder(appContext);
                builder.setMessage("Team Creation Failed.")
                        .setTitle("Alert")
                        .setCancelable(true);

                builder.setPositiveButton("Something went wrong while creating user",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert = builder.create();
                alert.show();
            }
        });
    }

    private void showPopup(String msg, Boolean isSuccess) {
        if(isSuccess){
            messageDialog.setContentView(R.layout.popup_message_positive);
        }
        else{
            messageDialog.setContentView(R.layout.popup_message_negative);
        }
        ImageView closeImg = (ImageView)messageDialog.findViewById(R.id.closePopupImg);
        Button closeBtn = messageDialog.findViewById(R.id.closePopupButton);
        TextView alertMsg = messageDialog.findViewById(R.id.message);
        alertMsg.setText(msg);

        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        messageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        messageDialog.show();
    }
}