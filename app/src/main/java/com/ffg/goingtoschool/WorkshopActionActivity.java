package com.ffg.goingtoschool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.ffg.goingtoschool.apiConsumer.APIClient;
import com.ffg.goingtoschool.apiConsumer.APIInterface;
import com.ffg.goingtoschool.apiConsumer.pojo.ResponseWrapper;
import com.ffg.goingtoschool.apiConsumer.pojo.School;
import com.ffg.goingtoschool.apiConsumer.pojo.Workshop;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import at.grabner.circleprogress.CircleProgressView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkshopActionActivity extends Activity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final String TAG = "WorkshopActivity";
    private final MySharePreference sp = new MySharePreference();

    private Button saveSubmitBtn;
    private Spinner workshopListSpinner;
    private int progress;
    private List<String> workshopList = new ArrayList<>();
    private List<String> workshopStatusList = new ArrayList<>();
    private List<Workshop> workshops = new ArrayList<>();
    private List<Integer> workshopId = new ArrayList<>();
    private CircleProgressView mCircleView;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_workshop_action);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        this.setFinishOnTouchOutside(false);
        saveSubmitBtn = findViewById(R.id.saveSubmit);
        saveSubmitBtn.setOnClickListener(this);

        mCircleView = (CircleProgressView) findViewById(R.id.circleView);

        mCircleView.setSeekModeEnabled(false);
        mCircleView.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(float value) {

                progress = (int) value;
                Log.d(TAG, "Progress Changed: " + value);
                mCircleView.setText(getProgressString(progress));

            }
        });

        workshopListSpinner = (Spinner) findViewById(R.id.workshopList);

        populateWorkshopList();
        workshopListSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logoutSubmit:
                logOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void logOut() {
        Intent loginActivity = new Intent(this, LoginActivity.class);
        startActivity(loginActivity);
    }

    private void populateWorkshopList() {

        Gson gson = new Gson();
        Integer schoolID = gson.fromJson(sp.getStringFromMyPref(Constants.SCHOOL_SP, ""), School.class).getId();
        Call<ResponseWrapper<Workshop>> call = apiInterface.getWorkshops(sp.getStringFromMyPref(Constants.AWS_TOKEN, ""),
                schoolID.toString(), sp.getStringFromMyPref(Constants.WARD_ID_SP, ""));
        call.enqueue(new Callback<ResponseWrapper<Workshop>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Workshop>> call, Response<ResponseWrapper<Workshop>> responseWrapper) {
                Log.d("TAG", responseWrapper.code() + "");
                if (responseWrapper.isSuccessful()) {
                    ResponseWrapper<Workshop> res = responseWrapper.body();
                    if (res.getStatus().equalsIgnoreCase("success")) {
                        workshops = res.getData();
                        Log.d("TAG", "Workshops -> " + workshops.size());


                        for (Workshop workshop : workshops) {
                            workshopList.add(workshop.getName());
                            workshopStatusList.add(workshop.getStatus());
                            Log.d(TAG, "workshop" + workshop.getId().toString());
                            workshopId.add(workshop.getId());

                        }

                        ArrayAdapter<String> workshopAdapter = new ArrayAdapter<String>(WorkshopActionActivity.this, android.R.layout.simple_spinner_item, workshopList);
                        workshopListSpinner.setAdapter(workshopAdapter);
                        if (!workshopList.isEmpty()) {
                            mCircleView.setSeekModeEnabled(true);
                        }

                    }
                } else {

                    Toast.makeText(getApplicationContext(), "Error while fetching workshops", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", responseWrapper.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Workshop>> call, Throwable t) {
                call.cancel();

                Toast.makeText(getApplicationContext(), "Error while fetching workshops", Toast.LENGTH_SHORT).show();
                Log.e("TAG", call.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.saveSubmit:
                saveWorkshop();
                break;
            default:
                break;
        }
    }


    private void saveWorkshop() {

        Gson gson = new Gson();
        Integer schoolID = gson.fromJson(sp.getStringFromMyPref(Constants.SCHOOL_SP, ""), School.class).getId();

        Workshop workshop = new Workshop(schoolID, Integer.parseInt(sp.getStringFromMyPref(Constants.WARD_ID_SP, "")),
                workshopId.get(workshopListSpinner.getSelectedItemPosition()), getProgressString(progress));

        Call<Workshop> call = apiInterface.updateWorkshop(sp.getStringFromMyPref(Constants.AWS_TOKEN, ""), workshop);
        call.enqueue(new Callback<Workshop>() {
            @Override
            public void onResponse(Call<Workshop> call, Response<Workshop> response) {


                Log.d("TAG", response.code() + "");
                if (response.isSuccessful()) {

                    Toast.makeText(WorkshopActionActivity.this, "Updated Workshop!", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getApplicationContext(), "Failed to update workshop", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<Workshop> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failed to update workshop", Toast.LENGTH_LONG).show();

            }
        });


        Intent facDashboardActivity = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(facDashboardActivity);
    }

    private String getProgressString(int progress) {
        String progressString = null;
        if (progress <= 10) {
            progressString = Constants.NOT_STARTED;
        } else if (progress >= 10 && progress <= 50) {
            progressString = Constants.IN_PROGRESS;
        } else if (progress > 50 && progress <= 100) {
            progressString = Constants.COMPLETED;
        }
        return progressString;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        switch (adapterView.getId()) {
            case R.id.workshopList:
                if (workshopStatusList.get(position).equalsIgnoreCase(Constants.NOT_STARTED)) {

                    mCircleView.setMinValueAllowed(10);
                    mCircleView.setValue(10);

                } else if (workshopStatusList.get(position).equalsIgnoreCase(Constants.IN_PROGRESS)) {

                    mCircleView.setMinValueAllowed(50);
                    mCircleView.setValue(50);

                } else if (workshopStatusList.get(position).equalsIgnoreCase(Constants.COMPLETED)) {
                    mCircleView.setMinValueAllowed(100);
                    mCircleView.setValue(100);

                }

                break;

            default:
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Toast.makeText(this, "Please select a workshop", Toast.LENGTH_SHORT).show();
    }


}
