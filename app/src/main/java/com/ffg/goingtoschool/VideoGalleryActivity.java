package com.ffg.goingtoschool;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ffg.goingtoschool.S3.S3Uploader;
import com.ffg.goingtoschool.S3.S3Utils;
import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.ffg.goingtoschool.database.MediaUploadUtil;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class VideoGalleryActivity extends AppCompatActivity {

    private Toolbar toolbar;
    protected GalleryAdapter mAdapter;
    protected MediaUploadUtil mediaUploadUtil;
    protected S3Uploader s3uploaderObj;
    protected String urlFromS3 = null;
    protected ProgressDialog progressDialog;
    protected SparseBooleanArray mSelectedItemsIds;
    protected Switch autoSync;
    protected SharedPreferences myPrefs;
    protected LinkedHashSet<String> contents = new LinkedHashSet();
    protected List<String> checkedMediaList = new ArrayList();
    protected LinkedHashSet<MediaUpload> uploadPendingMediaList = new LinkedHashSet();
    protected LinkedHashSet<String> uploadPendingMediaListContents = new LinkedHashSet();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    protected MediaNotUploaded mediaNotUploaded;
    protected MediaUploaded mediaUploaded;
    protected MediaDelete mediaDelete;
    private ViewPagerAdapter adapter;
    private final MySharePreference sp = new MySharePreference();

    Dialog messageDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_gallery);

        messageDialog = new Dialog(this);
        mediaUploadUtil = new MediaUploadUtil(this);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.videoViewPager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        mediaNotUploaded = new MediaNotUploaded();
        mediaUploaded = new MediaUploaded();
        mediaDelete = new MediaDelete();

        adapter.addFragment(mediaNotUploaded, "Not Uploaded");
        adapter.addFragment(mediaUploaded, "Uploaded");
        adapter.addFragment(mediaDelete, "Recycle Bin");


        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_media_cloud_not_uploaded);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_media_cloud_uploaded);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_media_delete_forever);

        tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.themeYellow), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.themeRed), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.themeRed), PorterDuff.Mode.SRC_IN);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabLayout.getTabAt(tab.getPosition()).getIcon().setColorFilter(getResources().getColor(R.color.themeYellow), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tabLayout.getTabAt(tab.getPosition()).getIcon().setColorFilter(getResources().getColor(R.color.themeRed), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        autoSync = (Switch) findViewById(R.id.autoSync);

        myPrefs = getSharedPreferences("autoSync_Status", Context.MODE_PRIVATE);
        autoSync.setChecked(myPrefs.getBoolean("value", true));
        autoSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (autoSync.isChecked()) {
                    SharedPreferences.Editor editor = getSharedPreferences("autoSync_Status", Context.MODE_PRIVATE).edit();
                    editor.putBoolean("value", true);
                    editor.apply();
                    autoSync.setChecked(true);
                } else {
                    SharedPreferences.Editor editor = getSharedPreferences("autoSync_Status", Context.MODE_PRIVATE).edit();
                    editor.putBoolean("value", false);
                    editor.apply();
                    autoSync.setChecked(false);
                }
            }
        });

        if (uploadPendingMediaList != null && !uploadPendingMediaList.isEmpty()) {
            uploadPendingMediaList.clear();
        }

        if (uploadPendingMediaList != null) {
            uploadPendingMediaList.addAll(mediaUploadUtil.readPendingUploadMedia());
        }

        if (uploadPendingMediaListContents != null && !uploadPendingMediaListContents.isEmpty()) {
            uploadPendingMediaListContents.clear();
        }

        if (uploadPendingMediaList != null) {
            Iterator<MediaUpload> itr = uploadPendingMediaList.iterator();
            while (itr.hasNext()) {
                if (uploadPendingMediaListContents != null) {
                    uploadPendingMediaListContents.add(itr.next().getMedia_File_Name());
                }
            }

        }

        // below code is to check if device is connected to Wifi or Mobile Data
        short wifiStatus = NetworkUtil.getConnectivityStatusString(this);

        //Insert into DB while capturing a media...
        if (uploadPendingMediaList != null && !uploadPendingMediaList.isEmpty() && autoSync.isChecked() && wifiStatus == 0) {
            s3uploaderObj = new S3Uploader(this);
            Iterator<MediaUpload> i = uploadPendingMediaList.iterator();
            while (i.hasNext()) {
                MediaUpload obj = i.next();
                if (obj.getIs_Media_Uploaded().equals(Constants.NOT_UPLOADED_S3)) {
                    checkedMediaList.add(obj.getMedia_File_Name());
                }
            }

            AsyncImageUpload asyncTask = new AsyncImageUpload(this);
            asyncTask.execute();
        }
    }

    private void alertDialog(String message, boolean btnFlag) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(message);
        dialog.setTitle("Warning!!!");
        dialog.setIcon(android.R.drawable.ic_dialog_alert);

        if (btnFlag) {
            dialog.setCancelable(false);
            dialog.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            short wifiStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
                            if (wifiStatus == 0) {
                                LinkedHashSet<MediaUpload> uploadedMediaList = new LinkedHashSet();
                                try {
                                    if (uploadPendingMediaList != null) {
                                        Iterator<MediaUpload> itr = uploadPendingMediaList.iterator();
                                        s3uploaderObj = new S3Uploader(getApplicationContext());
                                        Gson gson = new Gson();
                                        int teamId = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getId();
                                        String teamName = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getName();

                                        while (itr.hasNext()) {
                                            String mediFileName = itr.next().getMedia_File_Name();
                                            File file = new File(mediFileName);
                                            Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);

                                            uploadedMediaList = mediaUploadUtil.readMediaUpload(mediFileName);

                                            if (uploadedMediaList != null && uploadedMediaList.isEmpty()) {

                                                MediaUpload mediaUpload = new MediaUpload();
                                                mediaUpload.setMedia_File_Name(mediFileName);
                                                mediaUpload.setIs_Media_Uploaded(Constants.NOT_UPLOADED_S3);
                                                mediaUpload.setTeam_Id(teamId);
                                                mediaUpload.setTeam_Name(teamName);

                                                mediaUploadUtil.insertMediaUpload(mediaUpload);

                                                if (uploadFileToS3(fileUri, file)) {
                                                    updateUploadedMedia(mediaUpload, Constants.UPLOADED_S3);
                                                }

                                            } else if (uploadedMediaList != null) {
                                                Iterator<MediaUpload> itr1 = uploadedMediaList.iterator();
                                                while (itr1.hasNext()) {
                                                    MediaUpload mediaUpload = new MediaUpload();
                                                    mediaUpload = itr1.next();

                                                    if (mediaUpload.getIs_Media_Uploaded() != null && mediaUpload.getIs_Media_Uploaded().equals(Constants.NOT_UPLOADED_S3) && uploadFileToS3(fileUri, file)) {
                                                        updateUploadedMedia(mediaUpload, Constants.UPLOADED_S3);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } finally {
                                    if (uploadedMediaList != null && !uploadedMediaList.isEmpty()) {
                                        uploadedMediaList.clear();
                                    }
                                }
                            }
                        }
                    });
            doUnmark(false);
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = getSharedPreferences("autoSync_Status", Context.MODE_PRIVATE).edit();
                    editor.putBoolean("value", false);
                    editor.apply();
                    autoSync.setChecked(false);
                }
            });

        }
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        if (!autoSync.isChecked()) {
            doMark(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_gallery_submenu, menu);

        autoSync = (Switch) findViewById(R.id.autoSync);

        autoSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    alertDialog(Constants.CONFIRMATION_MSG, true);
                }
            }
        });

        return true;
    }

    private void doMark(boolean bypassMsg) {
        if (!autoSync.isChecked() && contents != null && !contents.isEmpty()) {
            checkedMediaList.addAll(contents);

            mAdapter.selectAll();
        } else if (contents != null && contents.isEmpty()) {
            showPopup(Constants.NO_MEDIA_FOUND, false);
        } else if (bypassMsg) {
            alertDialog(Constants.WARNING_MSG, false);
        }
    }

    public void doUnmark(boolean bypassMsg) {
        if (autoSync.isChecked() || (contents != null && !contents.isEmpty())) {
            mediaNotUploaded.doUnmark(bypassMsg);
        } else if (contents != null && contents.isEmpty()) {
            showPopup(Constants.NO_MEDIA_FOUND, false);
        } else if (bypassMsg) {
            alertDialog(Constants.WARNING_MSG, false);
        }
        checkedMediaList.clear();
    }

    /**
     * Check the Checkbox if not checked
     **/
    public void checkCheckBox(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, true);
        else
            mSelectedItemsIds.delete(position);

        mAdapter.notifyDataSetChanged();
    }

    /**
     * Remove all checkbox Selection
     **/
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        mAdapter.notifyDataSetChanged();
    }


    /**
     * Return the selected Checkbox IDs
     **/
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }


    private void uploadImageTos3(String Path) {
        final String path = Path;

        if (path != null) {
            s3uploaderObj.initUpload(path, false);
            s3uploaderObj.setOns3UploadDone(new S3Uploader.S3UploadInterface() {
                @Override
                public void onUploadSuccess(String response) {
                    if (response.equalsIgnoreCase("Success")) {
                        hideLoading();
                        urlFromS3 = S3Utils.generates3ShareUrl(getApplicationContext(), path);
                    }
                }

                @Override
                public void onUploadError(String response) {
                    hideLoading();
                    Toast.makeText(getApplicationContext(), urlFromS3 + ":" + response, Toast.LENGTH_SHORT).show();
                    Log.e("", "Error Uploading");

                }
            });
        }
    }

    private void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public boolean uploadFileToS3(Uri fileUri, File file) {
        try {
            uploadImageTos3(file.getAbsolutePath());
            s3uploaderObj = new S3Uploader(this);
            progressDialog = new ProgressDialog(this);
        } catch (Exception e) {
            Log.d("Exception:", e.getMessage());
            return false;
        }
        return true;
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> lstFragments = new ArrayList<>();
        private final List<String> lstTitles = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return lstFragments.get(position);
        }

        @Override
        public int getCount() {
            return lstTitles.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return lstTitles.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            lstFragments.add(fragment);
            lstTitles.add(title);
        }
    }

    private void showPopup(String msg, Boolean isSuccess) {
        if(isSuccess){
            messageDialog.setContentView(R.layout.popup_message_positive);
        }
        else{
            messageDialog.setContentView(R.layout.popup_message_negative);
        }
        ImageView closeImg = (ImageView)messageDialog.findViewById(R.id.closePopupImg);
        Button closeBtn = messageDialog.findViewById(R.id.closePopupButton);
        TextView alertMsg = messageDialog.findViewById(R.id.message);
        alertMsg.setText(msg);

        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        messageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        messageDialog.show();
    }

    private void updateUploadedMedia(MediaUpload mediaUpload, String status) {
        MediaUpload updateMediaUpload = new MediaUpload();
        updateMediaUpload.setIs_Media_Uploaded(status);
        updateMediaUpload.setMedia_File_Name(mediaUpload.getMedia_File_Name());
        updateMediaUpload.setTeam_Name(mediaUpload.getTeam_Name());
        updateMediaUpload.setTeam_Id(mediaUpload.getTeam_Id());

        mediaUploadUtil.updateMediaUpload(updateMediaUpload);
    }
}
