package com.ffg.goingtoschool;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;
import com.ffg.goingtoschool.database.MediaUploadUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class MediaUploaded extends Fragment {

    protected Switch autoSync;
    protected SharedPreferences mPrefs;
    protected LinkedHashSet<String> contents = new LinkedHashSet();
    protected List<String> checkedMediaList = new ArrayList();
    protected LinkedHashSet<MediaUpload> uploadedMediaList = new LinkedHashSet();
    protected LinkedHashSet<String> uploadedMediaListContents = new LinkedHashSet();
    private View v;
    private Context context;
    private GalleryAdapter mAdapter;
    private RecyclerView recyclerView;
    private MediaUploadUtil mediaUploadUtil;
    private short wifiStatus = -1;

    public MediaUploaded() {
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        context = getActivity();

        mediaUploadUtil = new MediaUploadUtil(context);

        if (contents != null && !contents.isEmpty()) {
            contents.clear();
        }

        if (contents != null) {
            contents.addAll(FetchFileNames());
        }

        if (uploadedMediaList != null && !uploadedMediaList.isEmpty()) {
            uploadedMediaList.clear();
        }

        if (uploadedMediaList != null) {
            uploadedMediaList.addAll(mediaUploadUtil.readUploadedMedia());
        }

        if (uploadedMediaListContents != null && !uploadedMediaListContents.isEmpty()) {
            uploadedMediaListContents.clear();
        }

        if (uploadedMediaList != null) {
            Iterator<MediaUpload> itr = uploadedMediaList.iterator();
            while (itr.hasNext()) {
                if (uploadedMediaListContents != null) {
                    uploadedMediaListContents.add(itr.next().getMedia_File_Name());
                }
            }
        }

        recyclerView = (RecyclerView) getView().findViewById(R.id.recycler_view);

        mAdapter = new GalleryAdapter(getActivity(), new ArrayList<String>(uploadedMediaListContents));
        setHasOptionsMenu(true);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getActivity(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i;
                ArrayList<String> _uploadedMediaListContents = new ArrayList(uploadedMediaListContents);

                try {
                    if (_uploadedMediaListContents.get(position).contains(Constants.IMAGE_EXTENSION)) {
                        i = new Intent(getActivity(), ImageViewerActivity.class);
                        i.putExtra("filepath", _uploadedMediaListContents.get(position));
                        startActivity(i);
                    } else if (_uploadedMediaListContents.get(position).contains(Constants.VIDEO_EXTENSION)) {
                        i = new Intent(getActivity(), ExoPlayerActivity.class);
                        i.putExtra("filepath", _uploadedMediaListContents.get(position));
                        startActivity(i);
                    }

                } finally {
                    if (_uploadedMediaListContents != null && !_uploadedMediaListContents.isEmpty()) {
                        _uploadedMediaListContents.clear();
                    }
                    if (_uploadedMediaListContents != null && !_uploadedMediaListContents.isEmpty()) {
                        _uploadedMediaListContents.clear();
                    }
                }

            }

            @Override
            public void onLongClick(View view, int position) {
                ArrayList<MediaUpload> _uploadedMediaList = new ArrayList(uploadedMediaList);
                ArrayList<String> _uploadedMediaListContents = new ArrayList(uploadedMediaListContents);
                try {
                    for (int i = 0; i < uploadedMediaList.size(); i++) {
                        if (_uploadedMediaList.get(i).getMedia_File_Name().contains(_uploadedMediaListContents.get(position))
                                && !(checkedMediaList.contains(_uploadedMediaListContents.get(position)))) {

                            ((CheckBox) (view.findViewById(R.id.chkBox))).setChecked(true);
                            view.findViewById(R.id.chkBox).setVisibility(View.VISIBLE);
                            checkedMediaList.add(_uploadedMediaListContents.get(position));
                            break;
                        } else if (checkedMediaList.contains(_uploadedMediaListContents.get(position))) {
                            ((CheckBox) (view.findViewById(R.id.chkBox))).setChecked(false);
                            view.findViewById(R.id.chkBox).setVisibility(View.GONE);
                            checkedMediaList.remove(_uploadedMediaListContents.get(position));
                            break;
                        }
                    }
                } finally {
                    if (_uploadedMediaListContents != null && !_uploadedMediaListContents.isEmpty()) {
                        _uploadedMediaListContents.clear();
                    }
                    if (_uploadedMediaList != null && !_uploadedMediaList.isEmpty()) {
                        _uploadedMediaList.clear();
                    }
                }
            }
        }));

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater layout, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = layout.inflate(R.layout.activity_video_gallery_upload, container, false);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private LinkedHashSet<String> FetchFileNames() {

        LinkedHashSet<String> filenames = new LinkedHashSet<String>();
        String path = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + Constants.GALLERY_DIRECTORY_NAME;
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        if (null != files) {
            Log.d("Files", "Size: " + files.length);
            for (File f : files) {
                Log.d("Files", "FileName:" + f.getName());
                String file_name = f.getName();
                // you can store name to arraylist and use it later
                filenames.add(path + "/" + file_name);
            }
        }
        return filenames;
    }

    protected void doMark(boolean bypassMsg) {
        if (uploadedMediaListContents != null && !uploadedMediaListContents.isEmpty()) {
            checkedMediaList.addAll(uploadedMediaListContents);
            mAdapter.selectAll();
        } else if (uploadedMediaListContents != null && uploadedMediaListContents.isEmpty()) {
            Toast.makeText(getActivity(), "No Media found, please take a image/video from capture option", Toast.LENGTH_SHORT).show();
        } else if (bypassMsg) {
            alertDialog(Constants.WARNING_MSG, false);
        }
    }

    public void doUnmark(boolean bypassMsg) {
        if ((checkedMediaList != null && !checkedMediaList.isEmpty())) {
            mAdapter.deselectAll();
        } else if (uploadedMediaListContents != null && uploadedMediaListContents.isEmpty() && checkedMediaList != null && checkedMediaList.isEmpty()) {
            Toast.makeText(getActivity(), "No Media found, please take an image/video from Capture option", Toast.LENGTH_SHORT).show();
        } else if (bypassMsg && uploadedMediaListContents != null && !uploadedMediaListContents.isEmpty() && checkedMediaList != null && checkedMediaList.isEmpty()) {
            Toast.makeText(getActivity(), "Nothing to Unmark", Toast.LENGTH_SHORT).show();
        } else if (!bypassMsg) {
            alertDialog(Constants.WARNING_MSG, false);
        }

        if (checkedMediaList != null) {
            checkedMediaList.clear();
        }
    }


    private void alertDialog(String message, boolean btnFlag) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(message);
        dialog.setTitle("Warning!!!");
        dialog.setIcon(android.R.drawable.ic_dialog_alert);

        if (btnFlag) {
            dialog.setCancelable(false);
            dialog.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            doUnmark(false);
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = context.getSharedPreferences("autoSync_Status", Context.MODE_PRIVATE).edit();
                    editor.putBoolean("value", false);
                    editor.apply();
                }
            });

        }
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.MarkAll:
                doMark(true);
                return true;
            case R.id.UnmarkAll:
                doUnmark(false);
                return true;

            case R.id.Upload:
            case R.id.Recover:
                alertDialog(Constants.DISABLE_OPTION_MSG, false);
                mAdapter.deselectAll();
                return true;

            case R.id.Delete:
                wifiStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                if (checkedMediaList != null && !checkedMediaList.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    LinkedHashSet<MediaUpload> deleteMediaList = new LinkedHashSet();

                    try {
                        for (int i = 0; i < checkedMediaList.size(); i++) {

                            deleteMediaList = mediaUploadUtil.readMediaUpload(checkedMediaList.get(i));

                            if (deleteMediaList != null && !deleteMediaList.isEmpty()) {
                                Iterator<MediaUpload> itr = deleteMediaList.iterator();
                                while (itr.hasNext()) {
                                    MediaUpload mediaUpload = new MediaUpload();
                                    mediaUpload = itr.next();

                                    if (mediaUpload.getIs_Media_Uploaded() != null && mediaUpload.getIs_Media_Uploaded().equals(Constants.UPLOADED_S3)) {
                                        updateUploadedMedia(mediaUpload, Constants.PENDING_DELETE_S3);
                                    }
                                }
                            }

                        }
                    } finally {
                        if (deleteMediaList != null && !deleteMediaList.isEmpty()) {
                            deleteMediaList.clear();
                        }
                    }
                    doUnmark(false);
                } else if (uploadedMediaListContents != null && uploadedMediaListContents.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    Toast.makeText(getActivity(), "No Media found to delete.", Toast.LENGTH_LONG).show();
                } else if (checkedMediaList != null && checkedMediaList.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    Toast.makeText(getActivity(), "Please select at least one Media", Toast.LENGTH_SHORT).show();
                } else if (checkedMediaList != null && !checkedMediaList.isEmpty() && (wifiStatus != 0 || wifiStatus != 1)) {
                    Toast.makeText(getActivity(), "Please enable, either wifi or mobile data.", Toast.LENGTH_LONG).show();
                } else {
                    alertDialog(Constants.WARNING_MSG, false);
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //set the status as pending delete
    private void updateUploadedMedia(MediaUpload mediaUpload, String status) {
        MediaUpload updateMediaUpload = new MediaUpload();
        updateMediaUpload.setIs_Media_Uploaded(status);
        updateMediaUpload.setMedia_File_Name(mediaUpload.getMedia_File_Name());
        updateMediaUpload.setTeam_Name(mediaUpload.getTeam_Name());
        updateMediaUpload.setTeam_Id(mediaUpload.getTeam_Id());

        mediaUploadUtil.updateMediaUpload(updateMediaUpload);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
