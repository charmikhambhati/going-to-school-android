package com.ffg.goingtoschool;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.ffg.goingtoschool.Animations.ZoomOutPageTransformer;
import com.ffg.goingtoschool.S3.S3Uploader;
import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;
import com.ffg.goingtoschool.database.MediaUploadUtil;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static Context contextOfApplication;
    private ImageView logoutBtn;
    public LottieAnimationView animation;
    protected S3Uploader s3uploaderObj;
    protected SharedPreferences mPrefs;
    protected List<String> checkedMediaList = new ArrayList();
    protected MediaUploadUtil mediaUploadUtil = new MediaUploadUtil(MainActivity.this);
    protected String urlFromS3;

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION) && intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false) && mPrefs.getBoolean("value", true)) {
                //Need to check autosync status
                List<MediaUpload> uploadPendingMediaList = new ArrayList();
                uploadPendingMediaList.addAll(mediaUploadUtil.readPendingUploadMedia());

                if (uploadPendingMediaList != null && !uploadPendingMediaList.isEmpty()) {
                    for (int n = 0; n < uploadPendingMediaList.size(); n++) {
                        if (uploadPendingMediaList.get(n).getIs_Media_Uploaded().equals(Constants.NOT_UPLOADED_S3)) {
                            checkedMediaList.add(uploadPendingMediaList.get(n).getMedia_File_Name());
                        }
                    }
                    AsyncImageUpload asyncTask = new AsyncImageUpload(MainActivity.this);
                    asyncTask.execute();

                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (null != broadcastReceiver) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    private void logout() {
        android.app.AlertDialog alert;
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Sign out ?")
                .setTitle("Alert")
                .setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Cognito cognito = new Cognito(MainActivity.this, MainActivity.this);
                if (cognito.signOut()) {
                    Log.d(this.getClass().getSimpleName(), "Redirecting to Login Page");
                    Intent loginActivity = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(loginActivity);
                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "Sign out Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
        alert = builder.create();

        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Context context = MainActivity.this;

                Button negButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                negButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                negButton.setTextColor(context.getResources().getColor(R.color.themeRed));

                Button posButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                posButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                posButton.setTextColor(context.getResources().getColor(R.color.themeYellow));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(20, 0, 0, 0);
                negButton.setLayoutParams(params);
            }
        });

        alert.show();
    }

    public void showLoading() {
        animation.setVisibility(View.VISIBLE);
        animation = (LottieAnimationView) findViewById(R.id.thumb_down);
        animation.setRepeatCount(LottieDrawable.INFINITE);
        animation.playAnimation();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        animation.pauseAnimation();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        animation.setVisibility(View.INVISIBLE);
    }

    public static Context getContextOfApplication() {
        return contextOfApplication;
    }

    class ViewPagerAdaptor extends FragmentPagerAdapter {
        private String role;

        public ViewPagerAdaptor(FragmentManager fragmentManager, String role) {
            super(fragmentManager);
            this.role = role;
        }

        @Override
        public Fragment getItem(int position) {
            if (Constants.STUDENT.equalsIgnoreCase(role)) {
                switch (position) {
                    case 0:
                        return new FacDashboardActivity();
                    case 1:
                        return new TeamCreation();
                    case 2:
                        return new ProblemDashboard();
                    default:
                        return null;
                }
            } else {
                switch (position) {
                    case 0:
                        return new FacDashboardActivity();
                    case 1:
                        return new ProblemDashboard();
                    default:
                        return null;
                }
            }

        }

        @Override
        public int getCount() {
            if (role.equalsIgnoreCase(Constants.STUDENT)) {
                return 3;
            } else {
                return 2;
            }
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            if (Constants.STUDENT.equalsIgnoreCase(role)) {
                switch (position) {
                    case 0:
                        return "My Dashboard";
                    case 1:
                        return "Team";
                    case 2:
                        return "Problem Dashboard";
                    default:
                        return null;
                }
            } else {
                switch (position) {
                    case 0:
                        return "Dashboard";
                    case 1:
                        return "Problem Dashboard";
                    default:
                        return null;
                }
            }

        }
    }

    @Override
    public void onBackPressed() {
        // do nothing
        Toast.makeText(this, "Back is not allowed. Please log out", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contextOfApplication = getApplicationContext();

        final MySharePreference sp = new MySharePreference();

        Log.d(this.getClass().getSimpleName(), "Logged in User :" + sp.getStringFromMyPref(Constants.LOGGED_IN_USER, ""));
        Log.d(this.getClass().getSimpleName(), "Token Exipration time : " + (new Date(sp.getLongFromMyPref(Constants.TOKEN_EXPIRY_TIME, 0L))));

        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.mainViewPager);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        PagerAdapter pagerAdapter = new ViewPagerAdaptor(getSupportFragmentManager(), sp.getStringFromMyPref(Constants.LOGGED_IN_USER_ROLE, ""));
        viewPager.setAdapter(pagerAdapter);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        logoutBtn = (ImageView) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        animation = (LottieAnimationView) findViewById(R.id.thumb_down);
        animation.setRepeatCount(LottieDrawable.INFINITE);

        s3uploaderObj = new S3Uploader(this);
        short wifiStatus = NetworkUtil.getConnectivityStatusString(this);
        mPrefs = getSharedPreferences("autoSync_Status", Context.MODE_PRIVATE);
        if (mPrefs.getBoolean("value", true) && wifiStatus == 0) {
            List<MediaUpload> uploadPendingMediaList = new ArrayList();
            uploadPendingMediaList.addAll(mediaUploadUtil.readPendingUploadMedia());

            if (uploadPendingMediaList != null && !uploadPendingMediaList.isEmpty()) {
                for (int n = 0; n < uploadPendingMediaList.size(); n++) {
                    if (uploadPendingMediaList.get(n).getIs_Media_Uploaded().equals(Constants.NOT_UPLOADED_S3)) {
                        checkedMediaList.add(uploadPendingMediaList.get(n).getMedia_File_Name());
                    }
                }
                AsyncImageUpload asyncTask = new AsyncImageUpload(this);
                asyncTask.execute();
            }
        }
    }
}
