package com.ffg.goingtoschool;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ffg.goingtoschool.S3.S3Uploader;
import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.ffg.goingtoschool.database.MediaUploadUtil;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class MediaNotUploaded extends Fragment {

    protected SharedPreferences mPrefs;
    protected LinkedHashSet<String> contents = new LinkedHashSet();
    protected List<String> checkedMediaList = new ArrayList();
    protected LinkedHashSet<MediaUpload> uploadPendingMediaList = new LinkedHashSet();
    protected LinkedHashSet<String> uploadPendingMediaListContents = new LinkedHashSet();
    private View v;
    private Context context;
    private GalleryAdapter mAdapter;
    private RecyclerView recyclerView;
    private MediaUploadUtil mediaUploadUtil;
    private S3Uploader s3uploaderObj;
    private ProgressDialog progressDialog;
    private short wifiStatus = -1;
    private final MySharePreference sp = new MySharePreference();

    public MediaNotUploaded() {
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        context = getActivity();
        mediaUploadUtil = new MediaUploadUtil(getActivity());
        s3uploaderObj = new S3Uploader(context);

        if (contents != null && !contents.isEmpty()) {
            contents.clear();
        }

        if (contents != null) {
            contents.addAll(FetchFileNames());
        }

        if (uploadPendingMediaList != null && !uploadPendingMediaList.isEmpty()) {
            uploadPendingMediaList.clear();
        }

        if (uploadPendingMediaList != null) {
            uploadPendingMediaList.addAll(mediaUploadUtil.readPendingUploadMedia());
        }

        if (uploadPendingMediaListContents != null && !uploadPendingMediaListContents.isEmpty()) {
            uploadPendingMediaListContents.clear();
        }

        if (uploadPendingMediaList != null) {
            Iterator<MediaUpload> itr = uploadPendingMediaList.iterator();
            while (itr.hasNext()) {
                if (uploadPendingMediaListContents != null) {
                    uploadPendingMediaListContents.add(itr.next().getMedia_File_Name());
                }
            }
        }

        mPrefs = context.getSharedPreferences("autoSync_Status", Context.MODE_PRIVATE);
        recyclerView = (RecyclerView) getView().findViewById(R.id.recycler_view);

        mAdapter = new GalleryAdapter(getActivity(), new ArrayList<String>(uploadPendingMediaListContents));
        setHasOptionsMenu(true);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getActivity(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i;
                ArrayList<String> _uploadPendingMediaListContents = new ArrayList(uploadPendingMediaListContents);
                try {
                    if (_uploadPendingMediaListContents.get(position).contains(Constants.IMAGE_EXTENSION)) {
                        i = new Intent(getActivity(), ImageViewerActivity.class);
                        i.putExtra("filepath", _uploadPendingMediaListContents.get(position));
                        startActivity(i);
                    } else if (_uploadPendingMediaListContents.get(position).contains(Constants.VIDEO_EXTENSION)) {
                        i = new Intent(getActivity(), ExoPlayerActivity.class);
                        i.putExtra("filepath", _uploadPendingMediaListContents.get(position));
                        startActivity(i);
                    }
                } finally {
                    if (_uploadPendingMediaListContents != null && !_uploadPendingMediaListContents.isEmpty()) {
                        _uploadPendingMediaListContents.clear();
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {

                if (!mPrefs.getBoolean("value", true)) {
                    ArrayList<MediaUpload> _uploadPendingMediaList = new ArrayList(uploadPendingMediaList);
                    ArrayList<String> _uploadPendingMediaListContents = new ArrayList(uploadPendingMediaListContents);
                    try {
                        for (int i = 0; i < _uploadPendingMediaList.size(); i++) {
                            if (_uploadPendingMediaList.get(i).getMedia_File_Name().contains(_uploadPendingMediaListContents.get(position))
                                    && !(checkedMediaList.contains(_uploadPendingMediaListContents.get(position)))) {

                                ((CheckBox) (view.findViewById(R.id.chkBox))).setChecked(true);
                                view.findViewById(R.id.chkBox).setVisibility(View.VISIBLE);
                                checkedMediaList.add(_uploadPendingMediaListContents.get(position));
                                break;
                            } else if (checkedMediaList.contains(_uploadPendingMediaListContents.get(position))) {
                                ((CheckBox) (view.findViewById(R.id.chkBox))).setChecked(false);
                                view.findViewById(R.id.chkBox).setVisibility(View.GONE);
                                checkedMediaList.remove(_uploadPendingMediaListContents.get(position));
                                break;
                            }
                        }
                    } finally {
                        if (_uploadPendingMediaListContents != null && !_uploadPendingMediaListContents.isEmpty()) {
                            _uploadPendingMediaListContents.clear();
                        }
                        if (_uploadPendingMediaList != null && !_uploadPendingMediaList.isEmpty()) {
                            _uploadPendingMediaList.clear();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "Auto-Sync functionality is Enabled", Toast.LENGTH_SHORT).show();
                }
            }
        }));
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater layout, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = layout.inflate(R.layout.activity_video_gallery_notupload, container, false);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private LinkedHashSet<String> FetchFileNames() {

        LinkedHashSet<String> filenames = new LinkedHashSet<String>();
        String path = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + Constants.GALLERY_DIRECTORY_NAME;
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        if (null != files) {
            Log.d("Files", "Size: " + files.length);
            for (File f : files) {
                Log.d("Files", "FileName:" + f.getName());
                String file_name = f.getName();
                // you can store name to arraylist and use it later
                filenames.add(path + "/" + file_name);
            }
        }
        return filenames;
    }

    protected void doMark(boolean bypassMsg) {
        if (!mPrefs.getBoolean("value", true) && uploadPendingMediaListContents != null && !uploadPendingMediaListContents.isEmpty()) {
            checkedMediaList.addAll(uploadPendingMediaListContents);

            mAdapter.selectAll();
        } else if (!mPrefs.getBoolean("value", true) && uploadPendingMediaListContents != null && uploadPendingMediaListContents.isEmpty()) {
            Toast.makeText(getActivity(), "No Media found, please take a image/video from capture option", Toast.LENGTH_SHORT).show();
        } else if (bypassMsg) {
            alertDialog(Constants.WARNING_MSG, false);
        }
    }

    public void doUnmark(boolean bypassMsg) {
        if (!mPrefs.getBoolean("value", true) || (checkedMediaList != null && !checkedMediaList.isEmpty())) {
            mAdapter.deselectAll();
        } else if (!mPrefs.getBoolean("value", true) && uploadPendingMediaListContents != null && uploadPendingMediaListContents.isEmpty() && checkedMediaList != null && checkedMediaList.isEmpty()) {
            Toast.makeText(getActivity(), "No Media found, please take an image/video from Capture option", Toast.LENGTH_SHORT).show();
        } else if (bypassMsg && uploadPendingMediaListContents != null && !uploadPendingMediaListContents.isEmpty() && checkedMediaList != null && checkedMediaList.isEmpty()) {
            Toast.makeText(getActivity(), "Nothing to Unmark", Toast.LENGTH_SHORT).show();
        } else if (!bypassMsg) {
            alertDialog(Constants.WARNING_MSG, false);
        }

        if (checkedMediaList != null) {
            checkedMediaList.clear();
        }
    }

    private void alertDialog(String message, boolean btnFlag) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage(message);
        dialog.setTitle("Warning!!!");
        dialog.setIcon(android.R.drawable.ic_dialog_alert);

        if (btnFlag) {
            dialog.setCancelable(false);
            dialog.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            doUnmark(false);
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = context.getSharedPreferences("autoSync_Status", Context.MODE_PRIVATE).edit();
                    editor.putBoolean("value", false);
                    editor.apply();
                }
            });
        }
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        if (!mPrefs.getBoolean("value", true) && !checkedMediaList.isEmpty()) {
            doUnmark(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.MarkAll:
                doMark(true);
                return true;
            case R.id.UnmarkAll:
                doUnmark(false);
                return true;
            case R.id.Upload:
                wifiStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                if (!mPrefs.getBoolean("value", true) && checkedMediaList != null && !checkedMediaList.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    LinkedHashSet<MediaUpload> uploadedMediaList = new LinkedHashSet();

                    try {
                        for (int i = 0; i < checkedMediaList.size(); i++) {

                            File file = new File(checkedMediaList.get(i));
                            Uri fileUri = CameraUtils.getOutputMediaFileUri(getActivity(), file);

                            uploadedMediaList = mediaUploadUtil.readMediaUpload(checkedMediaList.get(i));

                            if (uploadedMediaList != null && uploadedMediaList.isEmpty()) {

                                MediaUpload mediaUpload = new MediaUpload();

                                Gson gson = new Gson();
                                int teamId = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getId();
                                String teamName = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getName();

                                mediaUpload.setMedia_File_Name(checkedMediaList.get(i));
                                mediaUpload.setIs_Media_Uploaded(Constants.NOT_UPLOADED_S3);
                                mediaUpload.setId(teamId);
                                mediaUpload.setTeam_Name(teamName);

                                mediaUploadUtil.insertMediaUpload(mediaUpload);

                                if (uploadFileToS3(fileUri, file)) {
                                    updateUploadedMedia(mediaUpload, Constants.UPLOADED_S3);
                                }

                            } else if (uploadedMediaList != null) {
                                Iterator<MediaUpload> itr = uploadedMediaList.iterator();
                                while (itr.hasNext()) {
                                    MediaUpload mediaUpload = new MediaUpload();
                                    mediaUpload = itr.next();

                                    if (mediaUpload.getIs_Media_Uploaded() != null && mediaUpload.getIs_Media_Uploaded().equals(Constants.NOT_UPLOADED_S3) && uploadFileToS3(fileUri, file)) {
                                        updateUploadedMedia(mediaUpload, Constants.UPLOADED_S3);
                                    }
                                }
                            }
                        }
                    } finally {
                        if (uploadedMediaList != null && !uploadedMediaList.isEmpty()) {
                            uploadedMediaList.clear();
                        }
                    }
                    doUnmark(false);
                } else if (!mPrefs.getBoolean("value", true) && uploadPendingMediaListContents != null && uploadPendingMediaListContents.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    Toast.makeText(getActivity(), "No Media found, please take a image/video from capture option", Toast.LENGTH_LONG).show();
                } else if (!mPrefs.getBoolean("value", true) && checkedMediaList != null && checkedMediaList.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    Toast.makeText(getActivity(), "Please select at least one Media", Toast.LENGTH_SHORT).show();
                } else if (!mPrefs.getBoolean("value", true) && checkedMediaList != null && !checkedMediaList.isEmpty() && (wifiStatus != 0 || wifiStatus != 1)) {
                    Toast.makeText(getActivity(), "Please enable, either wifi or mobile data.", Toast.LENGTH_LONG).show();
                } else {
                    alertDialog(Constants.WARNING_MSG, false);
                }
                return true;

            case R.id.Delete:
                wifiStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                if (!mPrefs.getBoolean("value", true) && checkedMediaList != null && !checkedMediaList.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    LinkedHashSet<MediaUpload> deleteMediaList = new LinkedHashSet();

                    try {
                        for (int i = 0; i < checkedMediaList.size(); i++) {

                            deleteMediaList = mediaUploadUtil.readMediaUpload(checkedMediaList.get(i));

                            if (deleteMediaList != null && !deleteMediaList.isEmpty()) {
                                Iterator<MediaUpload> itr = deleteMediaList.iterator();
                                while (itr.hasNext()) {
                                    MediaUpload mediaUpload = new MediaUpload();
                                    mediaUpload = itr.next();

                                    if (mediaUpload.getIs_Media_Uploaded() != null && mediaUpload.getIs_Media_Uploaded().equals(Constants.NOT_UPLOADED_S3)) {
                                        updateUploadedMedia(mediaUpload, Constants.PENDING_DELETE_S3);
                                    }
                                }
                            }
                        }
                    } finally {
                        if (deleteMediaList != null && !deleteMediaList.isEmpty()) {
                            deleteMediaList.clear();
                        }
                    }
                    doUnmark(false);
                } else if (!mPrefs.getBoolean("value", true) && uploadPendingMediaListContents != null && uploadPendingMediaListContents.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    Toast.makeText(getActivity(), "No Media found to delete.", Toast.LENGTH_LONG).show();
                } else if (!mPrefs.getBoolean("value", true) && checkedMediaList != null && checkedMediaList.isEmpty() && (wifiStatus == 0 || wifiStatus == 1)) {
                    Toast.makeText(getActivity(), "Please select at least one Media", Toast.LENGTH_SHORT).show();
                } else if (!mPrefs.getBoolean("value", true) && checkedMediaList != null && !checkedMediaList.isEmpty() && (wifiStatus != 0 || wifiStatus != 1)) {
                    Toast.makeText(getActivity(), "Please enable, either wifi or mobile data.", Toast.LENGTH_LONG).show();
                } else {
                    alertDialog(Constants.WARNING_MSG, false);
                }
                return true;

            case R.id.Recover:
                alertDialog(Constants.DISABLE_OPTION_MSG, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean uploadFileToS3(Uri fileUri, File file) {
        try {
            uploadImageTos3(file.getAbsolutePath());
            s3uploaderObj = new S3Uploader(getActivity());
            progressDialog = new ProgressDialog(getActivity());
        } catch (Exception e) {
            Log.d("Exception:", e.getMessage());
            return false;
        }
        return true;
    }

    private void uploadImageTos3(String Path) {
        final String path = Path;

        if (path != null) {
            s3uploaderObj.initUpload(path, false);
            s3uploaderObj.setOns3UploadDone(new S3Uploader.S3UploadInterface() {
                @Override
                public void onUploadSuccess(String response) {
                    if (response.equalsIgnoreCase("Success")) {
                        hideLoading();
                    }
                }

                @Override
                public void onUploadError(String response) {
                    hideLoading();
                    Log.e("", "Error Uploading");

                }
            });
        }
    }

    private void updateUploadedMedia(MediaUpload mediaUpload, String status) {
        MediaUpload updateMediaUpload = new MediaUpload();
        updateMediaUpload.setIs_Media_Uploaded(status);
        updateMediaUpload.setMedia_File_Name(mediaUpload.getMedia_File_Name());
        updateMediaUpload.setTeam_Name(mediaUpload.getTeam_Name());
        updateMediaUpload.setTeam_Id(mediaUpload.getTeam_Id());

        mediaUploadUtil.updateMediaUpload(updateMediaUpload);
    }

    private void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
