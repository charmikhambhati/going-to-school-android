package com.ffg.goingtoschool;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.chaos.view.PinView;
import com.google.android.material.textfield.TextInputLayout;

public class SignUp extends AppCompatActivity {

    TextInputLayout etUsername;        // Enter Username
    TextInputLayout etEmail;           // Enter Email
    TextInputLayout etMobile;          // Enter Mobile
    TextInputLayout etPass;            // Enter Password
    TextInputLayout etRepeatPass;      // Repeat Password
    TextInputLayout etConfCode;        // Enter Confirmation Code

    TextInputLayout etMobileCntyCode;

    Button btnSignUp;           // Sending data to Cognito for registering new user
    Button btnVerify;
    private AwesomeValidation awesomeValidation; // for field validation
    Cognito authentication;
    private String userId;

    public static LinearLayout first, second;
    private PinView pinView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        authentication = new Cognito(getApplicationContext(), this);
        initViewComponents();

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        setValidationOnFields();

        //Hide Action Bar
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_actions, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SignUp.this, LoginActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    private void setValidationOnFields() {
        //adding validation to edittexts
        awesomeValidation.addValidation(this, R.id.etUsername, "^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$", R.string.usernameerror);
        awesomeValidation.addValidation(this, R.id.etEmail, Patterns.EMAIL_ADDRESS, R.string.emailerror);
        awesomeValidation.addValidation(this, R.id.etMobile, "^[0-9]{3,12}$", R.string.mobileerror);

        String regexPassword = "^[A-Za-z\\d@!$&_]{8,}$";
        awesomeValidation.addValidation(this, R.id.etPass, regexPassword, R.string.err_password);
        awesomeValidation.addValidation(this, R.id.etRepeatPass, R.id.etPass, R.string.repeat_passworderr);

    }

    private void initViewComponents(){
        etUsername = findViewById(R.id.etUsername);
        etEmail= findViewById(R.id.etEmail);
        etMobile = findViewById(R.id.etMobile);
        etPass = findViewById(R.id.etPass);
        etRepeatPass = findViewById(R.id.etRepeatPass);

        btnSignUp = findViewById(R.id.btnSignUp);
        btnVerify = findViewById(R.id.btnVerify);
        etMobileCntyCode = findViewById(R.id.etMobileCntyCode);

        first = findViewById(R.id.first_step);
        second = findViewById(R.id.secondStep);
        pinView = findViewById(R.id.pinView);
        first.setVisibility(View.VISIBLE);
        etMobileCntyCode.getEditText().setText(GetCountryZipCode());

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(awesomeValidation.validate() && etPass.getEditText().getText().toString().endsWith(etRepeatPass.getEditText().getText().toString())){
                    userId = etUsername.getEditText().getText().toString().replace(" ", "");
                    authentication.addAttribute("name", userId);

                    StringBuilder sb = new StringBuilder("+");
                    sb.append(etMobileCntyCode.getEditText().getText().toString());
                    sb.append(etMobile.getEditText().getText().toString());

                    authentication.addAttribute("phone_number", sb.toString());
                    authentication.addAttribute("email", etEmail.getEditText().getText().toString().replace(" ", ""));
                    authentication.signUpInBackground(userId, etPass.getEditText().getText().toString());
                }
                else{
                    Log.i("Ignore", "block");
                }
            }
        });

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authentication.confirmUser(userId, pinView.getText().toString().replace(" ", ""));
            }
        });
    }
}
