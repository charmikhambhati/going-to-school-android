package com.ffg.goingtoschool.ProblemDashBoardArtifacts;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.ffg.goingtoschool.CreateProblemDashboard;
import com.ffg.goingtoschool.ProblemDashboardDetails;
import com.ffg.goingtoschool.R;
import com.ffg.goingtoschool.S3.S3Utils;

import java.util.List;

public class ProblemDashboardAdapter extends PagerAdapter {

    List<ProblemDashboard> problemDashboards;
    LayoutInflater layoutInflater;
    Context context;

    public ProblemDashboardAdapter(List<ProblemDashboard> problemDashboards, Context context) {
        this.problemDashboards = problemDashboards;
        this.context = context;
    }

    @Override
    public int getCount() {
        return problemDashboards.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.problemdashboarditem, container, false);

        ImageView imageView;
        TextView title,desc;
        imageView = view.findViewById(R.id.dashboardImage);
        title = view.findViewById(R.id.dashboardImageTitle);

        setProfilePicture(problemDashboards.get(position).getCover_picture(),imageView);

        title.setText(problemDashboards.get(position).getProblemDashboardTitle());
        container.addView(view, 0);
        view.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                Intent intent =  null;
                if(problemDashboards.get(position).getProblemDashboardId() == 0){
                    //Open pop up to create new problem dashboard
                    intent = new Intent(context, CreateProblemDashboard.class);
                }else{
                    //Open exisiting problem dashboard details page
                    intent = new Intent(context, ProblemDashboardDetails.class);
                    intent.putExtra("selected_problem_dashboard", problemDashboards.get(position).getProblemDashboardTitle());
                    intent.putExtra("selected_problem_dashboard_id", problemDashboards.get(position).getProblemDashboardId());
                }
                v.getContext().startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    private void setProfilePicture(final String fileName,ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.progress_animation).centerCrop();
        requestOptions.error(R.mipmap.broken_foreground);
        requestOptions.skipMemoryCache(true);
        requestOptions.fitCenter();
        requestOptions.priority(Priority.HIGH);
        requestOptions.dontAnimate();
        requestOptions.dontTransform();
        if (fileName == null || "".equalsIgnoreCase(fileName)) {
            Log.d("Message:", "File Name is null");
        } else {
            String signedUrl = S3Utils.getS3Url(context, fileName);
            if(!"".equalsIgnoreCase(signedUrl)){
                if (S3Utils.checkObjectExistInS3(context, fileName)) {
                    Glide.with(imageView.getContext()).asBitmap().load(signedUrl).apply(requestOptions).into(imageView);
                }else{
                    Glide.with(imageView.getContext()).asBitmap().load(R.mipmap.broken_foreground).into(imageView);
                }
            }else{
                Glide.with(imageView.getContext()).asBitmap().load(R.mipmap.broken_foreground).into(imageView);
            }
        }
    }
}
