package com.ffg.goingtoschool.ProblemDashBoardArtifacts;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.ffg.goingtoschool.AttachImageActivity;
import com.ffg.goingtoschool.Constants;
import com.ffg.goingtoschool.ImageViewer;
import com.ffg.goingtoschool.R;
import com.ffg.goingtoschool.S3.AmazonUtil;
import com.google.gson.Gson;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProblemDashboardDetailsAdapter extends RecyclerView.Adapter<ProblemDashboardDetailsAdapter.ViewHolder>{
    //vars
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mattachedImagesNames = new ArrayList();
    private Context mContext;

    private String problemDashboardName;
    private int problemDashboardId;

    public ProblemDashboardDetailsAdapter(Context context, ArrayList<String> imageUrls,ArrayList<String> attachedImagesNames
                ,String dashName,int dashId) {
        mImageUrls = imageUrls;
        mContext = context;
        mattachedImagesNames = attachedImagesNames;

        problemDashboardName = dashName;
        problemDashboardId = dashId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.problem_dashboard_details_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.progress_animation).centerCrop();
        requestOptions.error(R.drawable.ic_error_outline_black_24dp);
        requestOptions.skipMemoryCache(true);
        requestOptions.fitCenter();
        requestOptions.priority(Priority.HIGH);
        requestOptions.dontAnimate();
        requestOptions.dontTransform();


        Glide.with(mContext)
                .asBitmap()
                .load(mImageUrls.get(position))
                .apply(requestOptions)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return mImageUrls.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView image;
        TextView name;

        private void gotoMediaViewer(int position) {
            Intent intent = new Intent(mContext, ImageViewer.class);
            intent.putExtra("url", mImageUrls.get(position));
            mContext.startActivity(intent);
        }

        private void gotoAttachImageActivity() {
            Gson gson = new Gson();
            String attachedImages = gson.toJson(mattachedImagesNames);
            String m_problemDashName = problemDashboardName;
            Intent intent = new Intent(mContext, AttachImageActivity.class);
            intent.putExtra("attachedImagesNames", attachedImages);
            intent.putExtra("selected_problem_dashboard", m_problemDashName);
            intent.putExtra("selected_problem_dashboard_id", problemDashboardId);
            mContext.startActivity(intent);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.dashboardImage);
            name = itemView.findViewById(R.id.dashboardImageTitle);
            final Vibrator vibe = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE) ;
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getLayoutPosition();
                    if (pos == 0) {
                        //Option to attach media
                        gotoAttachImageActivity();
                    } else {
                        gotoMediaViewer(pos);
                    }
                }
            });


            image.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = getLayoutPosition();

                    if (pos == 0) {
                        //Option to attach media

                    } else {
                        vibe.vibrate(50);

                        android.app.AlertDialog alert = null;
                        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setMessage("Do you want to remove the selected media from Dashboard")
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setIcon(R.drawable.ic_error_outline_black_24dp);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Thread thread = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Looper.prepare();
                                            Log.d("Exception catch:", "Image List size : " + mattachedImagesNames.size());
                                            Log.d("Exception catch:", "Image position : " + mattachedImagesNames.size());
                                            AmazonUtil.deleteObject(Constants.BUCKET_NAME, mattachedImagesNames.get(getLayoutPosition() - 1));
                                            mattachedImagesNames.remove(getLayoutPosition());
                                            Log.d("Exception catch:", "Image ist size : " + mattachedImagesNames.size());
                                        } catch (Exception e) {
                                            Log.d("Exception catch:", e.getMessage());
                                        }
                                    }
                                });
                                thread.start();
                                mImageUrls.remove(getLayoutPosition());
                                notifyItemRemoved(getLayoutPosition());
                            }
                        });

                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alert = builder.create();

                        alert.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {

                                Context context = mContext;

                                Button negButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                                negButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                                negButton.setTextColor(context.getResources().getColor(R.color.themeRed));

                                Button posButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                                posButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                                posButton.setTextColor(context.getResources().getColor(R.color.themeYellow));

                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                );
                                params.setMargins(20, 0, 0, 0);
                                negButton.setLayoutParams(params);
                            }
                        });
                        alert.show();
                    }
                    return true;
                }
            });
        }

    }
}
