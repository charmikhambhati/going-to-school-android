package com.ffg.goingtoschool.ProblemDashBoardArtifacts;

import android.view.View;

public interface RecyclerClickInterface {
    public void onClick(View view, int position);

    public void onLongClick(View view, int position);
}
