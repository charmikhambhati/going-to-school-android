package com.ffg.goingtoschool.ProblemDashBoardArtifacts;

public class ProblemDashboard {
    private int image;
    private int problemDashboardId;
    private String problemDashboardTitle;
    private int mediaCount;
    private String description;
    private String cover_picture;

    public ProblemDashboard(int problemDashboardId, String cover_picture, String problemDashboardTitle, String description) {
        this.cover_picture = cover_picture;
        this.problemDashboardTitle = problemDashboardTitle;
        this.description = description;
        this.problemDashboardId = problemDashboardId;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getProblemDashboardTitle() {
        return problemDashboardTitle;
    }

    public void setProblemDashboardTitle(String problemDashboardTitle) {
        this.problemDashboardTitle = problemDashboardTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMediaCount() {
        return mediaCount;
    }

    public void setMediaCount(int mediaCount) {
        this.mediaCount = mediaCount;
    }

    public int getProblemDashboardId() {
        return problemDashboardId;
    }

    public void setProblemDashboardId(int problemDashboardId) {
        this.problemDashboardId = problemDashboardId;
    }

    public String getCover_picture() {
        return cover_picture;
    }

    public void setCover_picture(String cover_picture) {
        this.cover_picture = cover_picture;
    }
}
