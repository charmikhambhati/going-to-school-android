package com.ffg.goingtoschool.ProblemDashBoardArtifacts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.ffg.goingtoschool.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    public static ArrayList<EditModel> editModelArrayList;


    public CustomAdapter(Context ctx, ArrayList<EditModel> editModelArrayList){
        inflater = LayoutInflater.from(ctx);
        this.editModelArrayList = editModelArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.rv_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.editText.getEditText().setText(editModelArrayList.get(position).getEditTextValue());
        holder.editText.setHint(editModelArrayList.get(position).getFieldName());
    }

    @Override
    public int getItemCount() {
        return editModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        protected TextInputLayout editText;

        public MyViewHolder(View itemView) {
            super(itemView);

            editText = (TextInputLayout) itemView.findViewById(R.id.description);

        }

    }
}
