package com.ffg.goingtoschool;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.amazonaws.util.StringUtils;
import com.ffg.goingtoschool.apiConsumer.APIClient;
import com.ffg.goingtoschool.apiConsumer.APIInterface;
import com.ffg.goingtoschool.apiConsumer.pojo.ResponseWrapper;
import com.ffg.goingtoschool.apiConsumer.pojo.School;
import com.ffg.goingtoschool.apiConsumer.pojo.Ward;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoggedInOptionsActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Button m_submitEnter;

    private Spinner m_wardList, m_schoolList;
    APIInterface apiInterface;
    final MySharePreference sp = new MySharePreference();
    List<String> wardStringList = new ArrayList<>();
    List<String> schoolStringList = new ArrayList<>();
    List<Integer> schoolIDList = new ArrayList<>();
    List<School> schoolList = new ArrayList<>();
    List<Ward> wards = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in_options);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        m_submitEnter = findViewById(R.id.enterSubmit);
        m_submitEnter.setOnClickListener(this);

        m_wardList = findViewById(R.id.wardList);
        m_schoolList = findViewById(R.id.schoolList);

        populateWards();
        m_wardList.setOnItemSelectedListener(this);
    }

    private void populateWards() {
        Call<ResponseWrapper<Ward>> call = apiInterface.getAllWards(sp.getStringFromMyPref(Constants.AWS_TOKEN,""));
        call.enqueue(new Callback<ResponseWrapper<Ward>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Ward>> call, Response<ResponseWrapper<Ward>> responseWrapper) {
                Log.d("TAG", responseWrapper.code()+"");

                if(responseWrapper.isSuccessful()){
                    ResponseWrapper<Ward> res = responseWrapper.body();
                    if(res.getStatus().equalsIgnoreCase("success")){
                        wards = res.getData();
                        Log.d("TAG","Wards -> " + wards.size());


                        for (Ward ward :wards) {
                            wardStringList.add(ward.getName());
                        }
                        ArrayAdapter<String> wardAdapter = new ArrayAdapter<String>(LoggedInOptionsActivity.this, R.layout.spinner_item, wardStringList);
                        m_wardList.setAdapter(wardAdapter);

                        // we have to clear school list also

                        schoolStringList.clear();
                        schoolList.clear();
                        schoolIDList.clear();
                        ArrayAdapter<String> schoolAdapter = new ArrayAdapter<String>(LoggedInOptionsActivity.this, R.layout.spinner_item, schoolStringList);
                        m_schoolList.setAdapter(schoolAdapter);
                    }
                }else{

                    Toast.makeText(getApplicationContext(), "Error while fetching wards", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", responseWrapper.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Ward>> call, Throwable t) {
                call.cancel();

                Toast.makeText(getApplicationContext(), "Error while fetching wards", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logoutSubmit:
                logOut(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void logOut(MenuItem item) {
        Intent loginActivity = new Intent(this, LoginActivity.class);
        startActivity(loginActivity);
        return;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.enterSubmit:
                if (m_wardList.getSelectedItemPosition() >= 0) {
                    if (m_schoolList.getSelectedItemPosition() >= 0) {
                        sp.addWardDetailsToMyPref(Constants.WARD_SP,wardStringList.get(m_wardList.getSelectedItemPosition()));
                        Log.d("the ward id was", String.valueOf(wards.get(m_wardList.getSelectedItemPosition()).getId()));
                        sp.addStringToMyPref(Constants.WARD_ID_SP, String.valueOf(wards.get(m_wardList.getSelectedItemPosition()).getId()));

                        sp.addSchoolToMyPref(Constants.SCHOOL_SP, schoolList.get(m_schoolList.getSelectedItemPosition()));
                        gotoDashboard();


                    } else {
                        Toast.makeText(this, "Please Select a School", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Please Select a Ward", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    private void gotoDashboard(){
        Intent intent = null;
        intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//because we use Application context
        this.startActivity(intent);
    }
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        switch (adapterView.getId()) {
            case R.id.wardList:
                String spinner_value= adapterView.getItemAtPosition(position).toString();
                populateSchools(spinner_value);
                break;
            default:
                break;
        }
    }

    private void populateSchools(String ward) {
        if(StringUtils.isBlank(ward)){
            Toast.makeText(this.getBaseContext(), "Please select a ward", Toast.LENGTH_LONG).show();
            return;
        }

        Call<ResponseWrapper<School>> call = apiInterface.getAllSchoolsInWard(sp.getStringFromMyPref(Constants.AWS_TOKEN,""),ward);
        call.enqueue(new Callback<ResponseWrapper<School>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<School>> call, Response<ResponseWrapper<School>> responseWrapper) {
                Log.d("TAG", responseWrapper.code() + "");
                if (responseWrapper.isSuccessful()) {
                    ResponseWrapper<School> res = responseWrapper.body();
                    if (res.getStatus().equalsIgnoreCase("success")) {
                        List<School> schools = res.getData();
                        Log.d("TAG", "Schools -> " + schools.size());

                        if(schools.size() == 0){
                            Toast.makeText(getApplicationContext(), "No schools associated with selected ward", Toast.LENGTH_SHORT).show();
                        }

                        schoolStringList.clear();
                        schoolIDList.clear();
                        schoolList.clear();
                        for (School school : schools) {
                            schoolStringList.add(school.getName());
                            schoolIDList.add(school.getId());
                            schoolList.add(school);
                        }
                        ArrayAdapter<String> schoolAdapter = new ArrayAdapter<String>(LoggedInOptionsActivity.this, R.layout.spinner_item, schoolStringList);
                        m_schoolList.setAdapter(schoolAdapter);

                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error while fetching schools", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", responseWrapper.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<School>> call, Throwable t) {
                call.cancel();
                Toast.makeText(getApplicationContext(), "Error while fetching schools", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }
}
