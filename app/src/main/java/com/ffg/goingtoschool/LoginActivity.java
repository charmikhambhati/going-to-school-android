package com.ffg.goingtoschool;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.amazonaws.util.StringUtils;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    public static Context contextOfApplication;
    private Button m_loginButton;
    private Button m_signUpButton;
    private TextView m_resetPasswordSubmit;
    private TextInputLayout userNameEditText;
    private TextInputLayout userPasswordEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        contextOfApplication = getApplicationContext();
        m_loginButton = findViewById(R.id.loginSubmit);
        m_signUpButton = findViewById(R.id.signupSubmit);
        m_resetPasswordSubmit = findViewById(R.id.resetPasswordSubmit);
        userNameEditText = findViewById(R.id.teamNameId);
        userPasswordEditText = findViewById(R.id.login_password);
        m_loginButton.setOnClickListener(this);
        m_signUpButton.setOnClickListener(this);
        m_resetPasswordSubmit.setOnClickListener(this);

        //To clear fields on login screen redirections
        userNameEditText.getEditText().setText("");
        userPasswordEditText.getEditText().setText("");

        //Hide Action Bar
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginSubmit: gotoLoggedIn();
                                    break;
            case R.id.signupSubmit: gotoSignUp();
                                    break;
            case R.id.resetPasswordSubmit: gotoForgetPassword();
                                    break;
            default:
                break;
        }
    }

    private void gotoForgetPassword() {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    private void gotoSignUp() {
        Intent intent = new Intent(this, SignUp.class);
        startActivity(intent);
    }

    private void gotoLoggedIn() {
        if (StringUtils.isBlank(userNameEditText.getEditText().getText().toString()) ||
                StringUtils.isBlank(userPasswordEditText.getEditText().getText().toString())) {
            Toast.makeText(getApplicationContext(), "Please enter credentials to login", Toast.LENGTH_SHORT).show();
        } else {
            Cognito authentication = new Cognito(getApplicationContext(), this);
            authentication.userLogin(userNameEditText.getEditText().getText().toString().replace(" ", ""), userPasswordEditText.getEditText().getText().toString());
        }
    }
    @Override
    public void onBackPressed() {
        // do nothing
    }

}
