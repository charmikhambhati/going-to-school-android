package com.ffg.goingtoschool;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.util.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.ffg.goingtoschool.ProblemDashBoardArtifacts.ProblemDashboardDetailsAdapter;
import com.ffg.goingtoschool.S3.AmazonUtil;
import com.ffg.goingtoschool.S3.S3Uploader;
import com.ffg.goingtoschool.S3.S3Utils;
import com.ffg.goingtoschool.apiConsumer.APIClient;
import com.ffg.goingtoschool.apiConsumer.APIInterface;
import com.ffg.goingtoschool.apiConsumer.pojo.Condition;
import com.ffg.goingtoschool.apiConsumer.pojo.DashboardUpdatePojo;
import com.ffg.goingtoschool.apiConsumer.pojo.ProblemDashboardPojo;
import com.ffg.goingtoschool.apiConsumer.pojo.ResponseWrapper;
import com.ffg.goingtoschool.apiConsumer.pojo.Update;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProblemDashboardDetails extends AppCompatActivity {
    private ArrayList<Integer> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ImageView coverPictureView;
    private TextView problemDashName;
    APIInterface apiInterface;
    final MySharePreference sp = new MySharePreference();

    private TextInputLayout pdDesctiption;
    private TextInputLayout pdAction;
    private FloatingActionButton editBtn;
    private FloatingActionButton saveBtn;

    private ArrayList<String> attachedImagesNames = new ArrayList();
    private int problemDashboardId;
    private ImageView logoutBtn;
    private Uri imageUri;
    Boolean coverPictureChanged;
    private S3Uploader s3uploaderObj;
    private List<String> listing;
    RecyclerView recyclerView;
    ProblemDashboardDetailsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_dashboard_details);

        coverPictureView = findViewById(R.id.coverPicture);
        problemDashName = (TextView) findViewById(R.id.problemDashName);
        problemDashName.setText(getIntent().getStringExtra("selected_problem_dashboard"));
        problemDashboardId = getIntent().getIntExtra("selected_problem_dashboard_id", 0);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        pdDesctiption = findViewById(R.id.pdDesc);
        pdAction = findViewById(R.id.pdAction);

        editBtn = findViewById(R.id.editPD);
        saveBtn = findViewById(R.id.savePD);
        logoutBtn = (ImageView) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        coverPictureView.setEnabled(false);
        s3uploaderObj = new S3Uploader(getApplicationContext());

        coverPictureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeCoverPicture();
            }
        });

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProblemDash();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImageTos3(imageUri);
            }
        });

        getProblemDashboardData();
        getImages();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void changeCoverPicture() {
        if (CameraUtils.checkPermissions(getApplicationContext())) {
            final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA}, Constants.MY_CAMERA_PERMISSION_CODE);
                            } else {
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Camera not supported", Toast.LENGTH_LONG).show();
                        }
                    } else if (options[item].equals("Choose from Gallery")) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } else {
            requestCameraPermission(Constants.MEDIA_TYPE_IMAGE);
        }
    }

    private void requestCameraPermission(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            changeCoverPicture();

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        } else {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showPermissionsAlert() {
        android.app.AlertDialog alert;
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(ProblemDashboardDetails.this);
        builder.setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setTitle("Permissions required!")
                .setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CameraUtils.openSettings(getApplicationContext());
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(ProblemDashboardDetails.this, "Sign out Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
        alert = builder.create();

        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Context context = ProblemDashboardDetails.this;

                Button negButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                negButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                negButton.setTextColor(context.getResources().getColor(R.color.themeRed));

                Button posButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                posButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                posButton.setTextColor(context.getResources().getColor(R.color.themeYellow));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(20, 0, 0, 0);
                negButton.setLayoutParams(params);
            }
        });

        alert.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.CAMERA_REQUEST) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                Uri tempUri = getImageUri(getApplicationContext(), thumbnail);
                File finalFile = new File(getRealPathFromURI(tempUri));
                Log.d("ACTIVITY RESULT", "final url : " + finalFile);
                imageUri = Uri.fromFile(finalFile);
                Bitmap bMapScaled = Bitmap.createScaledBitmap(thumbnail, coverPictureView.getWidth(), coverPictureView.getHeight(), true);
                coverPictureView.setImageBitmap(bMapScaled);
            } else if (requestCode == 2) {
                imageUri = data.getData();

                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(imageUri);
                } catch (FileNotFoundException e) {
                    Log.e("Exception:", e.getMessage());
                }
                if (imageStream != null) {
//                    coverPictureView.setImageBitmap(BitmapFactory.decodeStream(imageStream));


                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.progress_animation).centerCrop();
                    requestOptions.error(R.drawable.ic_error_outline_black_24dp);
                    requestOptions.skipMemoryCache(true);
                    requestOptions.fitCenter();
                    requestOptions.priority(Priority.HIGH);
                    requestOptions.dontAnimate();
                    requestOptions.dontTransform();


                    Glide.with(getApplicationContext()).asBitmap().load(BitmapFactory.decodeStream(imageStream)).apply(requestOptions).into(coverPictureView);
                }
            }
            coverPictureChanged = true;
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "IMG_" + timeStamp, null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    private void getProblemDashboardData() {
        showLoading();
        ProblemDashboardPojo pojo = new ProblemDashboardPojo();
        pojo.setName(problemDashName.getText().toString());

        Call<ResponseWrapper<ProblemDashboardPojo>> call =
                apiInterface.fetchProblemDashboard(sp.getStringFromMyPref(Constants.AWS_TOKEN, ""), pojo);
        call.enqueue(new Callback<ResponseWrapper<ProblemDashboardPojo>>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseWrapper<ProblemDashboardPojo>> call, Response<ResponseWrapper<ProblemDashboardPojo>> responseWrapper) {
                Log.d("TAG", responseWrapper.code() + "");
                if (responseWrapper.isSuccessful()) {
                    ResponseWrapper<ProblemDashboardPojo> res = responseWrapper.body();
                    if (res != null && res.getStatus() != null && res.getStatus().equalsIgnoreCase("success")) {
                        List<ProblemDashboardPojo> problems = res.getData();
                        Log.d("TAG", "ProblemDashboardModel -> " + problems.size());

                        if (problems.size() == 0) {
                            hideLoading();
                        }

                        for (ProblemDashboardPojo problem : problems) {
                            new com.ffg.goingtoschool.ProblemDashBoardArtifacts.ProblemDashboard(problem.getId(), problem.getCover_picture(), problem.getName(), problem.getSchool() + " " + problem.getWard());
                        }

                        setProfilePicture(problems.get(0).getCover_picture(), coverPictureView);
                        pdDesctiption.getEditText().setText(problems.get(0).getDescription());
                        pdAction.getEditText().setText(problems.get(0).getAction());

                        hideLoading();
                    }
                    hideLoading();
                } else {
                    Toast.makeText(getApplicationContext(), "Error while fetching dashboard", Toast.LENGTH_SHORT).show();
                    Log.e("TAG", responseWrapper.message());
                    hideLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ProblemDashboardPojo>> call, Throwable t) {
                call.cancel();
                Toast.makeText(getApplicationContext(), "Error while fetching dashboard", Toast.LENGTH_SHORT).show();
                hideLoading();
            }
        });
    }

    private void getImages() {

        attachedImagesNames.clear();
        mImageUrls.clear();
        mNames.clear();

        String team = sp.getStringFromMyPref(Constants.TEAM_SP, "");
        if (team != null && !"".equals(team)) {
            Log.w("team", "not empty");
        } else {
            Toast.makeText(getApplicationContext(), "Error while fetching dashboards", Toast.LENGTH_SHORT).show();
            hideLoading();
            return;
        }

        mImageUrls.add(getSignedUrlsForImages("download.png"));
        mNames.add(0);
        fetchFileFromS3();

    }

    public void fetchFileFromS3() {
        final String subDir = problemDashboardId + "_" + problemDashName.getText();
        Log.d("subdir formed", subDir);

        // Get List of files from S3 Bucket
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Looper.prepare();
                    listing = AmazonUtil.getObjectNamesForBucket(Constants.BUCKET_NAME, subDir);

                    for (int i = 0; i < listing.size(); i++) {
                        mImageUrls.add(getSignedUrlsForImages(listing.get(i)));
                        attachedImagesNames.add(listing.get(i));
                    }
                    Looper.loop();

                } catch (Exception e) {
                    Log.e("tag", "Exception found while listing " + e);
                }

            }
        });
        thread.start();

        initRecyclerView();
        hideLoading();
    }

    private void initRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView = findViewById(R.id.problemDashboardRecyclerview);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ProblemDashboardDetailsAdapter(this, mImageUrls,attachedImagesNames,(String)problemDashName.getText(),problemDashboardId);
        recyclerView.setAdapter(adapter);

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerView.setItemAnimator(itemAnimator);

    }

    private void showLoading() {
    }

    private void hideLoading() {
    }

    private String getSignedUrlsForImages(final String fileName) {
        String returnValue = null;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> result = executor.submit(new Callable<String>() {
            public String call() throws Exception {
                return S3Utils.generates3ShareUrlFromFileName(getApplicationContext(), fileName);
            }
        });
        try {
            returnValue = result.get();
        } catch (Exception exception) {
            //handle exception
        }
        return returnValue;
    }

    private void setProfilePicture(final String fileName, ImageView imageView) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> result = executor.submit(new Callable<String>() {
            public String call() throws Exception {
                return S3Utils.generates3ShareUrlFromFileName(getApplicationContext(), fileName);
            }
        });
        try {
            String returnValue = result.get();
            if (!StringUtils.isBlank(returnValue)) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.progress_animation).centerCrop();
                requestOptions.error(R.drawable.ic_error_outline_black_24dp);
                requestOptions.skipMemoryCache(true);
                requestOptions.fitCenter();
                requestOptions.priority(Priority.HIGH);
                requestOptions.dontAnimate();
                requestOptions.dontTransform();


                Glide.with(imageView.getContext()).asBitmap().load(returnValue).apply(requestOptions).into(imageView);
            }
        } catch (Exception exception) {
            //handle exception
        }
    }

    private void editProblemDash() {
        editBtn.setVisibility(View.GONE);
        saveBtn.setVisibility(View.VISIBLE);
        pdDesctiption.setEnabled(true);
        pdAction.setEnabled(true);
        coverPictureView.setEnabled(true);
    }

    private String getFilePathfromURI(Uri selectedImageUri) {
        if (selectedImageUri == null) {
            return "";
        }

        String filePath = "";
        if (selectedImageUri.toString().contains("file:")) {
            filePath = selectedImageUri.toString().substring(7);
        } else {
            Log.d("TAG", selectedImageUri.toString());
            String wholeID = DocumentsContract.getDocumentId(selectedImageUri);
            String id = wholeID.split(":")[1];  // Split at colon, use second item in the array
            String[] column = {MediaStore.Images.Media.DATA};
            String sel = MediaStore.Images.Media._ID + "=?"; // where id is equal to

            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath;
    }

    private void uploadImageTos3(Uri imageUri) {
        Log.d("TEAM_CREATION", "Entered Upload image to S3 bucket");

        final String path = getFilePathfromURI(imageUri);
        Log.d("TEAM_CREATION", "Image path : " + path);

        if (!"".equalsIgnoreCase(path) && coverPictureChanged) {
            s3uploaderObj.initUpload(path, true);
            s3uploaderObj.setOns3UploadDone(new S3Uploader.S3UploadInterface() {
                @Override
                public void onUploadSuccess(String response) {
                    if (response.equalsIgnoreCase("Success")) {
                        saveDashboardDetails((new File(path)).getName());
                    }
                }

                @Override
                public void onUploadError(String response) {
                    Log.e("TAG", "Error Uploading Image : " + response);
                }
            });
        } else {
            saveDashboardDetails(null);
        }
    }

    private void saveDashboardDetails(String coverPicture) {
        showLoading();
        Log.d("PD_UPDATION", "updating dashboard details");

        Update update = new Update();
        update.setAction(pdAction.getEditText().getText().toString().trim());
        update.setDescription(pdDesctiption.getEditText().getText().toString().trim());
        if (coverPicture != null || !"".equalsIgnoreCase(coverPicture)) {
            update.setCover_picture(coverPicture);
        }
        Condition condition = new Condition();
        condition.setId(problemDashboardId);
        DashboardUpdatePojo updatePojo = new DashboardUpdatePojo(condition, update);

        Call<DashboardUpdatePojo> call = apiInterface.updateProblemDashboard(sp.getStringFromMyPref(Constants.AWS_TOKEN, "")
                , updatePojo);
        call.enqueue(new Callback<DashboardUpdatePojo>() {
            @Override
            public void onResponse(Call<DashboardUpdatePojo> call, Response<DashboardUpdatePojo> response) {
                hideLoading();
                saveBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.VISIBLE);
                pdDesctiption.setEnabled(false);
                pdAction.setEnabled(false);
                coverPictureView.setEnabled(false);
                Toast.makeText(ProblemDashboardDetails.this, "Problem Dashbaord updated", Toast.LENGTH_LONG);
            }

            @Override
            public void onFailure(Call<DashboardUpdatePojo> call, Throwable t) {
                Toast.makeText(ProblemDashboardDetails.this, "Problem Dashboard updating failed", Toast.LENGTH_LONG);
                hideLoading();
            }
        });
        hideLoading();
    }

    private void logout() {
        android.app.AlertDialog alert;
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(ProblemDashboardDetails.this);
        builder.setMessage("Sign out ?")
                .setTitle("Alert")
                .setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Cognito cognito = new Cognito(ProblemDashboardDetails.this, ProblemDashboardDetails.this);
                if (cognito.signOut()) {
                    Log.d(this.getClass().getSimpleName(), "Redirecting to Login Page");
                    Intent loginActivity = new Intent(ProblemDashboardDetails.this, LoginActivity.class);
                    startActivity(loginActivity);
                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(ProblemDashboardDetails.this, "Sign out Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
        alert = builder.create();

        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Context context = ProblemDashboardDetails.this;

                Button negButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                negButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                negButton.setTextColor(context.getResources().getColor(R.color.themeRed));

                Button posButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                posButton.setBackgroundColor(context.getResources().getColor(R.color.white));
                posButton.setTextColor(context.getResources().getColor(R.color.themeYellow));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(20, 0, 0, 0);
                negButton.setLayoutParams(params);
            }
        });

        alert.show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ProblemDashboardDetails.this, MainActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}