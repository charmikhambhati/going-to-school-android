package com.ffg.goingtoschool;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.ffg.goingtoschool.S3.S3Uploader;
import com.ffg.goingtoschool.S3.S3Utils;
import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.google.gson.Gson;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class AsyncImageUpload extends AsyncTask<String, Void, Boolean> {

    private Context mContext;

    public AsyncImageUpload(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... params) {

        LinkedHashSet<MediaUpload> uploadedMediaList;
        final MySharePreference sp = new MySharePreference();

        Gson gson = new Gson();
        int teamId = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getId();
        String teamName = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getName();

        if (mContext instanceof VideoGalleryActivity) {
            for (int i = 0; i < ((VideoGalleryActivity) mContext).checkedMediaList.size(); i++) {

                File file = new File(((VideoGalleryActivity) mContext).checkedMediaList.get(i));
                Uri fileUri = CameraUtils.getOutputMediaFileUri(((VideoGalleryActivity) mContext).getApplicationContext(), file);

                uploadedMediaList = ((VideoGalleryActivity) mContext).mediaUploadUtil.readMediaUpload(((VideoGalleryActivity) mContext).checkedMediaList.get(i));

                if (uploadedMediaList != null && uploadedMediaList.isEmpty()) {

                    MediaUpload mediaUpload = new MediaUpload();
                    mediaUpload.setMedia_File_Name(((VideoGalleryActivity) mContext).checkedMediaList.get(i));
                    mediaUpload.setIs_Media_Uploaded(Constants.NOT_UPLOADED_S3);
                    mediaUpload.setTeam_Id(teamId);
                    mediaUpload.setTeam_Name(teamName);

                    ((VideoGalleryActivity) mContext).mediaUploadUtil.insertMediaUpload(mediaUpload);

                    if (uploadFileToS3(fileUri, file)) {
                        updateUploadedMedia(mediaUpload);
                    }

                } else if (uploadedMediaList != null) {
                    Iterator<MediaUpload> itr = uploadedMediaList.iterator();
                    while (itr.hasNext()) {
                        MediaUpload mediaUpload;
                        mediaUpload = itr.next();

                        if (mediaUpload.getIs_Media_Uploaded() != null && mediaUpload.getIs_Media_Uploaded().equals(Constants.NOT_UPLOADED_S3) && uploadFileToS3(fileUri, file)) {
                            updateUploadedMedia(mediaUpload);
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < ((MainActivity) mContext).checkedMediaList.size(); i++) {

                File file = new File(((MainActivity) mContext).checkedMediaList.get(i));
                Uri fileUri = CameraUtils.getOutputMediaFileUri(((MainActivity) mContext).getApplicationContext(), file);

                uploadedMediaList = ((MainActivity) mContext).mediaUploadUtil.readMediaUpload(((MainActivity) mContext).checkedMediaList.get(i));

                if (uploadedMediaList != null && uploadedMediaList.isEmpty()) {

                    MediaUpload mediaUpload = new MediaUpload();
                    mediaUpload.setMedia_File_Name(((MainActivity) mContext).checkedMediaList.get(i));
                    mediaUpload.setIs_Media_Uploaded(Constants.NOT_UPLOADED_S3);
                    mediaUpload.setTeam_Id(teamId);
                    mediaUpload.setTeam_Name(teamName);

                    ((MainActivity) mContext).mediaUploadUtil.insertMediaUpload(mediaUpload);

                    if (uploadFileToS3(fileUri, file)) {
                        updateUploadedMedia(mediaUpload);
                    }

                } else if (uploadedMediaList != null) {
                    Iterator<MediaUpload> itr = uploadedMediaList.iterator();
                    while (itr.hasNext()) {
                        MediaUpload mediaUpload;
                        mediaUpload = itr.next();

                        if (mediaUpload.getIs_Media_Uploaded() != null && mediaUpload.getIs_Media_Uploaded().equals(Constants.NOT_UPLOADED_S3) && uploadFileToS3(fileUri, file)) {
                            updateUploadedMedia(mediaUpload);
                        }
                    }
                }
            }
        }
        return Boolean.TRUE;
    }

    @Override
    protected void onPostExecute(Boolean bitmap) {
        super.onPostExecute(bitmap);
    }

    public boolean uploadFileToS3(Uri fileUri, File file) {
        try {
            uploadImageTos3(file.getAbsolutePath(), fileUri);
            if (mContext instanceof VideoGalleryActivity) {
                ((VideoGalleryActivity) mContext).s3uploaderObj = new S3Uploader(mContext);
            } else {
                ((MainActivity) mContext).s3uploaderObj = new S3Uploader(mContext);
            }

        } catch (Exception e) {
            Log.d("Exception:", e.getMessage());
            return false;
        }
        return true;
    }

    private void uploadImageTos3(String Path, Uri imageUri) {
        final String path = Path;

        if (path != null && mContext instanceof VideoGalleryActivity) {
            ((VideoGalleryActivity) mContext).s3uploaderObj.initUpload(path, false);
            ((VideoGalleryActivity) mContext).s3uploaderObj.setOns3UploadDone(new S3Uploader.S3UploadInterface() {
                @Override
                public void onUploadSuccess(String response) {
                    if (response.equalsIgnoreCase("Success")) {
                        ((VideoGalleryActivity) mContext).urlFromS3 = S3Utils.generates3ShareUrl(mContext, path);
                        if (!TextUtils.isEmpty(((VideoGalleryActivity) mContext).urlFromS3)) {
                            Log.d("", "Error Uploading");
                        }
                    }
                }

                @Override
                public void onUploadError(String response) {
                    Log.e("", "Error Uploading");
                }
            });
        } else if (path != null && mContext instanceof MainActivity) {
            ((MainActivity) mContext).s3uploaderObj.initUpload(path, false);
            ((MainActivity) mContext).s3uploaderObj.setOns3UploadDone(new S3Uploader.S3UploadInterface() {
                @Override
                public void onUploadSuccess(String response) {
                    if (response.equalsIgnoreCase("Success")) {
                        ((MainActivity) mContext).urlFromS3 = S3Utils.generates3ShareUrl(mContext, path);
                        if (!TextUtils.isEmpty(((MainActivity) mContext).urlFromS3)) {
                            Log.d("", "Error Uploading");
                        }
                    }
                }

                @Override
                public void onUploadError(String response) {
                    Log.e("", "Error Uploading");
                }
            });
        }
    }

    private void updateUploadedMedia(MediaUpload mediaUpload) {
        MediaUpload updateMediaUpload = new MediaUpload();
        updateMediaUpload.setIs_Media_Uploaded(Constants.UPLOADED_S3);
        updateMediaUpload.setMedia_File_Name(mediaUpload.getMedia_File_Name());
        updateMediaUpload.setTeam_Name(mediaUpload.getTeam_Name());
        updateMediaUpload.setTeam_Id(mediaUpload.getTeam_Id());

        if (mContext instanceof VideoGalleryActivity) {
            ((VideoGalleryActivity) mContext).mediaUploadUtil.updateMediaUpload(updateMediaUpload);
        } else {
            ((MainActivity) mContext).mediaUploadUtil.updateMediaUpload(updateMediaUpload);
        }

    }
}
