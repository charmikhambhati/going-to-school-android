package com.ffg.goingtoschool;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.ffg.goingtoschool.apiConsumer.pojo.MediaUpload;
import com.ffg.goingtoschool.apiConsumer.pojo.Team;
import com.ffg.goingtoschool.database.MediaUploadUtil;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.util.List;

import static com.ffg.goingtoschool.Constants.MEDIA_TYPE_IMAGE;

public class CaptureContentActivity extends AppCompatActivity {


    private String imageStoragePath;


    private Button recordBtn;
    private Button clickPictureBtn;
    private MediaUploadUtil mediaUploadUtil;
    final MySharePreference sp = new MySharePreference();

    Dialog messageDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_content);

        messageDialog = new Dialog(this);
        mediaUploadUtil = new MediaUploadUtil(this);

        /*Display Activity as a pop-up*/

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width * .7), (int) (height * .2));
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 10;
        params.y = -20;
        params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        params.dimAmount = 0.5f;
        getWindow().setAttributes(params);

        // Checking availability of the camera
        if (!CameraUtils.isDeviceSupportCamera(getApplicationContext())) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device doesn't have camera
            finish();
        }

        recordBtn = findViewById(R.id.recordVideo);
        clickPictureBtn = findViewById(R.id.clickPicture);

        /**
         * Capture image on button click
         */
        clickPictureBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (CameraUtils.checkPermissions(getApplicationContext())) {
                    captureImage();
                } else {
                    requestCameraPermission(Constants.MEDIA_TYPE_IMAGE);
                }
            }
        });

        /**
         * Record video on button click
         */
        recordBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (CameraUtils.checkPermissions(getApplicationContext())) {
                    captureVideo();
                } else {
                    requestCameraPermission(Constants.MEDIA_TYPE_VIDEO);
                }
            }
        });

        // restoring storage image path from saved instance state
        // otherwise the path will be null on device rotation
        restoreFromBundle(savedInstanceState);
    }

    /**
     * Restoring store image path from saved instance state
     */
    private void restoreFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(Constants.KEY_IMAGE_STORAGE_PATH)) {
            imageStoragePath = savedInstanceState.getString(Constants.KEY_IMAGE_STORAGE_PATH);
            if (!TextUtils.isEmpty(imageStoragePath)) {
                if (imageStoragePath.substring(imageStoragePath.lastIndexOf('.')).equals("." + Constants.IMAGE_EXTENSION)) {
                    showPopup("Picture Clicked!", true);
                } else if (imageStoragePath.substring(imageStoragePath.lastIndexOf('.')).equals("." + Constants.VIDEO_EXTENSION)) {
                    showPopup("Video Recorded", true);
                }
            }
        }
    }

    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                captureImage();
                            } else {
                                captureVideo();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, Constants.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Saving stored image path to saved instance state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putString(Constants.KEY_IMAGE_STORAGE_PATH, imageStoragePath);
    }

    /**
     * Restoring image path from saved instance state
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        imageStoragePath = savedInstanceState.getString(Constants.KEY_IMAGE_STORAGE_PATH);
    }

    /**
     * Launching camera app to record video
     */
    private void captureVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        File file = CameraUtils.getOutputMediaFile(Constants.MEDIA_TYPE_VIDEO);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);

        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file

        // start the video capture Intent
        startActivityForResult(intent, Constants.CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    /**
     * Activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
            Intent i = new Intent(this, VideoGalleryActivity.class);

            Gson gson = new Gson();
            int teamId = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP, ""), Team.class).getId();
            String teamName = gson.fromJson(sp.getStringFromMyPref(Constants.TEAM_SP,""), Team.class).getName();

            MediaUpload imgUpl = new MediaUpload();
            imgUpl.setIs_Media_Uploaded(Constants.NOT_UPLOADED_S3);
            imgUpl.setMedia_File_Name(imageStoragePath);
            imgUpl.setTeam_Id(teamId);
            imgUpl.setTeam_Name(teamName);
            mediaUploadUtil.insertMediaUpload(imgUpl);

            startActivity(i);
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(),
                    "User cancelled media", Toast.LENGTH_SHORT)
                    .show();
            showPopup("User cancelled media", false);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Failed to capture media", Toast.LENGTH_SHORT)
                    .show();
            showPopup("Sorry! Failed to capture media", false);
        }
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(getApplicationContext());
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /* Do nothing*/
                    }
                }).show();
    }

    private void showPopup(String msg, Boolean isSuccess) {
        if (isSuccess) {
            messageDialog.setContentView(R.layout.popup_message_positive);
        } else {
            messageDialog.setContentView(R.layout.popup_message_negative);
        }
        ImageView closeImg = (ImageView) messageDialog.findViewById(R.id.closePopupImg);
        Button closeBtn = messageDialog.findViewById(R.id.closePopupButton);
        TextView alertMsg = messageDialog.findViewById(R.id.message);
        alertMsg.setText(msg);

        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        messageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        messageDialog.show();
    }
}